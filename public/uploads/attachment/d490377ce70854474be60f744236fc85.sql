-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 03, 2022 at 08:04 PM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 7.4.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `freelancer_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `additional_services`
--

CREATE TABLE `additional_services` (
  `id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  `title` text DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `description` longtext DEFAULT NULL,
  `deliver_time` text DEFAULT NULL,
  `deliver_at_same_time` int(11) DEFAULT NULL,
  `additional_days` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `additional_services`
--

INSERT INTO `additional_services` (`id`, `service_id`, `title`, `price`, `description`, `deliver_time`, `deliver_at_same_time`, `additional_days`, `created_at`, `updated_at`, `deleted_at`) VALUES
(21, 20, 'test 1', 12, NULL, NULL, 0, 10, '2021-11-27 09:42:34', '2021-11-27 09:42:34', NULL),
(22, 22, 'Test1', 12, 'Test1', '3', NULL, 1, '2021-12-30 10:31:38', NULL, NULL),
(23, 22, 'Test2', 22, 'Test2', '2', NULL, 2, '2021-12-30 10:31:38', NULL, NULL),
(24, 22, 'Test3', 33, 'Test3', '1', NULL, 3, '2021-12-30 10:31:38', NULL, NULL),
(25, 23, 'Test1', 12, 'Test1', '3', NULL, 1, '2021-12-30 10:34:23', NULL, NULL),
(26, 23, 'Test2', 22, 'Test2', '2', NULL, 2, '2021-12-30 10:34:23', NULL, NULL),
(27, 23, 'Test3', 33, 'Test3', '1', NULL, 3, '2021-12-30 10:34:23', NULL, NULL),
(28, 24, 'Test1', 12, 'Test1', '3', NULL, 1, '2021-12-30 10:38:02', NULL, NULL),
(29, 24, 'Test2', 22, 'Test2', '2', NULL, 2, '2021-12-30 10:38:02', NULL, NULL),
(30, 24, 'Test3', 33, 'Test3', '1', NULL, 3, '2021-12-30 10:38:02', NULL, NULL),
(31, 25, 'Test1', 12, 'Test1', '3', NULL, 1, '2021-12-30 10:38:39', NULL, NULL),
(32, 25, 'Test2', 22, 'Test2', '2', NULL, 2, '2021-12-30 10:38:39', NULL, NULL),
(33, 25, 'Test3', 33, 'Test3', '1', NULL, 3, '2021-12-30 10:38:39', NULL, NULL),
(34, 26, 'Test1', 12, 'Test1', '3', NULL, 1, '2021-12-30 12:10:45', NULL, NULL),
(35, 26, 'Test2', 22, 'Test2', '2', NULL, 2, '2021-12-30 12:10:45', NULL, NULL),
(36, 26, 'Test3', 33, 'Test3', '1', NULL, 3, '2021-12-30 12:10:45', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `carts`
--

CREATE TABLE `carts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cart_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `service_id` int(11) NOT NULL,
  `quantity` smallint(5) UNSIGNED NOT NULL,
  `price` double NOT NULL,
  `total_price` double NOT NULL,
  `notes` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attachment` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `name_ar` text NOT NULL,
  `name_en` text NOT NULL,
  `image` text DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `name_ar`, `name_en`, `image`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'كتابة، تحرير، ترجمة\r\n', 'Writing, editing, translating\r\n', 'service-1.png', '2021-11-17 06:47:21', NULL, NULL),
(2, 'تسويق إلكتروني\r\n', 'E-Marketing\r\n', 'service-2.png', '2021-11-17 06:47:21', NULL, NULL),
(3, 'تصميم وأعمال فنية وإبداعية\r\n', 'Design, artwork and creativity\r\n', 'service-3.png', '2021-11-17 06:47:21', NULL, NULL),
(4, 'موسيقى و صوت\r\n', 'music and sound', 'service-4.png', '2021-11-17 06:47:21', NULL, NULL),
(5, 'أعمال وخدمات استشارية وإدارية\r\n', 'Business, consulting and management services', 'service-5.png', '2021-11-17 06:47:21', NULL, NULL),
(6, 'برمجة، تطوير وبناء المواقع والتطبيقات\r\n', 'Programming, development and construction of websites and applications', 'service-6.png', '2021-11-17 06:47:21', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `comments_reply`
--

CREATE TABLE `comments_reply` (
  `id` int(11) NOT NULL,
  `comment_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  `comment` text DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `comments_reply`
--

INSERT INTO `comments_reply` (`id`, `comment_id`, `user_id`, `service_id`, `comment`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 2, 21, 'أشكرك أستاذي الكريم ، تشرفت بالعمل مع حضرتك\r\n', '2021-11-27 12:06:38', NULL, NULL),
(2, 2, 5, 21, 'أشكرك أستاذي الكريم ، تشرفت بالعمل مع حضرتك\r\n', '2021-11-27 12:06:38', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fav_services`
--

CREATE TABLE `fav_services` (
  `id` int(11) NOT NULL,
  `service_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `fav_services`
--

INSERT INTO `fav_services` (`id`, `service_id`, `user_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(6, 1, 3, '2021-12-28 10:25:03', NULL, NULL),
(7, 1, 3, '2021-12-28 10:26:02', NULL, NULL),
(8, 1, 3, '2021-12-28 10:26:04', NULL, NULL),
(12, 1, 1, '2021-12-30 14:01:48', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `general_questions`
--

CREATE TABLE `general_questions` (
  `id` int(11) NOT NULL,
  `question_ar` text DEFAULT NULL,
  `answer_ar` longtext DEFAULT NULL,
  `question_en` longtext DEFAULT NULL,
  `answer_en` longtext DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `general_questions`
--

INSERT INTO `general_questions` (`id`, `question_ar`, `answer_ar`, `question_en`, `answer_en`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'السؤال 1', 'الجواب 1', 'question1', 'answer 1', '2021-11-16 10:11:55', NULL, NULL),
(2, 'السؤال 2', 'الجواب 2', 'question2', 'answer 2', '2021-11-16 10:11:55', NULL, NULL),
(3, 'السؤال 3', 'الجواب 3', 'question3', 'answer 3', '2021-11-16 10:11:55', NULL, NULL),
(4, 'السؤال 4', 'الجواب 4', 'question4', 'answer 4', '2021-11-16 10:11:55', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `home_slider`
--

CREATE TABLE `home_slider` (
  `id` int(11) NOT NULL,
  `title_1_ar` text DEFAULT NULL,
  `title_1_en` text DEFAULT NULL,
  `title_2_ar` text DEFAULT NULL,
  `title_2_en` text DEFAULT NULL,
  `description_1_ar` text DEFAULT NULL,
  `description_1_en` text DEFAULT NULL,
  `description_2_ar` text DEFAULT NULL,
  `description_2_en` text DEFAULT NULL,
  `image` text DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updatet_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `home_slider`
--

INSERT INTO `home_slider` (`id`, `title_1_ar`, `title_1_en`, `title_2_ar`, `title_2_en`, `description_1_ar`, `description_1_en`, `description_2_ar`, `description_2_en`, `image`, `created_at`, `updatet_at`, `deleted_at`) VALUES
(1, 'title_1_ar', 'title_1_en', 'title_2_ar', 'title_2_en', 'description_1_ar', 'description_1_en', 'description_2_ar', 'description_2_en', '6337-4.jpg', '2021-11-16 11:20:54', NULL, NULL),
(2, 'title_1_ar', 'title_1_en', 'title_2_ar', 'title_2_en', 'description_1_ar', 'description_1_en', 'description_2_ar', 'description_2_en', '6337-4.jpg', '2021-11-16 11:20:54', NULL, NULL),
(3, 'title_1_ar', 'title_1_en', 'title_2_ar', 'title_2_en', 'description_1_ar', 'description_1_en', 'description_2_ar', 'description_2_en', '6337-4.jpg', '2021-11-16 11:20:54', NULL, NULL),
(4, 'title_1_ar', 'title_1_en', 'title_2_ar', 'title_2_en', 'description_1_ar', 'description_1_en', 'description_2_ar', 'description_2_en', '6337-4.jpg', '2021-11-16 11:20:54', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `labels`
--

CREATE TABLE `labels` (
  `id` int(11) NOT NULL,
  `screen_id` int(11) NOT NULL,
  `language_id` int(4) NOT NULL,
  `label_id` varchar(60) DEFAULT NULL,
  `label_text_en` varchar(1000) DEFAULT NULL,
  `label_text_automated` varchar(1000) DEFAULT NULL,
  `label_text_override` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `labels`
--

INSERT INTO `labels` (`id`, `screen_id`, `language_id`, `label_id`, `label_text_en`, `label_text_automated`, `label_text_override`) VALUES
(1, 1, 1, 'english', 'English', 'انجليزية', NULL),
(2, 1, 2, 'english', 'English', 'English', NULL),
(3, 1, 2, 'communicate_with_service', 'Communicate with service owners and get the best quality service\r\n', 'Communicate with service owners and get the best quality service\r\n', NULL),
(4, 1, 1, 'communicate_with_service', 'Communicate with service owners and get the best quality service\r\n', 'تواصل مع أصحاب الخدمات واحصل على خدمة بأفضل جودة\r\n', NULL),
(5, 1, 1, 'here_you_will_find_freelance_services', 'Here you will find freelance services similar to your request\r\n', 'هنا تجد خدمات العمل الحر المماثلة لطلبك\r\n', NULL),
(6, 1, 2, 'here_you_will_find_freelance_services', 'Here you will find freelance services similar to your request\r\n', 'Here you will find freelance services similar to your request\r\n', NULL),
(7, 1, 2, 'search', 'Search', 'Search', NULL),
(8, 1, 1, 'search', 'Search', 'ابحث', NULL),
(9, 1, 1, 'most_wanted', 'most wanted\r\n', 'الأكثر طلبا', NULL),
(10, 1, 2, 'most_wanted', 'most wanted\r\n', 'most wanted\r\n', NULL),
(11, 1, 1, 'whole_world_of_premium_services', 'A whole world of premium services at your fingertips', 'عالم كامل من أصحاب الخدمات المميزة في متناول يدك\r\n', NULL),
(12, 1, 2, 'whole_world_of_premium_services', 'A whole world of premium services at your fingertips', 'A whole world of premium services at your fingertips', NULL),
(13, 1, 1, 'guaranteed_payment_method', 'Guaranteed payment method', 'طريقة دفع مضمونة', NULL),
(14, 1, 2, 'guaranteed_payment_method', 'Guaranteed payment method', 'Guaranteed payment method', NULL),
(15, 1, 1, 'dont_pay_until_you_agree_to_work', 'Don\'t pay until you agree to work', 'لا تدفع حتى توافق على العمل', NULL),
(16, 1, 2, 'dont_pay_until_you_agree_to_work', 'Don\'t pay until you agree to work', 'Don\'t pay until you agree to work', NULL),
(17, 1, 1, 'get_your_work_done_quickly', 'Get your work done quickly', 'انجز أعمالك بسرعه', NULL),
(18, 1, 2, 'get_your_work_done_quickly', 'Get your work done quickly', 'Get your work done quickly', NULL),
(19, 1, 1, 'choose_the_right_freelancer_to_get', 'Choose the right freelancer to get your work done quickly\r\n', ' اختر المستقل المناسب لإنجاز عملك في أسرع وقت', NULL),
(20, 1, 2, 'choose_the_right_freelancer_to_get', 'Choose the right freelancer to get your work done quickly\r\n', 'Choose the right freelancer to get your work done quickly\r\n', NULL),
(21, 1, 1, 'implement_your_projects', 'Implement your projects at lower costs', 'نفذ مشاريعك بتكاليف أقل', NULL),
(22, 1, 2, 'implement_your_projects', 'Implement your projects at lower costs', 'Implement your projects at lower costs', NULL),
(23, 1, 1, 'choose_your_service_according', 'Choose your service according to your budget\r\n', 'اختر خدمتك بما يتناسب مع ميزانيتك', NULL),
(24, 1, 2, 'choose_your_service_according', 'Choose your service according to your budget\r\n', 'Choose your service according to your budget\r\n', NULL),
(25, 1, 2, 'service_sections', 'Service Sections', 'Service Sections', NULL),
(26, 1, 1, 'service_sections', 'Service Sections', 'أقسام الخدمات', NULL),
(27, 1, 1, 'service_models', 'Service models', 'نماذج خدمات', NULL),
(28, 1, 2, 'service_models', 'Service models\r\n\r\n\r\n', 'Service models', NULL),
(29, 1, 2, 'find_the_talent_needed_to_grow', 'Find the talent needed to grow your business\r\n', 'Find the talent needed to grow your business\r\n', NULL),
(30, 1, 1, 'find_the_talent_needed_to_grow', 'Find the talent needed to grow your business\r\n', 'ابحث عن الموهبة اللازمة لتنمية أعمالك', NULL),
(31, 1, 1, 'start_now', 'Start Now', 'ابدأ الآن', NULL),
(32, 1, 2, 'start_now', 'Start Now', 'Start Now', NULL),
(33, 1, 1, 'about_us', 'About Us\n', 'من نحن\r\n', NULL),
(34, 1, 2, 'about_us', 'About Us\n', 'About Us\n', NULL),
(35, 1, 1, 'privacy_policy', 'Privacy policy\r\n', ' سياسة الخصوصية', NULL),
(36, 1, 2, 'privacy_policy', 'Privacy policy\r\n', 'Privacy policy\r\n', NULL),
(37, 1, 1, 'terms_conditions', 'Terms and Conditions\r\n', 'الشروط و الأحكام', NULL),
(38, 1, 2, 'terms_conditions', 'Terms and Conditions\r\n', 'Terms and Conditions\r\n', NULL),
(39, 1, 1, 'contact_us', 'Contact Us\r\n', 'تواصل معنا\r\n', NULL),
(40, 1, 2, 'contact_us', 'Contact Us\r\n', 'Contact Us\r\n', NULL),
(41, 1, 1, 'service_owner', 'Service Owners\n', 'صاحب خدمة', NULL),
(42, 1, 2, 'service_owner', 'Service Owners\n', 'Service Owners\n', NULL),
(43, 1, 1, 'common_questions', 'Frequently Asked Questions\n', 'الأسئلة الشائعة', NULL),
(44, 1, 2, 'common_questions', 'Frequently Asked Questions\n', 'Frequently Asked Questions\n', NULL),
(45, 1, 1, 'subscribe', 'Join Us\n', 'اشترك معنا\r\n', NULL),
(46, 1, 2, 'subscribe', 'Join Us', 'Join Us', NULL),
(47, 1, 1, 'proposals', 'Proposals', 'مقترحات', NULL),
(48, 1, 2, 'proposals', 'Proposals', 'Proposals', NULL),
(49, 1, 1, 'rights_reserved', 'All rights reserved', 'كل الحقوق محفوظة', NULL),
(50, 1, 2, 'rights_reserved', 'All rights reserved', 'All rights reserved', NULL),
(51, 1, 1, 'entrepreneur', 'Service Buyers\n', 'صاحب مشروع\r\n', NULL),
(52, 1, 2, 'entrepreneur', 'Service Buyers\n', 'Service Buyers\n', NULL),
(53, 1, 1, 'login', 'Login', 'تسجيل دخول', NULL),
(54, 1, 2, 'login', 'Login', 'Login', NULL),
(55, 1, 1, 'or', 'OR', 'أو', NULL),
(56, 1, 2, 'or', 'OR', 'OR', NULL),
(57, 1, 1, 'forgot_pass', 'Forgot password?', 'نسيت كلمة المرور؟\r\n', NULL),
(58, 1, 2, 'forgot_pass', 'Forgot password?', 'Forgot password?', NULL),
(59, 1, 1, 'remember', 'remember', 'تذكرني', NULL),
(60, 1, 2, 'remember', 'remember', 'remember', NULL),
(61, 1, 1, 'dont_have_account', 'I dont have an account?', 'ليس لدي حساب؟', NULL),
(62, 1, 2, 'dont_have_account', 'I dont have an account?', 'I dont have an account?', NULL),
(63, 1, 2, 'create_accont', 'Create Account', 'Create Account', NULL),
(64, 1, 1, 'create_accont', 'Create Account', 'انشاء حساب', NULL),
(65, 1, 1, 'joining_means_agreeing_to', 'Joining means agreeing to', 'انضمامي يعني الموافقة على', NULL),
(66, 1, 2, 'joining_means_agreeing_to', 'Joining means agreeing to', 'Joining means agreeing to', NULL),
(67, 1, 2, 'have_account', 'have account? ', 'have account? ', NULL),
(68, 1, 1, 'have_account', 'have account? ', 'لدي حساب؟', NULL),
(69, 1, 1, 'service_details', 'Service Details', 'تفاصيل الخدمة', NULL),
(70, 1, 2, 'service_details', 'Service Details', 'Service Details', NULL),
(71, 1, 1, 'welcome', 'Welcome,', 'أهلاً بك أستاذ/ة', NULL),
(72, 1, 2, 'welcome', 'Welcome,', 'Welcome,', NULL),
(73, 1, 1, 'get_the_service_you_want', 'Find the service owner you\'re looking for!\n', 'احصل على الخدمة التي تريدها من أصحاب خدمات مميزين هنا\r\n', NULL),
(74, 1, 2, 'get_the_service_you_want', 'Find the service owner you\'re looking for!\n', 'Find the service owner you\'re looking for!\n', NULL),
(75, 1, 1, 'latest_services', 'Latest Services', 'أحدث الخدمات', NULL),
(76, 1, 2, 'latest_services', 'Latest Services', 'Latest Services', NULL),
(77, 1, 1, 'services_you_may_like', 'Services you may like', 'خدمات قد تعجبك', NULL),
(78, 1, 2, 'services_you_may_like', 'Services you may like', 'Services you may like', NULL),
(79, 1, 1, 'be_a_service_owner', 'Are you a service owner?', 'كن صاحب خدمة؟', NULL),
(80, 1, 2, 'be_a_service_owner', 'Are you a service owner?', 'Are you a service owner?', NULL),
(81, 1, 1, 'settings', 'settings', 'الاعدادات', NULL),
(82, 1, 2, 'settings', 'settings', 'settings', NULL),
(83, 1, 2, 'help', 'Help', 'Help', NULL),
(84, 1, 1, 'help', 'Help', 'مساعدة', NULL),
(85, 1, 1, 'logout', 'Logout', 'تسجيل خروج', NULL),
(86, 1, 2, 'logout', 'Logout', 'Logout', NULL),
(87, 1, 2, 'service_models', 'Service models\r\n', 'Service models\r\n', NULL),
(88, 1, 1, 'service_models', 'Service models\r\n', 'نماذج الخدمات\r\n', NULL),
(89, 1, 2, 'description', 'Description', 'Description', NULL),
(90, 1, 1, 'description', 'Description', 'نبذة عني', NULL),
(91, 1, 2, 'date_registration', 'Member since\n', 'Member since\n', NULL),
(92, 1, 1, 'date_registration', 'Member since\n', 'تاريخ التسجيل\r\n', NULL),
(93, 1, 1, 'last_seen', 'last seen\r\n', 'اخر تواجد', NULL),
(94, 1, 2, 'last_seen', 'last seen\r\n', 'last seen\r\n', NULL),
(95, 1, 1, 'rate', 'Rate', 'التقييم', NULL),
(96, 1, 2, 'rate', 'Rate', 'Rate', NULL),
(97, 1, 1, 'skills', 'Skills', 'مهاراتي', NULL),
(98, 1, 2, 'skills', 'Skills', 'Skills', NULL),
(99, 1, 2, 'languages', 'Languages', 'Languages', NULL),
(100, 1, 1, 'languages', 'Languages', 'اللغات', NULL),
(101, 1, 2, 'education', 'Education', 'Education', NULL),
(102, 1, 1, 'education', 'Education', 'التعليم', NULL),
(103, 1, 2, 'watch_video', 'Watch video', 'Watch video', NULL),
(104, 1, 1, 'watch_video', 'Watch video', 'شاهد الفيديو', NULL),
(105, 1, 2, 'purchases', 'Purchases', 'Purchases', NULL),
(106, 1, 1, 'purchases', 'Purchases', 'المشتريات', NULL),
(107, 1, 2, 'incoming_orders', 'Incoming orders', 'Incoming orders', NULL),
(108, 1, 1, 'incoming_orders', 'Incoming orders', 'الطلبات الواردة', NULL),
(109, 1, 2, 'add_service', 'add service', 'add service', NULL),
(110, 1, 1, 'add_service', 'add service', 'أضف خدمة', NULL),
(111, 1, 2, 'common_questions_footer', 'FAQ', 'FAQ', NULL),
(112, 1, 1, 'common_questions_footer', 'FAQ', 'الاسئلة الشائعة', NULL),
(113, 1, 2, 'seller_rate', 'Seller\'s rating\r\n', 'Seller\'s rating\r\n', NULL),
(114, 1, 1, 'seller_rate', 'Seller\'s rating\r\n', 'تقييم البائع', NULL),
(115, 1, 2, 'edit_profile', 'Edit Profile', 'Edit Profile', NULL),
(116, 1, 1, 'edit_profile', 'Edit Profile', 'تعديل الملف الشخصي', NULL),
(117, 1, 2, 'name', 'Name', 'Name', NULL),
(118, 1, 1, 'name', 'Name', 'الاسم', NULL),
(119, 1, 2, 'job', 'Job', 'Job', NULL),
(120, 1, 1, 'job', 'Job', 'الوظيفة', NULL),
(121, 1, 2, 'skills', 'Skills', 'Skills', NULL),
(122, 1, 1, 'skills', 'Skills', 'مهاراتي', NULL),
(123, 1, 2, 'university', 'University', 'University', NULL),
(124, 1, 1, 'university', 'University', 'الجامعة', NULL),
(125, 1, 2, 'graduation_date', 'Graduation Date', 'Graduation Date', NULL),
(126, 1, 1, 'graduation_date', 'Graduation Date', 'تاريخ التخرج', NULL),
(127, 1, 2, 'about', 'About', 'About', NULL),
(128, 1, 1, 'about', 'About', 'نبذة عني', NULL),
(129, 1, 2, 'country', 'Country', 'Country', NULL),
(130, 1, 1, 'country', 'Country', 'الدولة', NULL),
(131, 1, 2, 'personal_photo', 'Personal photo', 'Personal photo', NULL),
(132, 1, 1, 'personal_photo', 'Personal photo', 'الصورة الشخصية', NULL),
(133, 1, 2, 'scientific_certificate', 'Scientific certificate', 'Scientific certificate', NULL),
(134, 1, 1, 'scientific_certificate', 'Scientific certificate', 'الشهادة العلمية', NULL),
(135, 1, 1, 'education', 'Education', 'التعليم', NULL),
(136, 1, 2, 'education', 'Education', 'Education', NULL),
(137, 1, 2, 'update', 'update', 'update', NULL),
(138, 1, 1, 'update', 'update', 'تحديث', NULL),
(139, 1, 2, 'service_rating', 'Service Rating', 'Service Rating', NULL),
(140, 1, 1, 'service_rating', 'Service Rating', 'تقييم الخدمة', NULL),
(141, 1, 2, 'additional_services', 'Additional Services', 'Additional Services', NULL),
(142, 1, 1, 'additional_services', 'Additional Services', 'خدمات اضافية', NULL),
(143, 1, 2, 'buyers_reviews', 'Buyers Reviews', 'Buyers Reviews', NULL),
(144, 1, 1, 'buyers_reviews', 'Buyers Reviews', 'آراء المشترين', NULL),
(145, 1, 1, 'add_to_cart', 'Add to cart', 'اضافة الى السلة', NULL),
(146, 1, 2, 'add_to_cart', 'Add to cart', 'Add to cart', NULL),
(147, 1, 2, 'number_times', 'number of times', 'number of times', NULL),
(148, 1, 1, 'number_times', 'number of times', 'عدد المرات', NULL),
(149, 1, 2, 'some_help', 'Some help', 'Some help', NULL),
(150, 1, 1, 'some_help', 'Some help', 'بعض التعليمات', NULL),
(151, 1, 2, 'attachment', 'Attachment', 'Attachment', NULL),
(152, 1, 1, 'attachment', 'Attachment', 'مرفق', NULL),
(153, 1, 2, 'keywords', 'Keywords', 'Keywords', NULL),
(154, 1, 1, 'keywords', 'Keywords', 'كلمات مفتاحية', NULL),
(155, 1, 2, 'suggested_services', 'Suggested Services', 'Suggested Services', NULL),
(156, 1, 1, 'suggested_services', 'Suggested Services', 'خدمات مقترحة', NULL),
(157, 1, 2, 'contact_service_owner', 'Contact with service owner', 'Contact with service owner', NULL),
(158, 1, 1, 'contact_service_owner', 'Contact with service owner', 'تواصل مع صاحب الخدمة', NULL),
(159, 1, 1, 'response_speed', 'Response speed', 'م.سرعة الرد', NULL),
(160, 1, 2, 'response_speed', 'Response speed', 'Response speed', NULL),
(161, 1, 2, 'hours', 'hours', 'hours', NULL),
(162, 1, 1, 'hours', 'hours', 'ساعات', NULL),
(163, 1, 1, 'message_to', 'message to', 'رسالة الى ', NULL),
(164, 1, 2, 'message_to', 'message to', 'message to', NULL),
(165, 1, 2, 'service', 'Service', 'Service', NULL),
(166, 1, 1, 'service', 'Service', 'الخدمة', NULL),
(167, 1, 2, 'message_content', 'Message content', 'Message content', NULL),
(168, 1, 1, 'message_content', 'Message content', 'محتوى الرسالة', NULL),
(169, 1, 2, 'ask_service_owner', 'Ask the service provider what you want to know about this service. It is forbidden to put external means of communication', 'Ask the service provider what you want to know about this service. It is forbidden to put external means of communication', NULL),
(170, 1, 1, 'ask_service_owner', 'Ask the service provider what you want to know about this service. It is forbidden to put external means of communication', 'اسأل مقدم الخدمة ما تريد معرفته عن هذه الخدمة. يمنع وضع وسائل تواصل خارجية\r\n', NULL),
(171, 1, 2, 'remaining', 'remaining', 'remaining', NULL),
(172, 1, 1, 'remaining', 'remaining', 'متبقي', NULL),
(173, 1, 2, 'message_does_not_contain', 'This message does not contain external means of communication and I send it because I want to purchase the offered service', 'This message does not contain external means of communication and I send it because I want to purchase the offered service', NULL),
(174, 1, 1, 'message_does_not_contain', 'This message does not contain external means of communication and I send it because I want to purchase the offered service', 'هذه الرسالة لا تحتوي على وسائل تواصل خارجية وأرسلها لأني أرغب بشراء الخدمة المعروضة', NULL),
(175, 1, 1, 'I_checked', 'I checked', 'لقد راجعت', NULL),
(176, 1, 2, 'I_checked', 'I checked', 'I checked', NULL),
(177, 1, 2, 'this_message', 'This message does not contradict it', 'This message does not contradict it', NULL),
(178, 1, 1, 'this_message', 'This message does not contradict it', ' وهذه الرسالة لا تخالفها بشيء', NULL),
(179, 1, 2, 'send_message', 'send message', 'send message', NULL),
(180, 1, 1, 'send_message', 'send message', 'ارسال الرسالة', NULL),
(181, 1, 2, 'all_chats', 'All Chats', 'All Chats', NULL),
(182, 1, 1, 'all_chats', 'All Chats', 'جميع المحادثات', NULL),
(183, 1, 2, 'add_new_service', 'Add a new service\r\n', 'Add a new service\r\n', NULL),
(184, 1, 1, 'add_new_service', 'Add a new service\r\n', 'أضف خدمة جديدة\r\n', NULL),
(185, 1, 2, 'service_title', 'Service title', 'Service title', NULL),
(186, 1, 1, 'service_title', 'Service title', 'عنوان الخدمة', NULL),
(187, 1, 2, 'category', 'Category', 'Category', NULL),
(188, 1, 1, 'category', 'Category', 'التصنيف', NULL),
(189, 1, 2, 'choose_section', 'Choose section', 'Choose section', NULL),
(190, 1, 1, 'choose_section', 'Choose section', 'اختر القسم', NULL),
(191, 1, 1, 'enter_accurate_service_description', 'Enter an accurate service description. Make sure you include all necessary details. Including external means of communication is strictly prohibited.\r\n', 'أدخل وصف الخدمة بدقة يتضمن جميع المعلومات والشروط . يمنع وضع البريد الالكتروني، رقم الهاتف أو أي معلومات اتصال أخرى', NULL),
(192, 1, 2, 'enter_accurate_service_description', 'Enter an accurate service description. Make sure you include all necessary details. Including external means of communication is strictly prohibited.\r\n', 'Enter an accurate service description. Make sure you include all necessary details. Including external means of communication is strictly prohibited.\r\n', NULL),
(193, 1, 1, 'delivery_time', 'Delivery time', 'مدة التسليم (يوم)', NULL),
(194, 1, 2, 'delivery_time', 'Delivery time', 'Delivery time', NULL),
(195, 1, 2, 'price', 'Price', 'Price', NULL),
(196, 1, 1, 'price', 'Price', 'السعر', NULL),
(197, 1, 1, 'instructions_service_buyer', 'Instructions for service buyer', 'تعليمات للمشتري', NULL),
(198, 1, 2, 'instructions_service_buyer', 'Instructions for service buyer', 'Instructions for service buyer', NULL),
(199, 1, 1, 'details_client_should_provide', 'Specify what details the client should provide you with before starting the project.\r\n', 'المعلومات التي تحتاجها من المشتري لتنفيذ الخدمة. تظهر هذه المعلومات بعد شراء الخدمة فقط', NULL),
(200, 1, 2, 'details_client_should_provide', 'Specify what details the client should provide you with before starting the project.\r\n', 'Specify what details the client should provide you with before starting the project.\r\n', NULL),
(201, 1, 2, 'service_gallery', 'Service Gallery\r\n', 'Service Gallery\r\n', NULL),
(202, 1, 1, 'service_gallery', 'Service Gallery\r\n', 'معرض الخدمة', NULL),
(203, 1, 1, 'gallery_notes', 'Choosing a well-designed video or image will make your service look professional and help you increase your sales.', 'اختيار فيديو أو صورة مصممة بشكل جيد ستظهر خدمتك بشكل احترافي وتزيد من مبيعاتك.', NULL),
(204, 1, 2, 'gallery_notes', 'Choosing a well-designed video or image will make your service look professional and help you increase your sales.\r\n', 'Choosing a well-designed video or image will make your service look professional and help you increase your sales.\r\n', NULL),
(205, 1, 2, 'additional_services', 'Additional services', 'Additional services', NULL),
(206, 1, 1, 'additional_services', 'Additional services', 'خدمات اضافية', NULL),
(207, 1, 2, 'service_description', 'Service description', 'Service description', NULL),
(208, 1, 1, 'service_description', 'Service description', 'وصف الخدمة', NULL),
(209, 1, 2, 'add_question', 'Add Question', 'Add Question', NULL),
(210, 1, 1, 'add_question', 'Add Question', 'اضف سؤال', NULL),
(211, 1, 1, 'add_answer', 'Add Answer', 'أضف الاجابة', NULL),
(212, 1, 2, 'add_answer', 'Add Answer', 'Add Answer', NULL),
(213, 1, 2, 'add_cost', 'cost of addition', 'cost of addition', NULL),
(214, 1, 1, 'add_cost', 'cost of addition', 'تكلفة الاضافة', NULL),
(215, 1, 2, 'same_date', 'same date', 'same date', NULL),
(216, 1, 1, 'same_date', 'same date', 'نفس الموعد', NULL),
(217, 1, 2, 'extra_date', 'extra date', 'extra date', NULL),
(218, 1, 1, 'extra_date', 'extra date', 'موعد اضافي', NULL),
(219, 1, 2, 'extra_days', 'Extra days for delivery', 'Extra days for delivery', NULL),
(220, 1, 1, 'extra_days', 'Extra days for delivery', 'عدد الايام الاضافية للتسليم', NULL),
(221, 1, 2, 'delete', 'Delete', 'Delete', NULL),
(222, 1, 1, 'delete', 'Delete', 'حذف', NULL),
(223, 1, 2, 'add_additional_service', 'add additional service', 'add additional service', NULL),
(224, 1, 1, 'add_additional_service', 'add additional service', 'أضف خدمة إضافية', NULL),
(225, 1, 2, 'add_service', 'Add Service', 'Add Service', NULL),
(226, 1, 1, 'add_service', 'Add Service', 'اضف الخدمة', NULL),
(227, 1, 2, 'settings', 'Settings', 'Settings', NULL),
(228, 1, 1, 'settings', 'Settings', 'الاعدادات', NULL),
(229, 1, 2, 'deactivate_account', 'Your profile and services will not be displayed on the site anymore', 'Your profile and services will not be displayed on the site anymore', NULL),
(230, 1, 1, 'deactivate_account', 'Your profile and services will not be displayed on the site anymore', 'لن يتم عرض ملفك الشخصي وخدماتك على الموقع بعد الآن', NULL),
(231, 1, 2, 'cancel_account', 'Cancel Account', 'Cancel Account', NULL),
(232, 1, 1, 'cancel_account', 'Cancel Account', 'الغاء الحساب', NULL),
(233, 1, 2, 'delete_account_reasone', 'Reason for account cancellation', 'Reason for account cancellation', NULL),
(234, 1, 1, 'delete_account_reasone', 'Reason for account cancellation', 'سبب الغاء الحساب\r\n', NULL),
(235, 1, 2, 'select_reason', 'Select Reason', 'Select Reason', NULL),
(236, 1, 1, 'select_reason', 'Select Reason', 'اختر السبب', NULL),
(237, 1, 2, 'i_have_another_account', 'I have another account on the site', 'I have another account on the site', NULL),
(238, 1, 1, 'i_have_another_account', 'I have another account on the site', 'لدي حساب آخر على الموقع', NULL),
(239, 1, 1, 'i_want_change_user_name', 'I want to change my username', 'أريد تغيير اسم المستخدم', NULL),
(240, 1, 2, 'i_want_change_user_name', 'I want to change my username', 'I want to change my username', NULL),
(241, 1, 1, 'i_not_getting_good_offers', 'I\'m not getting good offers', 'لا أتلقى عروض جيدة', NULL),
(242, 1, 2, 'i_not_getting_good_offers', 'I\'m not getting good offers', 'I\'m not getting good offers', NULL),
(243, 1, 1, 'site_difficult_use', 'The site is difficult to use', 'الموقع صعب استخدامه', NULL),
(244, 1, 2, 'site_difficult_use', 'The site is difficult to use', 'The site is difficult to use', NULL),
(245, 1, 1, 'site_policies_inconvenient', 'Site policies are inconvenient', 'سياسات الموقع لم تعجبني', NULL),
(246, 1, 2, 'site_policies_inconvenient', 'Site policies are inconvenient', 'Site policies are inconvenient', NULL),
(247, 1, 2, 'account', 'Account', 'Account', NULL),
(248, 1, 1, 'account', 'Account', 'حسابي', NULL),
(249, 1, 1, 'notifications', 'Notifications', 'الاشعارات', NULL),
(250, 1, 2, 'notifications', 'Notifications', 'Notifications', NULL),
(251, 1, 1, 'your_feedback', 'Write your feedback here\r\n', 'اكتب ملاحظاتك هنا\r\n', NULL),
(252, 1, 2, 'your_feedback', 'Write your feedback here\r\n', 'Write your feedback here\r\n', NULL),
(253, 1, 1, 'disable_account', 'Disable Account', 'تعطيل الحساب', NULL),
(254, 1, 2, 'disable_account', 'Disable Account', 'Disable Account', NULL),
(255, 1, 2, 'enable', 'Enable', 'Enable', NULL),
(256, 1, 1, 'enable', 'Enable', 'مفعل', NULL),
(257, 1, 1, 'not_enabled', 'Not enabled', 'غير مفعل', NULL),
(258, 1, 2, 'not_enabled', 'Not enabled', 'Not enabled', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE `languages` (
  `id` int(11) NOT NULL,
  `lang_code` varchar(6) DEFAULT NULL,
  `lang_name` varchar(40) DEFAULT NULL,
  `lang_name_automated` varchar(150) DEFAULT NULL,
  `lang_flag` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `languages`
--

INSERT INTO `languages` (`id`, `lang_code`, `lang_name`, `lang_name_automated`, `lang_flag`) VALUES
(1, 'ar', 'Arabic', 'العربية', NULL),
(2, 'en', 'English', 'English', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` int(11) NOT NULL,
  `sender_id` int(11) NOT NULL,
  `receiver_id` int(11) NOT NULL,
  `message` longtext DEFAULT NULL,
  `is_read` int(11) DEFAULT NULL,
  `read_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `sender_id`, `receiver_id`, `message`, `is_read`, `read_at`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 3, 1, 'rrrrrrrrrrrrrrrrrrrrrrrrrrrr', NULL, NULL, '2021-11-29 08:12:45', '2021-11-29 08:12:45', NULL),
(2, 1, 3, 'ttttttttttttttttt', NULL, NULL, '2021-11-29 08:18:53', '2021-11-29 08:18:53', NULL),
(3, 1, 3, 'ttttttttttttttttt', NULL, NULL, '2021-11-29 08:19:01', '2021-11-29 08:19:01', NULL),
(4, 3, 1, 'test Message', NULL, NULL, '2021-11-29 09:13:23', '2021-11-29 09:13:23', NULL),
(5, 1, 2, 'opopopo', NULL, NULL, '2021-11-30 13:39:41', '2021-11-30 13:39:41', NULL),
(6, 2, 1, 'انا بدي الخدمة فثسف', NULL, NULL, '2021-11-30 13:43:45', '2021-11-30 13:43:45', NULL),
(7, 2, 1, 'انا بدي الخدمة فثسف', NULL, NULL, '2021-11-30 13:44:01', '2021-11-30 13:44:01', NULL),
(8, 7, 1, 'ي محمد انا بدي خدمة', NULL, NULL, '2021-12-01 07:38:53', '2021-12-01 07:38:53', NULL),
(9, 7, 1, 'Ut perferendis qui m', NULL, NULL, '2021-12-01 14:04:29', '2021-12-01 14:04:29', NULL),
(10, 1, 7, 'as./d,;asl,d', NULL, NULL, '2021-12-01 20:43:06', '2021-12-01 20:43:06', NULL),
(11, 1, 7, 'محمد الشوربجي رد', NULL, NULL, '2021-12-01 20:43:31', '2021-12-01 20:43:31', NULL),
(12, 1, 3, 'asdasdasd', NULL, NULL, '2021-12-01 20:47:51', '2021-12-01 20:47:51', NULL),
(13, 4, 4, 'rrrrrr', NULL, NULL, '2021-12-08 19:10:57', '2021-12-08 19:10:57', NULL),
(14, 1, 4, 'مرحبا', NULL, NULL, '2021-12-12 19:49:28', '2021-12-12 19:49:28', NULL),
(15, 4, 1, 'ي هلا ي معلمي', NULL, NULL, '2021-12-12 19:50:43', '2021-12-12 19:50:43', NULL),
(16, 5, 4, 'مرحبا ي محمد', NULL, NULL, '2021-12-13 15:01:31', '2021-12-13 15:01:31', NULL),
(17, 5, 4, 'asdasdasd', NULL, NULL, '2021-12-13 15:13:04', '2021-12-13 15:13:04', NULL),
(18, 5, 4, 'asdasd', NULL, NULL, '2021-12-13 15:14:51', '2021-12-13 15:14:51', NULL),
(19, 5, 4, 'asdasd', NULL, NULL, '2021-12-13 15:16:17', '2021-12-13 15:16:17', NULL),
(20, 5, 4, 'asdasdsad', NULL, NULL, '2021-12-13 15:18:10', '2021-12-13 15:18:10', NULL),
(21, 5, 4, 'asdasdsad', NULL, NULL, '2021-12-13 15:20:13', '2021-12-13 15:20:13', NULL),
(22, 5, 4, 'مصطفى', NULL, NULL, '2021-12-13 15:20:27', '2021-12-13 15:20:27', NULL),
(23, 5, 4, 'مصطفى', NULL, NULL, '2021-12-13 15:20:46', '2021-12-13 15:20:46', NULL),
(24, 5, 5, 'ي هلا ي معلمي', NULL, NULL, '2021-12-13 15:23:08', '2021-12-13 15:23:08', NULL),
(25, 4, 1, 'هلا ي محمد .. تست', NULL, NULL, '2021-12-14 15:26:26', '2021-12-14 15:26:26', NULL),
(26, 4, 1, 'هلا ي محمد .. تست', NULL, NULL, '2021-12-14 15:26:43', '2021-12-14 15:26:43', NULL),
(27, 4, 1, 'هلا ي محمد .. تست', NULL, NULL, '2021-12-14 15:29:22', '2021-12-14 15:29:22', NULL),
(28, 4, 1, 'asdsadasd', NULL, NULL, '2021-12-14 15:34:57', '2021-12-14 15:34:57', NULL),
(29, 4, 1, 'شسيشسيشسي', NULL, NULL, '2021-12-14 15:37:19', '2021-12-14 15:37:19', NULL),
(30, 4, 1, 'asdsad', NULL, NULL, '2021-12-14 15:37:30', '2021-12-14 15:37:30', NULL),
(31, 4, 1, 'شسيشسيسشيشس', NULL, NULL, '2021-12-14 15:42:40', '2021-12-14 15:42:40', NULL),
(32, 4, 1, 'asdasdasd', NULL, NULL, '2021-12-14 15:50:13', '2021-12-14 15:50:13', NULL),
(33, 4, 1, 'asdasdasd', NULL, NULL, '2021-12-14 15:50:28', '2021-12-14 15:50:28', NULL),
(34, 4, 1, 'asdasdasd', NULL, NULL, '2021-12-14 15:51:28', '2021-12-14 15:51:28', NULL),
(35, 4, 1, 'شسيشسيشسي', NULL, NULL, '2021-12-14 15:55:25', '2021-12-14 15:55:25', NULL),
(36, 4, 1, 'asdasdasd', NULL, NULL, '2021-12-14 15:59:30', '2021-12-14 15:59:30', NULL),
(37, 4, 1, 'asdasdsad', NULL, NULL, '2021-12-14 16:00:14', '2021-12-14 16:00:14', NULL),
(38, 4, 1, 'asdasdasdasdasd', NULL, NULL, '2021-12-14 16:04:49', '2021-12-14 16:04:49', NULL),
(39, 4, 1, 'sadasdasdsa', NULL, NULL, '2021-12-14 16:06:50', '2021-12-14 16:06:50', NULL),
(40, 4, 1, 'اهلا ي محمد', NULL, NULL, '2021-12-14 16:07:23', '2021-12-14 16:07:23', NULL),
(41, 4, 1, 'محمد انا بدي خدمة', NULL, NULL, '2021-12-14 16:08:24', '2021-12-14 16:08:24', NULL),
(42, 4, 1, 'asdasdddddddddd', NULL, NULL, '2021-12-14 16:09:04', '2021-12-14 16:09:04', NULL),
(43, 4, 1, 'asdasdasd', NULL, NULL, '2021-12-14 16:09:20', '2021-12-14 16:09:20', NULL),
(45, 4, 5, 'asdasdasd', NULL, NULL, '2021-12-14 16:16:19', '2021-12-14 16:16:19', NULL),
(46, 4, 5, 'asdasdasd', NULL, NULL, '2021-12-14 16:16:33', '2021-12-14 16:16:33', NULL),
(47, 4, 5, 'asdasd', NULL, NULL, '2021-12-14 16:18:11', '2021-12-14 16:18:11', NULL),
(48, 4, 5, 'asdsadasd', NULL, NULL, '2021-12-14 16:18:37', '2021-12-14 16:18:37', NULL),
(49, 4, 5, 'asdasdasd', NULL, NULL, '2021-12-14 16:20:06', '2021-12-14 16:20:06', NULL),
(50, 1, 3, 'asdasdassssssssssssssssssssssssssssssasdasdassssssssssssssssssssssssssssssasdasasdasdassssssssssssssssssssssssssssssasdasdassssssssssssssssssssssssssssssasdas', NULL, NULL, '2021-12-14 16:26:09', '2021-12-14 16:26:09', NULL),
(51, 4, 1, 'asdasdassssssssssssssssssssssssssssssasdasdassssssssssssssssssssssssssssssasdas', NULL, NULL, '2021-12-14 16:26:41', '2021-12-14 16:26:41', NULL),
(52, 4, 1, 'lvpflsakjdlsad', NULL, NULL, '2021-12-15 06:09:47', '2021-12-15 06:09:47', NULL),
(53, 4, 1, 'tysayudgasdsdsadasd', NULL, NULL, '2021-12-15 06:11:13', '2021-12-15 06:11:13', NULL),
(54, 4, 1, 'kasjdkasd', NULL, NULL, '2021-12-15 06:12:17', '2021-12-15 06:12:17', NULL),
(55, 4, 1, 'tttttttttt', NULL, NULL, '2021-12-15 06:19:58', '2021-12-15 06:19:58', NULL),
(56, 4, 1, 'jhasbdjkasnd', NULL, NULL, '2021-12-15 06:28:17', '2021-12-15 06:28:17', NULL),
(57, 4, 1, 'zcsaasd', NULL, NULL, '2021-12-15 06:28:33', '2021-12-15 06:28:33', NULL),
(58, 4, 1, 'saadddddddd', NULL, NULL, '2021-12-15 06:33:38', '2021-12-15 06:33:38', NULL),
(59, 4, 1, 'asdasdasd', NULL, NULL, '2021-12-15 06:33:50', '2021-12-15 06:33:50', NULL),
(60, 4, 1, 'akjsdnklasdnklsad', NULL, NULL, '2021-12-15 06:35:18', '2021-12-15 06:35:18', NULL),
(61, 4, 1, 'jashdkasd', NULL, NULL, '2021-12-15 07:02:51', '2021-12-15 07:02:51', NULL),
(62, 4, 1, 'tttttttttt', NULL, NULL, '2021-12-15 07:05:15', '2021-12-15 07:05:15', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(5, '2021_12_13_174821_create_notifications_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_id` bigint(20) UNSIGNED NOT NULL,
  `data` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `read_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `type`, `notifiable_type`, `notifiable_id`, `data`, `read_at`, `created_at`, `updated_at`) VALUES
('12b2a84e-babb-44bd-b952-8272eb9fb852', 'App\\Notifications\\web\\ServiceRequestsNotification', 'App\\Models\\User', 1, '{\"type\":\"service_request\",\"item_id\":53,\"service_name\":\"Test title1\",\"requester_user_id\":4,\"title\":\" \\u0637\\u0644\\u0628 \\u062e\\u062f\\u0645\\u0629\",\"body\":\"\\u0644\\u062f\\u064a\\u0643 \\u0637\\u0644\\u0628 \\u062e\\u062f\\u0645\\u0629\"}', '2021-12-15 07:53:06', '2021-12-15 07:52:49', '2021-12-15 07:53:06'),
('307c4a59-0707-40d1-8939-56975e99b48b', 'App\\Notifications\\web\\ServiceRequestsNotification', 'App\\Models\\User', 1, '{\"type\":\"service_request\",\"item_id\":51,\"service_name\":\"Test title1\",\"requester_user_id\":5,\"title\":\" \\u0637\\u0644\\u0628 \\u062e\\u062f\\u0645\\u0629\",\"body\":\"\\u0644\\u062f\\u064a\\u0643 \\u0637\\u0644\\u0628 \\u062e\\u062f\\u0645\\u0629\"}', '2021-12-13 16:42:52', '2021-12-13 16:42:38', '2021-12-13 16:42:52'),
('713d1ef9-6d48-4de0-9ea2-0c7a604c97d5', 'App\\Notifications\\web\\ServiceRequestsNotification', 'App\\Models\\User', 3, '{\"type\":\"service_request\",\"item_id\":4,\"service_name\":\"Test title2\",\"requester_user_id\":4,\"title\":\" \\u0637\\u0644\\u0628 \\u062e\\u062f\\u0645\\u0629\",\"body\":\"\\u0644\\u062f\\u064a\\u0643 \\u0637\\u0644\\u0628 \\u062e\\u062f\\u0645\\u0629\"}', NULL, '2021-12-20 13:56:53', '2021-12-20 13:56:53'),
('798803b3-60fc-49d1-816e-2e771d1fe06b', 'App\\Notifications\\web\\ServiceRequestsNotification', 'App\\Models\\User', 1, '{\"type\":\"service_request\",\"item_id\":1,\"service_name\":\"Test title1\",\"requester_user_id\":4,\"title\":\" \\u0637\\u0644\\u0628 \\u062e\\u062f\\u0645\\u0629\",\"body\":\"\\u0644\\u062f\\u064a\\u0643 \\u0637\\u0644\\u0628 \\u062e\\u062f\\u0645\\u0629\"}', NULL, '2021-12-19 15:18:20', '2021-12-19 15:18:20'),
('89d1e52b-bd7b-4729-8cb2-59bb35d945ca', 'App\\Notifications\\web\\ServiceRequestsNotification', 'App\\Models\\User', 4, '{\"type\":\"service_request\",\"item_id\":52,\"service_name\":\"\\u0639\\u0646\\u0648\\u0627\\u0646 \\u0627\\u0644\\u062e\\u062f\\u0645\\u0629\",\"requester_user_id\":1,\"title\":\" \\u0637\\u0644\\u0628 \\u062e\\u062f\\u0645\\u0629\",\"body\":\"\\u0644\\u062f\\u064a\\u0643 \\u0637\\u0644\\u0628 \\u062e\\u062f\\u0645\\u0629\"}', '2021-12-13 16:46:52', '2021-12-13 16:46:23', '2021-12-13 16:46:52'),
('c70eae04-c7a5-4d29-a0e8-064599f9c4d6', 'App\\Notifications\\web\\ServiceRequestsNotification', 'App\\Models\\User', 1, '{\"type\":\"service_request\",\"item_id\":2,\"service_name\":\"Test title1\",\"requester_user_id\":3,\"title\":\" \\u0637\\u0644\\u0628 \\u062e\\u062f\\u0645\\u0629\",\"body\":\"\\u0644\\u062f\\u064a\\u0643 \\u0637\\u0644\\u0628 \\u062e\\u062f\\u0645\\u0629\"}', NULL, '2021-12-20 06:58:45', '2021-12-20 06:58:45'),
('d285253f-b49a-4951-852f-8d284f20aed5', 'App\\Notifications\\web\\ServiceRequestsNotification', 'App\\Models\\User', 3, '{\"type\":\"service_request\",\"item_id\":3,\"service_name\":\"Test title2\",\"requester_user_id\":4,\"title\":\" \\u0637\\u0644\\u0628 \\u062e\\u062f\\u0645\\u0629\",\"body\":\"\\u0644\\u062f\\u064a\\u0643 \\u0637\\u0644\\u0628 \\u062e\\u062f\\u0645\\u0629\"}', NULL, '2021-12-20 13:51:05', '2021-12-20 13:51:05'),
('e251c4a5-632a-42f6-9425-a7abd0d45db3', 'App\\Notifications\\web\\ServiceRequestsNotification', 'App\\Models\\User', 1, '{\"type\":\"service_request\",\"item_id\":54,\"service_name\":\"Test title1\",\"requester_user_id\":4,\"title\":\" \\u0637\\u0644\\u0628 \\u062e\\u062f\\u0645\\u0629\",\"body\":\"\\u0644\\u062f\\u064a\\u0643 \\u0637\\u0644\\u0628 \\u062e\\u062f\\u0645\\u0629\"}', NULL, '2021-12-15 15:28:01', '2021-12-15 15:28:01'),
('e27bd121-2cd6-4637-b814-001288638862', 'App\\Notifications\\web\\ServiceRequestsNotification', 'App\\Models\\User', 1, '{\"type\":\"service_request\",\"item_id\":50,\"service_name\":\"Test title1\",\"requester_user_id\":5,\"title\":\" \\u0637\\u0644\\u0628 \\u062e\\u062f\\u0645\\u0629\",\"body\":\"\\u0644\\u062f\\u064a\\u0643 \\u0637\\u0644\\u0628 \\u062e\\u062f\\u0645\\u0629\"}', '2021-12-13 16:41:09', '2021-12-13 16:27:34', '2021-12-13 16:41:09'),
('f3ac436c-59a6-4796-95fb-c648d91f1383', 'App\\Notifications\\web\\ServiceRequestsNotification', 'App\\Models\\User', 1, '{\"type\":\"service_request\",\"item_id\":1,\"service_name\":\"Test title1\",\"requester_user_id\":\"4\",\"title\":\" \\u0637\\u0644\\u0628 \\u062e\\u062f\\u0645\\u0629\",\"body\":\"\\u0644\\u062f\\u064a\\u0643 \\u0637\\u0644\\u0628 \\u062e\\u062f\\u0645\\u0629\"}', NULL, '2021-12-20 23:25:29', '2021-12-20 23:25:29');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `total_price` double NOT NULL,
  `status` enum('UNPAID','COMPLATED','CANCELED') NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `user_id`, `total_price`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 120, 'UNPAID', '2021-12-28 07:49:58', '2021-12-20 23:25:29', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `order_items`
--

CREATE TABLE `order_items` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `price` double UNSIGNED NOT NULL,
  `user_owner_service_id` bigint(20) UNSIGNED NOT NULL,
  `in_progress` int(11) DEFAULT NULL,
  `is_complete` int(11) DEFAULT NULL,
  `is_canceled` int(11) DEFAULT NULL,
  `is_delivered` int(11) DEFAULT NULL,
  `waiting_acceptance` int(11) DEFAULT NULL,
  `finished` int(11) DEFAULT NULL,
  `notes` longtext DEFAULT NULL,
  `attachment` text DEFAULT NULL,
  `delivery_title` text DEFAULT NULL,
  `delivery_desc` longtext DEFAULT NULL,
  `delivery_attachment` text DEFAULT NULL,
  `review_title` text DEFAULT NULL,
  `review_desc` longtext DEFAULT NULL,
  `review_attachment` text DEFAULT NULL,
  `type` text DEFAULT NULL,
  `review_item_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `order_items`
--

INSERT INTO `order_items` (`id`, `order_id`, `service_id`, `quantity`, `price`, `user_owner_service_id`, `in_progress`, `is_complete`, `is_canceled`, `is_delivered`, `waiting_acceptance`, `finished`, `notes`, `attachment`, `delivery_title`, `delivery_desc`, `delivery_attachment`, `review_title`, `review_desc`, `review_attachment`, `type`, `review_item_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 1, 1, 120, 1, 0, 0, 0, 0, 0, 1, 'Test Notes', 'bdad83472411e73742e8139ca1db9fb0.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'review', NULL, '2021-12-20 15:38:06', '2021-12-20 23:38:06', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `order_movements_item`
--

CREATE TABLE `order_movements_item` (
  `id` int(11) NOT NULL,
  `order_item_id` int(11) DEFAULT NULL,
  `title` text DEFAULT NULL,
  `desc` longtext DEFAULT NULL,
  `attachment` text DEFAULT NULL,
  `type` text DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `order_movements_item`
--

INSERT INTO `order_movements_item` (`id`, `order_item_id`, `title`, `desc`, `attachment`, `type`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'First Deliver', 'Firest Deliver DEsc', 'a5f301c90448b82a203f7d8068a01b6a.png', 'deliver', '2021-12-20 23:35:15', '2021-12-20 23:35:15', NULL),
(2, 1, 'Review', 'Review', 'ac381a31a1b68e6997a3f5cce5f0fc6c.png', 'finished', '2021-12-20 23:37:10', '2021-12-20 23:38:06', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('admin@gmail.com', '366402786', '2021-12-26 10:43:50'),
('admin@gmail.com', '735652609', '2021-12-30 12:58:08'),
('admin@gmail.com', '712500784', '2021-12-30 12:58:16');

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `personal_access_tokens`
--

INSERT INTO `personal_access_tokens` (`id`, `tokenable_type`, `tokenable_id`, `name`, `token`, `abilities`, `last_used_at`, `created_at`, `updated_at`) VALUES
(1, 'App\\Models\\User', 18, 'MyToken', 'f36a55325c8ae1d1b24020f37bff15bf1f7c83a0a3a83acd24e0ee6176f3b2b8', '[\"*\"]', NULL, '2021-12-24 12:35:34', '2021-12-24 12:35:34'),
(2, 'App\\Models\\User', 18, 'MyAuthApp', 'c643baf307342401f3f1811df6248fbf48c5cede76d1cb408b210bd8cbc31c31', '[\"*\"]', NULL, '2021-12-26 14:31:57', '2021-12-26 14:31:57'),
(3, 'App\\Models\\User', 18, 'MyAuthApp', '714ecb8cde8119d3756347568767844f1a7c1a7160aca09be9077578ea938a22', '[\"*\"]', NULL, '2021-12-26 14:39:48', '2021-12-26 14:39:48'),
(4, 'App\\Models\\User', 19, 'MyToken', 'b6a74bf70dfc1c5bbae7892ec4bc31cf7c6f4926f95feac8458f6f344ed653bf', '[\"*\"]', NULL, '2021-12-27 17:11:25', '2021-12-27 17:11:25'),
(5, 'App\\Models\\User', 18, 'MyAuthApp', 'd381e2e429ac73e85982f8fefbc141cae6de551ab9a886f5c7301bc02d0e52b3', '[\"*\"]', NULL, '2021-12-27 17:50:40', '2021-12-27 17:50:40'),
(6, 'App\\Models\\User', 18, 'MyAuthApp', 'fc78d910d851a63836b1d0a260b73d5a5e3d8187d54e1e1ea435eaba7a5fba60', '[\"*\"]', '2021-12-30 14:01:48', '2021-12-28 07:42:40', '2021-12-30 14:01:48'),
(7, 'App\\Models\\User', 18, 'MyAuthApp', '652cdb4f1a7505e16bf59487a2d32ac031db28987d945b2786f2fe291789b9de', '[\"*\"]', '2021-12-30 14:02:20', '2021-12-29 09:25:28', '2021-12-30 14:02:20'),
(8, 'App\\Models\\User', 18, 'MyAuthApp', '6fde4ed74b1608e8acaf3fc48a7d2bbc0014aa4932471b9f316ce58b977f13d0', '[\"*\"]', '2021-12-30 14:03:19', '2021-12-30 02:46:32', '2021-12-30 14:03:19'),
(9, 'App\\Models\\User', 18, 'MyAuthApp', 'fdc99d0f472ae19101467752914b75091e90304878ba6e4970d31fbc4c012273', '[\"*\"]', NULL, '2021-12-30 12:55:27', '2021-12-30 12:55:27'),
(10, 'App\\Models\\User', 20, 'MyToken', '6b73c3cdd219a09ef432fab80a2b9b76e955af4204ddeca6394efe54904b0e80', '[\"*\"]', NULL, '2021-12-30 13:11:29', '2021-12-30 13:11:29'),
(11, 'App\\Models\\User', 20, 'MyToken', '026cb5533446fd6a077f90444de8c9d711bbee93b12dfe3762eb239681c9bc0d', '[\"*\"]', NULL, '2021-12-30 13:11:37', '2021-12-30 13:11:37'),
(12, 'App\\Models\\User', 21, 'MyToken', '88b76ca6eee039c12e5da397bad41e576d9230cb340014864405be89ecea0137', '[\"*\"]', NULL, '2021-12-30 13:11:41', '2021-12-30 13:11:41'),
(13, 'App\\Models\\User', 18, 'MyAuthApp', 'c81870bebed39e0a94db7cdb49c6822e5472bd41575158a65bffdcae7fa42035', '[\"*\"]', NULL, '2021-12-30 13:59:49', '2021-12-30 13:59:49');

-- --------------------------------------------------------

--
-- Table structure for table `recently_views`
--

CREATE TABLE `recently_views` (
  `id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `recently_views`
--

INSERT INTO `recently_views` (`id`, `service_id`, `user_id`, `created_at`) VALUES
(2, 1, 10, '2021-12-30 11:06:11'),
(3, 22, 1, '2021-12-30 11:28:59'),
(4, 21, 1, '2021-12-30 11:29:21'),
(5, 21, 2, '2021-12-30 11:30:48'),
(6, 22, 2, '2021-12-30 11:30:51'),
(7, 1, 2, '2021-12-30 11:30:55'),
(8, 1, 3, '2021-12-30 11:31:11'),
(9, 22, 3, '2021-12-30 11:31:19'),
(10, 22, 4, '2021-12-30 11:34:57'),
(11, 1, 1, '2021-12-30 12:11:17'),
(12, 1, 1, '2021-12-30 12:38:34'),
(13, 1, 1, '2021-12-30 12:49:41'),
(14, 1, 1, '2021-12-30 12:49:46'),
(15, 1, 1, '2021-12-30 14:02:48');

-- --------------------------------------------------------

--
-- Table structure for table `screens`
--

CREATE TABLE `screens` (
  `id` int(11) NOT NULL,
  `screen_code` varchar(50) NOT NULL,
  `screen_name` varchar(191) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `screens`
--

INSERT INTO `screens` (`id`, `screen_code`, `screen_name`) VALUES
(1, 'web.general', 'web.general');

-- --------------------------------------------------------

--
-- Table structure for table `service`
--

CREATE TABLE `service` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `title` longtext DEFAULT NULL,
  `description` longtext DEFAULT NULL,
  `sub_category_id` longtext DEFAULT NULL,
  `deliver_time` text DEFAULT NULL,
  `details_for_customer` longtext DEFAULT NULL,
  `image` text DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `media_url` text DEFAULT NULL,
  `youtube_url` text DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `service`
--

INSERT INTO `service` (`id`, `user_id`, `title`, `description`, `sub_category_id`, `deliver_time`, `details_for_customer`, `image`, `price`, `media_url`, `youtube_url`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'Test title1', 'Test Description1', '1', '4', 'Test Details', '6337-4.jpg', 120, NULL, NULL, '2021-11-01 09:22:04', NULL, NULL),
(2, 3, 'Test title2', 'Test Description2', '6', '4', 'Test Details', '6337-4.jpg', 150, NULL, NULL, '2021-11-16 09:22:09', NULL, NULL),
(3, 2, 'Test 3 title', 'Test Description3', '6', '4', 'Test Details', '6337-4.jpg', 200, NULL, NULL, '2021-11-17 09:22:09', NULL, NULL),
(20, 4, 'عنوان الخدمة', 'Tersadasd', '6', '15', 'تعليمات للمشتري', 'e2fe59de0af0eb105e7beb49818910f2.jpeg', 10, NULL, NULL, '2021-11-27 09:42:34', '2021-11-27 09:42:34', NULL),
(21, 4, 'Aut necessitatibus e', 'Non sint aliquam acc', '6', '4', 'Minim qui sit neque', '793a1b4c3fcaca4387a5106c04e06694.jpg', 32, NULL, NULL, '2021-11-27 10:06:06', '2021-11-27 10:06:06', NULL),
(22, 21, 'Test', 'TEst', '1', NULL, NULL, 'C:\\Users\\Nabeel Hamza\\AppData\\Local\\Temp\\php70FA.tmp', 12, NULL, NULL, '2021-12-30 05:31:38', NULL, NULL),
(23, 21, 'Test', 'TEst', '1', NULL, NULL, 'C:\\Users\\Nabeel Hamza\\AppData\\Local\\Temp\\phpF321.tmp', 12, NULL, NULL, '2021-12-30 05:34:23', NULL, NULL),
(24, 21, 'Test', 'TEst', '1', NULL, NULL, 'C:\\Users\\Nabeel Hamza\\AppData\\Local\\Temp\\php4A9A.tmp', 12, NULL, NULL, '2021-12-30 05:38:02', NULL, NULL),
(25, 21, 'Test', 'TEst', '1', NULL, NULL, 'phpDC7B.tmp', 12, NULL, NULL, '2021-12-30 05:38:39', NULL, NULL),
(26, 21, 'Test', 'TEst', '1', NULL, NULL, '/tmp/phpMZkZqh', 12, NULL, NULL, '2021-12-30 12:10:45', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `service_comments`
--

CREATE TABLE `service_comments` (
  `id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `comment` longtext DEFAULT NULL,
  `rate` text DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `service_comments`
--

INSERT INTO `service_comments` (`id`, `service_id`, `user_id`, `comment`, `rate`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 21, 3, 'خدمة ممتازة شكرا جزيلا، سيكون بيننا تعامل لاحقاَ ان شاء الله\r\n', '4', '2021-11-27 12:06:04', NULL, NULL),
(2, 21, 3, 'test comment', '4', '2021-11-27 12:06:04', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `service_questions`
--

CREATE TABLE `service_questions` (
  `id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  `question` longtext DEFAULT NULL,
  `answer` longtext DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `service_questions`
--

INSERT INTO `service_questions` (`id`, `service_id`, `question`, `answer`, `created_at`, `updated_at`, `deleted_at`) VALUES
(12, 20, 'test questtion', 'test answer', '2021-11-27 09:42:34', '2021-11-27 09:42:34', NULL),
(13, 21, 'Ratione in non sint', 'Cumque qui cillum ve', '2021-11-27 10:06:06', '2021-11-27 10:06:06', NULL),
(14, 22, 'Test?', NULL, '2021-12-30 05:31:38', NULL, NULL),
(15, 23, 'Test?', NULL, '2021-12-30 05:34:23', NULL, NULL),
(16, 25, 'Test?', NULL, '2021-12-30 05:38:39', NULL, NULL),
(17, 26, 'Test?', NULL, '2021-12-30 12:10:45', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `service_tags`
--

CREATE TABLE `service_tags` (
  `id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  `tag` text DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `service_tags`
--

INSERT INTO `service_tags` (`id`, `service_id`, `tag`, `created_at`, `updated_at`, `deleted_at`) VALUES
(21, 20, 'test', '2021-11-27 09:42:34', '2021-11-27 09:42:34', NULL),
(22, 21, 'Distinctio Quia rep', '2021-11-27 10:06:06', '2021-11-27 10:06:06', NULL),
(23, 22, NULL, '2021-12-30 05:31:38', NULL, NULL),
(24, 22, NULL, '2021-12-30 05:31:38', NULL, NULL),
(25, 22, NULL, '2021-12-30 05:31:38', NULL, NULL),
(26, 23, NULL, '2021-12-30 05:34:23', NULL, NULL),
(27, 23, NULL, '2021-12-30 05:34:23', NULL, NULL),
(28, 23, NULL, '2021-12-30 05:34:23', NULL, NULL),
(29, 25, 'first', '2021-12-30 05:38:39', NULL, NULL),
(30, 25, 'second', '2021-12-30 05:38:39', NULL, NULL),
(31, 25, 'third', '2021-12-30 05:38:39', NULL, NULL),
(32, 26, 'first', '2021-12-30 12:10:45', NULL, NULL),
(33, 26, 'second', '2021-12-30 12:10:45', NULL, NULL),
(34, 26, 'third', '2021-12-30 12:10:45', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sub_category`
--

CREATE TABLE `sub_category` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `name_en` text DEFAULT NULL,
  `name_ar` text DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sub_category`
--

INSERT INTO `sub_category` (`id`, `category_id`, `name_en`, `name_ar`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'Test English', 'تست عربي', '2021-11-15 18:18:42', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE `tags` (
  `id` int(11) NOT NULL,
  `name_ar` text DEFAULT NULL,
  `name_en` text DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_type` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `service_owner` int(11) DEFAULT NULL,
  `country` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `education` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` int(11) DEFAULT NULL,
  `deactivation_reason` text CHARACTER SET utf8 DEFAULT NULL,
  `deactivation_note` text CHARACTER SET utf8 DEFAULT NULL,
  `image` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `certificate_image` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `university` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `work_title` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `graduation_date` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `login_at` timestamp NULL DEFAULT NULL,
  `logout_at` timestamp NULL DEFAULT NULL,
  `social_provider_id` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `social_provider` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `device_key` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_verified` int(11) DEFAULT NULL,
  `otp` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `messages_push_notifications` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `user_type`, `service_owner`, `country`, `description`, `education`, `active`, `deactivation_reason`, `deactivation_note`, `image`, `certificate_image`, `university`, `work_title`, `graduation_date`, `remember_token`, `login_at`, `logout_at`, `social_provider_id`, `social_provider`, `device_key`, `is_verified`, `otp`, `messages_push_notifications`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'محمد الشوربجي', 'admin@admin', NULL, '$2y$10$gJ9olG/YdtNiKhoBa8tWL.qxvSn3OvUM94IEjSL869g2UWf1ogW4C', NULL, 1, 'United emiarte', 'نبذة قصيرة عن اللمستخدم', 'بكالوريوس - شبكات', 1, 'لدي حساب آخر على الموقع', 'test', '27196f37e3b12cc88510936a5ea942cf.png', '931425acef0aa60b31c5ded53ebd99e4.png', 'خليفة', 'Web developer', '2023-04-27', NULL, NULL, '2021-12-20 13:50:29', NULL, NULL, 'fzmpu4_cIXHC37lh9A6YN7:APA91bH4-FWP8YMKSrqxWHXjy2ejxI2xHQCpO7GdrCpyagyy0du12PQpxmUVCxCU8o1bxXyYlW_kGYT_iXjlbvGApjtyzRUYZTSaONR7McWDvNmvkAIVGb993SEVWKurVwSYXpXyNAcr', 1, NULL, NULL, '2021-11-10 18:01:04', '2022-01-03 18:01:09', NULL),
(2, 'Mostafa', 'admin@freelancer.ae3', NULL, '$2y$10$phGX5uBm54XbYPYhgrS9Aufvqb3SyANApHwbC8brVIxaBn2vtYx8e', NULL, 0, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-12-01 17:20:19', NULL, NULL, 'dB1H5R-3-yolggQ8OipqvI:APA91bGFAHagn2CWpJzreDC2SqU2zOOMXDJK1hXw55jXhr71C5_Jn9-V8S-w4NAaSQmQ-sbPzuANhVNzJZN3psjfTGI3xsCUFUWGJ8B2tVDRVhuY-nKEi7ltyxAuctw7leKsztf9mfxs', NULL, NULL, NULL, '2021-11-13 19:25:28', '2021-12-01 17:20:19', NULL),
(3, 'test User', 'admin@freelancer.ae15', NULL, '$2y$10$XLnEIKU1F27YMZYNRqGOm.yLBJpFeadOvK8VN48IX/8UJbphD.216', NULL, 0, 'Palestine', 'test desc', 'test', 1, NULL, NULL, '3bc372b7c468dfec32dd7d2d35f85aab.png', '0c6566eb2a82d9388bdbd834997c08e5.png', 'test', 'Test jon', '2021-11-03', NULL, NULL, '2021-12-20 14:02:39', NULL, NULL, 'fHkEw7-V6OcnMu12HR46XD:APA91bGQUNPb6XERJyXD1ssnXd6-XxieRC_3zc6Un_2pz9kP2pH2R1F6VzLMENMHw4quUYeySnzrcSRSIvrRSi9KWiVqJJXe2YR8rSwljbngyWnGtxwaq94mr84dYhgb2iExUBMXR8h9', 1, NULL, NULL, '2021-11-13 19:26:43', '2021-12-20 14:02:39', NULL),
(4, 'freelancer4', 'admin@freelancer.ae32', NULL, '$2y$10$BQ.wWg5bhr6PG2HxDDCL1ubOpoSzqLyBg2Am80ADwR4rK.wVHCHd6', NULL, 0, 'test', 'desc desc desc desc desc desc desc', 'test', 1, NULL, NULL, '85220eeeeb11e7df0ee3824b1612a786.png', '7d6d0cfc0e6b3c50a1f1b26511328c01.png', 'test', 'job', '2021-12-02', NULL, NULL, '2021-12-20 06:57:07', NULL, NULL, 'fOEcgx6NUedTu1g7JmmY1B:APA91bECskhzgOstUm3Njx0gDTQ-ZeG_9Y9AuS-o5xNMyXQaCCGGeDhJeRGDBLH1P3j1jFg8eGQRYUVTwNqIoY-7BKd-8ssSbprUOjRYw715SmoJ29uzb9ZthHJZjxPo4r9KS5A-2D1n', 1, NULL, NULL, '2021-11-13 19:28:08', '2021-12-20 23:24:57', NULL),
(5, 'freelancer5', 'asdadmin@freelancer.ae', NULL, '$2y$10$y45zuyi9h91n4xKytlRgsO9hnPtrP7.eyrE.Y3lhOI9l37DAZToe2', NULL, 0, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-12-13 16:45:51', NULL, NULL, 'dUiXOdRFOv2IrNh1FlFHPe:APA91bEjal2Pc6TTWUkquNFzu7xD2PZEpo_6amXflgEaqKwafxAs1UXTbCZWWCQ3VHJTx_RTiD6ZtlNVf1F3wy2dGvg8wV4oy7_aqMEoDzBMGGWXVPotCJlhFTNHNfC2pTUKlz53by15', 1, NULL, NULL, '2021-11-13 19:29:36', '2021-12-13 16:45:51', NULL),
(6, 'تثاشي', 'admin@freelancer.ae10', NULL, '$2y$10$NsvttKC4OH1X33UJNbcY8er//EPiXeWCBHCytf5PhTGIxSFWo2RYC', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-11-13 19:49:02', '2021-11-13 19:49:02', NULL),
(7, 'asdasdsad', 'admin1@freelancer.ae', NULL, '$2y$10$JdWbpPYRNE7y9j3kAlec2.Xef3AgKDuPaMQPEm1SoL2j7PRQ9vV92', '2', 0, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-12-01 20:45:33', NULL, NULL, 'czvcHytoDXjE4N3U1IXQfN:APA91bHlC4W437KmOKtdKl4teCUYoQdAJQFVBajqQQa0IqZSkLdha-dliUStg4rmyCnonO__iecD961dWdvFRtH4LeRIrPnMvCpdME6AduN1XyYfox9JBc3g-NgnGtniw9IR6u_CkP4u', NULL, NULL, NULL, '2021-11-13 19:50:13', '2021-12-01 20:45:33', NULL),
(8, 'asdasd', 'adasmin@freelancer.ae', NULL, '$2y$10$SgrVOzbeVlHfPiI5OLDvr.8GNLfqRtlbH0b3PT.aFZUjS.hVfZm62', '2', 1, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-11-13 19:51:32', '2021-11-13 19:51:32', NULL),
(10, 'Mohammed', 'admin@ucas.ae', NULL, '$2y$10$Dx03t../UKNJJSG1Euwe7eUx5DpLbTQIbYSr2lCqAJ3LDX1tAPv2K', '2', 2, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'f1DGJ12ZfQfO9Jmgq8FaCj:APA91bGyU3Zd_IBExAMS_27qatFLBnfokbaN25uFY9oVWlM54yXnMbPQkXrqJx5Zx0ULzmdN3dG9azYaeoDf5G6uYfqiCk03H7h5gQ0tlgGScCNzYmQJAqQotCJerG2lGDwLLBOPAp7M', NULL, NULL, NULL, '2021-12-04 11:45:49', '2021-12-04 11:45:50', NULL),
(11, 'joj', 'hoh@hoh', NULL, '$2y$10$6dJCgnLpHAG7fACpTR7Bsua36j6BKMSofp.zoS7lo8PPAUINba/d.', '2', 2, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'f1DGJ12ZfQfO9Jmgq8FaCj:APA91bGyU3Zd_IBExAMS_27qatFLBnfokbaN25uFY9oVWlM54yXnMbPQkXrqJx5Zx0ULzmdN3dG9azYaeoDf5G6uYfqiCk03H7h5gQ0tlgGScCNzYmQJAqQotCJerG2lGDwLLBOPAp7M', NULL, NULL, NULL, '2021-12-04 11:49:39', '2021-12-04 11:49:39', NULL),
(12, 'joj', 'wadasdasd@asdasd', NULL, '$2y$10$f0jUUjzh6w83WqUkPU/vduxOeYaJlBxszUSZqJdVqDU9nfHj0zwF.', '2', 0, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-12-04 12:01:24', NULL, NULL, 'f1DGJ12ZfQfO9Jmgq8FaCj:APA91bGyU3Zd_IBExAMS_27qatFLBnfokbaN25uFY9oVWlM54yXnMbPQkXrqJx5Zx0ULzmdN3dG9azYaeoDf5G6uYfqiCk03H7h5gQ0tlgGScCNzYmQJAqQotCJerG2lGDwLLBOPAp7M', NULL, NULL, NULL, '2021-12-04 11:50:38', '2021-12-04 12:01:24', NULL),
(13, 'asdasd', 'ASDAS@asdasd', NULL, '$2y$10$06uToH.Uoxtc1WcpNLwHTulRSb78kOX68kTBxqdAxSmDGXxUEEw6y', '2', 2, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'f1DGJ12ZfQfO9Jmgq8FaCj:APA91bGyU3Zd_IBExAMS_27qatFLBnfokbaN25uFY9oVWlM54yXnMbPQkXrqJx5Zx0ULzmdN3dG9azYaeoDf5G6uYfqiCk03H7h5gQ0tlgGScCNzYmQJAqQotCJerG2lGDwLLBOPAp7M', NULL, NULL, NULL, '2021-12-04 12:01:34', '2021-12-04 12:01:35', NULL),
(14, '@admin', 'admin@ads', NULL, '$2y$10$1b59qluWIzczD43mE5QVx.new7PxGtcr5hsRKG5PY1twl/Y7HNwBG', '2', 0, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-12-04 13:06:11', NULL, NULL, 'cSyAF_sVGbyy0gFMLV-2Ik:APA91bF-k2WuBLVVZ8vG5hVXf7-Xj9WB0Nfnm8uTVu5FY5C_svc-d3_pK7XA0otIYk4LPRm8H24VHMvW6aXFZi0WQCn3rqWAoLwtGsIgTBDAD3_ZQ0RH1mRytwbbpFVbK-ASs1TzgQ5W', NULL, NULL, NULL, '2021-12-04 13:05:41', '2021-12-04 13:06:11', NULL),
(15, 'adadad', 'admin@admin.com', NULL, '$2y$10$eu1C4nFopg1ejF4hMr7S7.CY2u2M9brjsY2hndBqn1uRREizmCC9.', '2', 0, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-12-05 15:14:02', NULL, NULL, 'ckIzpU3o8K8er5zIEn8YIJ:APA91bF8sVP3eN87LdUHnbgKCdEcj0oVo6R_BgMTPOvtRR8XPw_tNYUu-tzrp0X8kUI06WsxQz73YyMNXo0P0-uFjEtVsKqxKDLq0J5tiie-AIHOFCe8JYLHwKLAnip2PaxX-RcKfI2I', NULL, NULL, NULL, '2021-12-05 15:12:55', '2021-12-05 15:14:02', NULL),
(16, 'adadad', 'asdas@ADAS', NULL, '$2y$10$XMOSXbwkIQ7rnzBIqDwbn.jdMKZ9MOywsAtS4Xu5Muj4m6nNePWLa', '2', 0, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-12-05 15:22:55', NULL, NULL, 'ckIzpU3o8K8er5zIEn8YIJ:APA91bF8sVP3eN87LdUHnbgKCdEcj0oVo6R_BgMTPOvtRR8XPw_tNYUu-tzrp0X8kUI06WsxQz73YyMNXo0P0-uFjEtVsKqxKDLq0J5tiie-AIHOFCe8JYLHwKLAnip2PaxX-RcKfI2I', NULL, NULL, NULL, '2021-12-05 15:14:11', '2021-12-05 15:22:55', NULL),
(17, 'adadad', 'asdasd@ASASs', NULL, '$2y$10$IbdezqM4NqC72BlIGD0wsOOao/xyaeZwClKlG.q0/KqekLslkJL.G', '2', 2, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-12-05 15:24:44', '2021-12-05 15:24:44', NULL),
(18, 'Admin', 'admin@gmail.com', NULL, '$2y$10$QmYz7ygmp7efzR2QYo7GtOrpUJsKH20cb1FE7o9DxS8Tl8tS6H.2K', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-12-24 12:35:34', '2021-12-24 12:35:34', NULL),
(19, 'Admin', 'admin1@gmail.com', NULL, '$2y$10$oftn8v.p7nZEvHg64d6MZeX4K9jQFQ7Cpl1K75OGvq8tywLcVBi5O', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-12-27 17:11:25', '2021-12-27 17:11:25', NULL),
(20, 'user7672', '114757066666163887927', NULL, '$2y$10$shrCt50DGDaI2Aibd7AxiuyRrFvGtCQkU8iPKy.1yLx/zmz2UfZHu', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '114757066666163887927', 'facebook', NULL, NULL, NULL, NULL, '2021-12-30 13:11:29', '2021-12-30 13:11:29', NULL),
(21, 'user5905', '1147570666661638879271', NULL, '$2y$10$Oi.y8mtevw44uYA/5LMUke1R.M0EXdzC51r9i4GwW3enOdP23mYNi', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1147570666661638879271', 'facebook', NULL, NULL, NULL, NULL, '2021-12-30 13:11:41', '2021-12-30 13:11:41', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_languages`
--

CREATE TABLE `user_languages` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `language` text DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user_tags`
--

CREATE TABLE `user_tags` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `tag` text DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_tags`
--

INSERT INTO `user_tags` (`id`, `user_id`, `tag`, `created_at`, `updated_at`, `deleted_at`) VALUES
(12, 1, 'asdasd', '2021-11-20 17:18:22', '2021-11-20 17:18:40', '2021-11-20 17:18:40'),
(13, 1, 'ddd', '2021-11-20 17:18:40', '2021-11-20 17:19:00', '2021-11-20 17:19:00'),
(14, 1, 'tt', '2021-11-20 17:18:40', '2021-11-20 17:19:00', '2021-11-20 17:19:00'),
(15, 1, 'ddd', '2021-11-20 17:19:00', '2021-11-20 17:20:29', '2021-11-20 17:20:29'),
(16, 1, 'tt', '2021-11-20 17:19:00', '2021-11-20 17:20:29', '2021-11-20 17:20:29'),
(17, 1, 'oo', '2021-11-20 17:19:00', '2021-11-20 17:20:29', '2021-11-20 17:20:29'),
(18, 1, 'ddd', '2021-11-20 17:20:29', '2021-11-20 17:20:50', '2021-11-20 17:20:50'),
(19, 1, 'tt', '2021-11-20 17:20:29', '2021-11-20 17:20:50', '2021-11-20 17:20:50'),
(20, 1, 'oo', '2021-11-20 17:20:29', '2021-11-20 17:20:50', '2021-11-20 17:20:50'),
(21, 1, 'ddd', '2021-11-20 17:20:50', '2021-11-20 17:22:07', '2021-11-20 17:22:07'),
(22, 1, 'tt', '2021-11-20 17:20:50', '2021-11-20 17:22:07', '2021-11-20 17:22:07'),
(23, 1, 'oo', '2021-11-20 17:20:50', '2021-11-20 17:22:07', '2021-11-20 17:22:07'),
(24, 1, 'ddd', '2021-11-20 17:22:07', '2021-11-20 17:23:17', '2021-11-20 17:23:17'),
(25, 1, 'tt', '2021-11-20 17:22:07', '2021-11-20 17:23:17', '2021-11-20 17:23:17'),
(26, 1, 'oo', '2021-11-20 17:22:07', '2021-11-20 17:23:17', '2021-11-20 17:23:17'),
(27, 1, 'ddd', '2021-11-20 17:23:17', '2021-11-20 18:20:17', '2021-11-20 18:20:17'),
(28, 1, 'tt', '2021-11-20 17:23:17', '2021-11-20 18:20:17', '2021-11-20 18:20:17'),
(29, 1, 'oo', '2021-11-20 17:23:17', '2021-11-20 18:20:17', '2021-11-20 18:20:17'),
(30, 1, 'ddd', '2021-11-20 18:20:18', '2021-11-21 12:57:17', '2021-11-21 12:57:17'),
(31, 1, 'tt', '2021-11-20 18:20:18', '2021-11-21 12:57:17', '2021-11-21 12:57:17'),
(32, 1, 'oo', '2021-11-20 18:20:18', '2021-11-21 12:57:17', '2021-11-21 12:57:17'),
(33, 1, 'ddd', '2021-11-21 12:57:17', '2021-11-21 12:57:33', '2021-11-21 12:57:33'),
(34, 1, 'tt', '2021-11-21 12:57:17', '2021-11-21 12:57:33', '2021-11-21 12:57:33'),
(35, 1, 'oo', '2021-11-21 12:57:17', '2021-11-21 12:57:33', '2021-11-21 12:57:33'),
(36, 1, 'تصميم', '2021-11-21 12:57:33', '2021-11-21 13:13:56', '2021-11-21 13:13:56'),
(37, 1, 'تصميم', '2021-11-21 13:13:56', '2021-11-21 13:14:12', '2021-11-21 13:14:12'),
(38, 1, 'تصميم', '2021-11-21 13:14:12', '2021-11-21 13:15:50', '2021-11-21 13:15:50'),
(39, 1, 'تصميم', '2021-11-21 13:15:50', '2021-11-21 13:16:04', '2021-11-21 13:16:04'),
(40, 1, 'تصميم', '2021-11-21 13:16:04', '2021-11-21 13:16:15', '2021-11-21 13:16:15'),
(41, 1, 'تصميم', '2021-11-21 13:16:15', '2021-11-21 13:16:39', '2021-11-21 13:16:39'),
(42, 1, 'تصميم', '2021-11-21 13:16:39', '2021-11-21 13:16:46', '2021-11-21 13:16:46'),
(43, 1, 'تصميم', '2021-11-21 13:16:46', '2021-11-21 13:17:22', '2021-11-21 13:17:22'),
(44, 1, 'تصميم', '2021-11-21 13:17:22', '2021-11-21 13:17:38', '2021-11-21 13:17:38'),
(45, 1, 'تصميم', '2021-11-21 13:17:38', '2021-11-21 13:18:41', '2021-11-21 13:18:41'),
(46, 1, 'تصميم', '2021-11-21 13:18:41', '2021-11-22 13:05:47', '2021-11-22 13:05:47'),
(47, 1, 'تصميم', '2021-11-22 13:05:47', '2021-11-22 13:07:32', '2021-11-22 13:07:32'),
(48, 1, 'مصمم شبكات', '2021-11-22 13:05:47', '2021-11-22 13:07:32', '2021-11-22 13:07:32'),
(49, 1, 'تصميم', '2021-11-22 13:07:32', '2021-12-08 15:11:32', '2021-12-08 15:11:32'),
(50, 1, 'مصمم شبكات', '2021-11-22 13:07:32', '2021-12-08 15:11:32', '2021-12-08 15:11:32'),
(51, 3, 'test', '2021-11-22 13:10:26', '2021-11-22 13:10:26', NULL),
(52, 1, 'تصميم', '2021-12-08 15:11:32', '2021-12-08 15:11:51', '2021-12-08 15:11:51'),
(53, 1, 'مصمم شبكات', '2021-12-08 15:11:32', '2021-12-08 15:11:51', '2021-12-08 15:11:51'),
(54, 1, 'تصميم', '2021-12-08 15:11:51', '2021-12-08 15:11:51', NULL),
(55, 1, 'مصمم شبكات', '2021-12-08 15:11:51', '2021-12-08 15:11:51', NULL),
(56, 4, 'test', '2021-12-10 18:32:06', '2021-12-10 18:32:06', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `additional_services`
--
ALTER TABLE `additional_services`
  ADD PRIMARY KEY (`id`),
  ADD KEY `service_id` (`service_id`);

--
-- Indexes for table `carts`
--
ALTER TABLE `carts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `carts_user_id_foreign` (`user_id`),
  ADD KEY `carts_service_id_foreign` (`service_id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comments_reply`
--
ALTER TABLE `comments_reply`
  ADD PRIMARY KEY (`id`),
  ADD KEY `comment_id` (`comment_id`),
  ADD KEY `service_id` (`service_id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `fav_services`
--
ALTER TABLE `fav_services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `general_questions`
--
ALTER TABLE `general_questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `home_slider`
--
ALTER TABLE `home_slider`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `labels`
--
ALTER TABLE `labels`
  ADD PRIMARY KEY (`id`),
  ADD KEY `language_id` (`language_id`),
  ADD KEY `screen_id` (`screen_id`);

--
-- Indexes for table `languages`
--
ALTER TABLE `languages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `notifications_notifiable_type_notifiable_id_index` (`notifiable_type`,`notifiable_id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_items`
--
ALTER TABLE `order_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `service_id` (`service_id`),
  ADD KEY `order_id` (`order_id`),
  ADD KEY `user_owner_service_id` (`user_owner_service_id`);

--
-- Indexes for table `order_movements_item`
--
ALTER TABLE `order_movements_item`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `recently_views`
--
ALTER TABLE `recently_views`
  ADD PRIMARY KEY (`id`),
  ADD KEY `service_id` (`service_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `screens`
--
ALTER TABLE `screens`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `service`
--
ALTER TABLE `service`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `service_comments`
--
ALTER TABLE `service_comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `service_id` (`service_id`);

--
-- Indexes for table `service_questions`
--
ALTER TABLE `service_questions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `service_id` (`service_id`);

--
-- Indexes for table `service_tags`
--
ALTER TABLE `service_tags`
  ADD PRIMARY KEY (`id`),
  ADD KEY `service_id` (`service_id`);

--
-- Indexes for table `sub_category`
--
ALTER TABLE `sub_category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_id` (`category_id`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_languages`
--
ALTER TABLE `user_languages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_tags`
--
ALTER TABLE `user_tags`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `additional_services`
--
ALTER TABLE `additional_services`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `carts`
--
ALTER TABLE `carts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `comments_reply`
--
ALTER TABLE `comments_reply`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fav_services`
--
ALTER TABLE `fav_services`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `general_questions`
--
ALTER TABLE `general_questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `home_slider`
--
ALTER TABLE `home_slider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `labels`
--
ALTER TABLE `labels`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=259;

--
-- AUTO_INCREMENT for table `languages`
--
ALTER TABLE `languages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `order_items`
--
ALTER TABLE `order_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `order_movements_item`
--
ALTER TABLE `order_movements_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `recently_views`
--
ALTER TABLE `recently_views`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `screens`
--
ALTER TABLE `screens`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `service`
--
ALTER TABLE `service`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `service_comments`
--
ALTER TABLE `service_comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `service_questions`
--
ALTER TABLE `service_questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `service_tags`
--
ALTER TABLE `service_tags`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `sub_category`
--
ALTER TABLE `sub_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tags`
--
ALTER TABLE `tags`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `user_languages`
--
ALTER TABLE `user_languages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_tags`
--
ALTER TABLE `user_tags`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `additional_services`
--
ALTER TABLE `additional_services`
  ADD CONSTRAINT `additional_services_ibfk_1` FOREIGN KEY (`service_id`) REFERENCES `service` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `carts`
--
ALTER TABLE `carts`
  ADD CONSTRAINT `carts_service_id_foreign` FOREIGN KEY (`service_id`) REFERENCES `service` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `carts_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `comments_reply`
--
ALTER TABLE `comments_reply`
  ADD CONSTRAINT `comments_reply_ibfk_1` FOREIGN KEY (`comment_id`) REFERENCES `service_comments` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `comments_reply_ibfk_2` FOREIGN KEY (`service_id`) REFERENCES `service` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `labels`
--
ALTER TABLE `labels`
  ADD CONSTRAINT `labels_ibfk_1` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `labels_ibfk_2` FOREIGN KEY (`screen_id`) REFERENCES `screens` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `order_items`
--
ALTER TABLE `order_items`
  ADD CONSTRAINT `order_items_ibfk_2` FOREIGN KEY (`service_id`) REFERENCES `service` (`id`),
  ADD CONSTRAINT `order_items_ibfk_3` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`),
  ADD CONSTRAINT `order_items_ibfk_4` FOREIGN KEY (`user_owner_service_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `recently_views`
--
ALTER TABLE `recently_views`
  ADD CONSTRAINT `recently_views_ibfk_1` FOREIGN KEY (`service_id`) REFERENCES `service` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `service_comments`
--
ALTER TABLE `service_comments`
  ADD CONSTRAINT `service_comments_ibfk_1` FOREIGN KEY (`service_id`) REFERENCES `service` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `service_questions`
--
ALTER TABLE `service_questions`
  ADD CONSTRAINT `service_questions_ibfk_1` FOREIGN KEY (`service_id`) REFERENCES `service` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `service_tags`
--
ALTER TABLE `service_tags`
  ADD CONSTRAINT `service_tags_ibfk_2` FOREIGN KEY (`service_id`) REFERENCES `service` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `sub_category`
--
ALTER TABLE `sub_category`
  ADD CONSTRAINT `sub_category_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
