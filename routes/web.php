<?php


use App\Http\Controllers\Admin\Local\LocalController;
use App\Http\Controllers\User\Fav\FavController;
use App\Http\Controllers\Web\Auth\UserLoginController;
use App\Http\Controllers\Web\Auth\UserRegistersController;
use App\Http\Controllers\Web\Cart\CartController;
use App\Http\Controllers\Web\Contact\ContactController;
use App\Http\Controllers\Web\FcmNotifications\FcmNotifications;
use App\Http\Controllers\Web\Filter\FilterController;
use App\Http\Controllers\Web\Notifications\NotificationsController;
use App\Http\Controllers\Web\Orders\OrdersController;
use App\Http\Controllers\Web\Packages\PackagesController;
use App\Http\Controllers\Web\Payment\PaymentController;
use App\Http\Controllers\Web\Profile\ProfileController;
use App\Http\Controllers\Web\Seller\SellerController;
use App\Http\Controllers\Web\Services\ServiceController;
use App\Http\Controllers\Web\Settings\SettingsController;
use App\Http\Controllers\Web\SocialAuth\SocialAuthController;
use App\Imports\SubCategoryImport;
use App\Mail\Web\VerifyCodeEmail;
use App\Models\Packages\Packages;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Route;
use Maatwebsite\Excel\Facades\Excel;
use Stripe\Checkout\Session;
use Stripe\Stripe;
use Stripe\StripeClient;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//انا ضفتو متمسحوش الا باذني
Route::get('paymenttest', function () {

//      \Illuminate\Support\Facades\Log::emergency("test message");
//      \Illuminate\Support\Facades\Log::channel('telegram')->emergency("test message");

//    \Illuminate\Support\Facades\Log::debug('test error');
//    dd('success');
    $order = \App\Models\Order\Order::latest()->first();
     $response= [];
/*    foreach ($order->toArray() as $key=>$value){
        if (in_array($key,['total_price','provider_amount','system_commission','payment_tax'])){
            $response[$key] = current_value_in_currency($value);
        }

    }*/

//    dd(1);



    dd($response);

    session()->put('current_currency', 'AED');
    dd(current_value_in_currency(100,true));

    return view('web.paymenttest');
});
Route::get('carttt', function () {
    return view('web.carttt');
})->name('asdasdasd');
Route::get('cat', function () {
    return view('web.subcat');
});
//Route::get('newprice', [PackagesController::class, 'index'])->name('packages.index');



Route::get('developer/translate_controller_messages', [\App\Http\Controllers\DeveloperController::class,'translate_controller_messages']);
Route::get('developer/translate_resources_messages', [\App\Http\Controllers\DeveloperController::class,'translate_resources_messages']);

Auth::routes(['register' => false]);

Route::get('export_label', function () {
//    return \Maatwebsite\Excel\Facades\Excel::download(new \App\Exports\LabelsExport(), 'labels_ar.xlsx');
//
//    \Maatwebsite\Excel\Facades\Excel::import(new \App\Imports\LabelImport(), public_path('labels_filipino.xlsx'));
//    \Maatwebsite\Excel\Facades\Excel::import(new \App\Imports\LabelImport(), public_path('labels_Hindi.xlsx'));
//    \Maatwebsite\Excel\Facades\Excel::import(new \App\Imports\LabelImport(), public_path('labels_languages_en.xlsx'));
//    \Maatwebsite\Excel\Facades\Excel::import(new \App\Imports\LabelImport(), public_path('Urdu_Translation.xlsx'));
    if ($dir = opendir(public_path('new')))
    {

        $subcategories =  \App\Models\Category\SubCategory::get();
        $not_exisit_items  = [] ;
        while (($file = readdir($dir)) !== false)
        {

            if (in_array($file,['.','..'])) continue;
            $int_id = abs((int) filter_var($file, FILTER_SANITIZE_NUMBER_INT));


            echo "<p>".$file . $int_id."</p>";

            try {

                if ($int_id>=7) $int_id+=7;
                 $sub = $subcategories->where('id', $int_id)->first();

                 if ($sub)
                    $sub->update(['image'=>$file]);
                else{
                    $not_exisit_items[]=[
                        'id'=>$int_id,
                        '$file'=>$file,
                    ];
                }
//                dd($sub , $int_id);
            }catch (Exception $exception){

                dd($int_id, $sub);
            }


        }
        closedir($dir);
    }
    dd('success' , $not_exisit_items);

    return view('web.testprice');
});
Route::get('import_sub_category2', function () {

   $response=  Http::get("https://ipinfo.io");;

   dd($response->json());

    $details = ['title' => 'Email Verification Code', 'body' => "888"];

    Mail::to("elassoulidev@gmail.com")->send(new VerifyCodeEmail($details));


    dd('sucess');


//
//    dd('success to add  data');
////    return view('web.testprice');
});
Route::get('test_stripe', function () {
    $stripe = new StripeClient(
        env('STRIPE_SECRET')
    );
    $account_id = $stripe->accounts->create(['type' => 'express',
//        'country' => 'US',
        'email' => 'elassoulidev@gmail.com',
//        'capabilities' => [
//            'card_payments' => ['requested' => true],
//            'transfers' => ['requested' => true],
//        ],
    ]);

    $account_c = $stripe->accounts->allCapabilities($account_id->id);


    dd($account_c);


//    $count_link = $stripe->tokens->create(
//        [
//            'account' => 'acct_1032D82eZvKYlo2C',
//            'refresh_url' => url('reauth'),
//            'return_url' => url('return'),
//            'type' => 'account_onboarding',
//        ]
//    );

    /*    $count_link = $stripe->accountLinks->create(
            [
                'account' => 'acct_1032D82eZvKYlo2C',
                'refresh_url' => url('reauth'),
                'return_url' => url('return'),
                'type' => 'account_onboarding',
            ]
        );*/


    dd($count_link);
    $this->customer_stripe_account = $account_id->id;
//    $this->save();
    /*   $account_link  =  $stripe->accountLinks->create(
            [
                'account' => $this->customer_stripe_account,
                'refresh_url' => 'https://example.com/reauth',
                'return_url' => 'https://example.com/return',
                'type' => 'account_onboarding',
            ]
        );*/


    \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
    $product = $stripe->products->create([
        'name' => 'Gold Special',


    ]);
    $price = $stripe->prices->create([
        'unit_amount' => 2000,
        'currency' => 'aed',
        'recurring' => ['interval' => 'month'],
        'product' => $product->id,
    ]);

    /*
    $session = \Stripe\Checkout\Session::create([
        'payment_method_types' => ['card'],
        'line_items' => [[
            'name' => 'Stainless Steel Water Bottle',
            'amount' => 1000,
            'currency' => 'aed',
            'quantity' => 1,
        ]],
        'payment_intent_data' => [
            'application_fee_amount' => 123,
        ],
        'mode' => 'payment',
        'success_url' => url('success'),
        'cancel_url' => url('failure')  ,
    ], ['stripe_account' =>  $account_id->id]);*/
    $session = \Stripe\Checkout\Session::create([
        'line_items' => [[
            'price' => $price->id,
            'quantity' => 1,
        ]],
        /*
             'line_items' => [[
                 'name' => 'Stainless Steel Water Bottle',
                 'amount' => 1000,
                 'currency' => 'aed',
                 'quantity' => 1,
             ]],*/
//        'mode' => 'subscription',
        'mode' => 'payment',
        'success_url' => url('success'),
        'cancel_url' => url('failure'),
        'payment_intent_data' => [
            'application_fee_amount' => 123,
            'transfer_data' => [
                'destination' => $account_id->id,
            ],
        ],
    ]);

    dd($account_id, $session->url, $session->url);
//    return view('web.testprice');
});
Route::get('import_sub_category', function () {
    Excel::import(new SubCategoryImport(), public_path('import_sub_category.xlsx'));
});
Route::get('test_tran', function () {


    dd(env('APP_URL') . env('FACEBOOK_URL'));

    $keys = [
        'ServiceDeliveryModal',
        'Order in progress',
        'Accept and finish',
        'delivered',
        'Attachments',
        'Total amount',
        'Descreption',
        'number_of_item',
        'Service Cost',
        'View Order',
        'delivered',
        'Reset your password ',
        'new password',
        'Reset your password ',
        'Send',
        'Please check your email and enter the verification code  ',
        'Send',
        'To reset your password, enter your email address',
        'Forget Password ? ',
        'Log in',
        ' Enter  the verification code',
        'All right reserved | Freelance-Arabia.com',
        'Theres a whole world of talent waiting for you',
        'Next',
        'Weve created an environment where employers and freelancers can exchange their expertise.
     When you work with Freelance Arabia, you can be sure of quality work delivered at competitive prices.',
        'Freelance Arabia is a platform in the Middle East that provides professional freelancers with a high-value platform to showcase their talents.
       Our vision is to create a freelance opportunity for all, not just the few.
     Freelance Arabia is the first Arab freelancing platform that seeks to be a global freelancing platform,
      connecting employers and freelancers from all over the world. .',
        'Freelance Arabia will revolutionize the way you work.
        We want to make sure that every individual gets the chance to work with leading organizations,
    but also to providing the best working environment.
      You can be rest assured knowing you are in safe hands with us! .',
        'FreelancerArabic About Us',
        'shopping_basket',
        'number_of_item',
        'Additional_Services',
        'I_will_create_UI_UX_design_for_mobile_app_,_website_or_landing',
        'shopping_basket',
        'amount',
        'Inquiries_about_website_design_and_programming',
        'Tax_amount',
        'Total',
        'Services_you_may_like',
        'I_will_create_UI_UX_design_for_mobile_app_,_website_or_landing'
        , 'Confirm_and_checkout',
        'Card_Number',
        'CVC',
        'Expiration_Month',
        'Expiration_Year',
        'Confirm_and_checkout',
        'No Item',
        'price',
        'Tax',
        'Additional_Services',
        'Total',
        'pay',
        'service details',
        'username',
        'Name on Card',
        'Additional_Services',
        'Previous',
        'Next',
        'Description',
        'Order Status',
        'Delivered',
        'Canceled',
        'Show Order',
        'Service Canceled',
        'Service delivered',
        'In Progress',
        'Waiting',
        'Show Order',
        'Order Details',
        'Review',
        'Amount',
        'Number of times to buy',
        'Extra Services',
        'Title',
        'price',
        'Details',
        'Total Amount',
        'Attachments',
        'Accept',
        'Reject',
        'Delivery',
        'In Progress',
        'Your delivery is under review',
        'Service Cancelled',
        'Service Delivery',
        'Order Details',
        'Description',
        'Preview',
        'Description',
        'Amount',
        'Attachments',
        'Deliver',
        'Amount',
        'Details',
        'Accept',
        'Reject',
        'Delivery',
        'In Progress',
        'Service delivered',
        'Your delivery is under review',
        'Service delivered',
        'Service Cancelled',
        'Your delivery is under review',
        'Delivered and completed',
        'Delivery',
        'Service Delivery',
        'Title',
        'Description',
        'Attachments',
        'Deliver',
        'Attachments',
        'Amount',
        'Number of times to buy',
        'Number of times to buy',
        'Delivery view',
        'Accept and complete',
        'Review',
        'In Progress',
        'Pay',
        'Personal Info',
        'Education',
        'Occupation',
        'Next',
        'Instructions',
        'Media',
        'Service Info',
        'Next',
        'Delivery time',
        'Same appointment',
        'Extra appointment',
        'Extra days for delivery',
        'Next',
        'Completion Rate',
        'Completion Rate',
        'Personal Info',
        'More',
        'service details',


    ];


    foreach ($keys as $key)
        t_label($key);


});
Route::get('test_strip', function () {


    $stripe = new StripeClient(env('STRIPE_SECRET'));


    $total = 100;
    $user_id = auth('web')->user();

    $paymentIntents = $stripe->paymentIntents->create([
        'amount' => $total * 100,
        'currency' => 'usd',
        'customer' => $user_id->getStripeCustomerId(),
        'payment_method_types' => ['card'],
    ]);

    dd('success to add  data');
//    return view('web.testprice');
});
Route::get('import_sub_category', function () {
    \Maatwebsite\Excel\Facades\Excel::import(new \App\Imports\SubCategoryImport(), public_path('import_sub_category.xlsx'));

    dd('success to add  data');
//    return view('web.testprice');
});
Route::get('/switch-lang/{lang}', [LocalController::class, 'switchLang'])->name('switchLang');
Route::get('/switch-currency/{currency}', [LocalController::class, 'switchCurrency'])->name('switchCurrency');
Route::get('store/terms_conditions', [LocalController::class, 'terms_conditions'])->name('store.terms_conditions');
Route::get('store/about_us', [LocalController::class, 'about_us'])->name('store.about_us');
Route::get('store/privacy', [LocalController::class, 'privacy'])->name('store.privacy');
Route::get('store/common-questions', [LocalController::class, 'common_questions'])->name('store.common_questions');
Route::get('auth/{provider}', [SocialAuthController::class, 'redirect'])->name('SocialAuth.redirect');
Route::get('callback/{provider}', [SocialAuthController::class, 'SocialAuth'])->name('store.SocialAuth');
Route::post('email/verification', [UserRegistersController::class, 'email_verification'])->name('store.email_verification');
Route::get('sub-category/{id}', [App\Http\Controllers\Web\SubCategory\SubCategoryController::class, 'index'])->name('store.sub_category');
Route::get('sub-category/{id}/services', [App\Http\Controllers\Web\SubCategory\SubCategoryController::class, 'ServicesBaseSubCategory'])->name('store.ServicesBaseSubCategory');
Route::group(['namespace' => 'Web'], function () {

    Route::post('store-token', [FcmNotifications::class, 'StoreToken']);
    Route::get('/', [App\Http\Controllers\Web\Home\WebHomeController::class, 'index'])->name('store.index');
    Route::post('store/login', [UserLoginController::class, 'login'])->name('store.login');
    Route::post('store/register', [UserRegistersController::class, 'register'])->name('store.register');


    Route::post('/filter', [App\Http\Controllers\Web\Home\WebHomeController::class, 'general_search'])->name('store.general_search');
    Route::get('/profile-seller/{id}', [App\Http\Controllers\Web\Home\WebHomeController::class, 'profile_seller'])->name('store.profile_seller');
    ////// Filters
//    Route::group(['namespace' => 'filter'], function () {
//        Route::get('/deliver-time-filter', [FilterController::class, 'DeliverTimeFilter'])->name('store.filter.deliver_time_filter');
//        Route::get('/budget-filter', [FilterController::class, 'BudgetFilter'])->name('store.filter.budget_filter');
//        Route::get('/seller-details-filter', [FilterController::class, 'SellerDetailsFilter'])->name('store.filter.seller_details_filter');
//        Route::get('/service-options', [FilterController::class, 'ServiceOptions'])->name('store.filter.service_options');
//        Route::get('/online-seller', [FilterController::class, 'OnLineSeller'])->name('store.filter.online_seller');
//    });

    Route::get('/start_selling', [SellerController::class, 'start_selling'])->name('seller.start_selling');

    Route::group(['middleware' => 'auth:web'], function () {


        Route::get('/packages', [\App\Http\Controllers\Web\Packages\PackagesController::class, 'index'])->name('packages.index');
        Route::get('/subscribe-packages/{Packages:id}', [\App\Http\Controllers\Web\Packages\PackagesController::class, 'subscribe'])->name('packages.subscribe');
        Route::post('/subscribe-packages/checkout', [\App\Http\Controllers\Web\Packages\PackagesController::class, 'checkout'])->name('packages.subscribe.checkout');


        Route::get('/seller_on_boarding', [SellerController::class, 'seller_on_boarding'])->name('seller.seller_on_boarding');
        Route::post('/seller_on_boarding', [SellerController::class, 'update_seller'])->name('seller.update_seller');

        Route::get('store/home', [App\Http\Controllers\Web\Home\WebHomeController::class, 'home'])->name('store.home');

        Route::get('store/search', [App\Http\Controllers\Web\Home\WebHomeController::class, 'home_search'])->name('store.home_search');

        Route::get('store/service_owner/home', [LocalController::class, 'service_owner'])->name('store.service_owner');
        Route::post('store/logout', [UserLoginController::class, 'logout'])->name('store.logout');

        //services
        Route::get('store/service/create', [ServiceController::class, 'create'])->name('store.service.create');
        Route::get('store/service/details/{id}', [ServiceController::class, 'show'])->name('store.service.details');

//        Route::get('store/service/{id}/new-payment', [ServiceController::class, 'new_payment'])->name('store.service.new_payment');

        Route::post('store/service/store', [ServiceController::class, 'store'])->name('store.service.store');


        //orders
        Route::get('store/show/orders', [OrdersController::class, 'index'])->name('store.order.show');
        Route::get('store/purchases/orders', [OrdersController::class, 'purchases'])->name('store.order.purchases');

        Route::get('store/orders/complete', [OrdersController::class, 'orderComplete'])->name('store.order.orderComplete');
        Route::get('store/orders/canceled', [OrdersController::class, 'orderCanceled'])->name('store.order.orderCanceled');

        Route::get('store/purchases/complete', [OrdersController::class, 'purchasesComplete'])->name('store.order.purchasesComplete');
        Route::get('store/purchases/canceled', [OrdersController::class, 'purchasesCanceled'])->name('store.order.purchasesCanceled');
        Route::get('store/purchases/show/{id}', [OrdersController::class, 'show_purchases_item'])->name('order_item.show_purchases_item');

        Route::get('store/order/{id}/cancel', [OrdersController::class, 'cancel_order_item'])->name('order_item.cancel');
        Route::post('store/order/{id}/complete', [OrdersController::class, 'complete_order_item'])->name('order_item.complete');


        Route::post('store/order/{id}/deliver', [OrdersController::class, 'deliver_order_item'])->name('order_item.deliver_order_item');
        Route::post('store/order/{id}/review', [OrdersController::class, 'review_order_item'])->name('order_item.review_order_item');
        Route::get('store/order/{id}/accept', [OrdersController::class, 'accept_order_item'])->name('order_item.accept_order_item');
        Route::get('store/order/show/{id}', [OrdersController::class, 'show_order_item'])->name('order_item.show_order_item');
        Route::post('store/order/finish/{id}', [OrdersController::class, 'finish_order_item'])->name('order_item.finish_order_item');


        Route::get('store/profile/edit', [ProfileController::class, 'index'])->name('profile.index');
        Route::post('store/profile/update', [ProfileController::class, 'update'])->name('profile.update');

        Route::group(['prefix' => 'cart'], function () {
            Route::get('/', [CartController::class, 'showCart'])->name('store.cart.show');


            Route::post('/store', [CartController::class, 'store'])->name('store.cart');
            Route::get('/delete/{id}', [CartController::class, 'deleteCartItem'])->name('store.cart.delete_item');
            Route::post('/checkout', [CartController::class, 'checkout'])->name('checkout.cart');
            Route::get('/checkout/form/{paymentIntentId}', [CartController::class, 'PaymentIntentForm'])->name('checkout.form');
            Route::post('/checkout/confirm', [CartController::class, 'ConfirmPaymentIntent'])->name('confirm.checkout');
        });

        Route::group(['prefix' => 'contact'], function () {
            Route::get('/{id}', [ContactController::class, 'index'])->name('store.contact.send.show');
            Route::post('/contact/send', [ContactController::class, 'sendMessage'])->name('store.contact.sendMessage');
            Route::get('/contact/chat', [ContactController::class, 'show'])->name('store.contact.show');
            Route::get('/contact/messages/{sender_id}', [ContactController::class, 'showMessages'])->name('store.contact.showMessages');
            Route::post('/contact/messages/replay', [ContactController::class, 'replayMessages'])->name('store.contact.replayMessages');

        });


        Route::group(['prefix' => 'fav'], function () {
            Route::get('add/fav/{service_id}', [FavController::class, 'AddServiceFav'])->name('store.user.add_fav');
        });
        Route::group(['prefix' => 'settings'], function () {
            Route::get('/settings-account', [SettingsController::class, 'index'])->name('store.settings.show');
            Route::get('/settings-notifications', [SettingsController::class, 'settings_notifications'])->name('store.settings.settings_notifications');
            Route::get('/settings-payment', [SettingsController::class, 'settings_payment'])->name('store.settings.payment');
            Route::post('/withdraw-payment', [SettingsController::class, 'withdraw_payment'])->name('store.settings.withdraw_payment');
            Route::post('/general-settings/update', [SettingsController::class, 'general_settings'])->name('store.settings.general_settings.update');
            Route::post('/general-settings/update/account-connect', [SettingsController::class, 'account_connect'])->name('store.settings.general_settings.account_connect');
        });
        Route::get('notifications/', [NotificationsController::class, 'notifications'])->name('notification.index');
        Route::post('stop/notifications/', [NotificationsController::class, 'stop_notifications'])->name('notification.stop_notifications');

        Route::get('notifications/mark-read-notification/{item_id}/{notification_id}', [NotificationsController::class, 'markAsReadNotification'])->name('notification.markAsReadNotification');

    });


//    Route::get('telr/payment', [PaymentController::class, 'showForm'])->name('payment.showForm');
//    Route::get('telr/payment/pay', [PaymentController::class, 'CreatePayment'])->name('payment.CreatePayment');
    Route::get('get-cat/{cat_id?}', [ServiceController::class, 'getSubCategoryBaseCategory'])->name('services.getSubCategoryBaseCategory');
});
