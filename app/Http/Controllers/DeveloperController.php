<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;

class DeveloperController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function translate_controller_messages()
    {

        $folder = base_path('app/Http/Controllers/Web');
        $folders = [];
        $dirs = scandir($folder);
        $list_of_new_string_to_trans = [];

        foreach ($dirs as $dir) {

            if (!in_array($dir, ['.', '..'])) {

                $base2 = "$folder/$dir";
                $files2_file = scandir("$base2");


                foreach ($files2_file as $fi)
                    if (!in_array($fi, ['.', '..'])) {
                        $myfile = fopen("$base2/$fi", "r");

                        $content = fread($myfile, filesize("$base2/$fi"));

                        $keys_list = [];
                        $new_keys = array_merge($this->tag_contents($content, 't_label(\'', '\')'), $this->tag_contents($content, 't_label("', '")'));
                        foreach ($new_keys as $key) {
                            if (!\Illuminate\Support\Str::contains($key, 'php')) {

                                t_label($key);
                                $keys_list[] = $key;
                            }

                        }


                        $list_of_new_string_to_trans = array_merge($list_of_new_string_to_trans, $keys_list);
//                $list_of_new_string_to_trans = array_merge(tag_contents($content ,'t_label(\'','\')'),$list_of_new_string_to_trans);
//                $list = tag_contents($content ,'t_label(\'','\')');
//                dd($list);
//
//                if (preg_match('/t_label(.*?)/', $content, $match) ) {
//                    dd($match,$content);
//                    echo $match[1];
//                }
//                $list =   get_string_between($content,'t_label(',')');

                        //                dd($list);
//                dd(fread($myfile,filesize("$base2/$fi")));
//                dd($fi);
                    }
            }


        }


        dd('translate successfully ', $list_of_new_string_to_trans);
    }

    public function translate_resources_messages()
    {

        $folder = base_path('resources/views/web');

        $dirs = scandir($folder);

        $list_of_new_string_to_trans = [];

        try {

            foreach ($dirs as $dir) {
                $base2 = "$folder/$dir";


                if (!in_array($dir, ['.', '..','...','....']) && !Str::contains($dir,'..')){

                    $list_of_new_string_to_trans = array_merge($list_of_new_string_to_trans, $this->readFoldersFiles($base2));

                 }


            }
        } catch (\Exception $exception) {
            dd('translate successfully ', $dir, $list_of_new_string_to_trans);

        }
        dd('translate successfully ', $list_of_new_string_to_trans);


    }


    function readFoldersFiles($base2)
    {
//        $dirs = scandir($path);
        $list_of_new_string_to_trans = [];

//        foreach ($dirs as $dir) {
//            $base2 = "$path/$dir";


             if (is_dir($base2)) {

                $files2_file = scandir("$base2");

                foreach ($files2_file as $fi) {

                    if (!in_array($fi, ['.', '..','...','....']) && !Str::contains($fi,'..'))
                        $list_of_new_string_to_trans = array_merge($list_of_new_string_to_trans, $this->readFoldersFiles("$base2/$fi"));
                    dd($list_of_new_string_to_trans);
                }


            }
        else {
                //                         if (!in_array($fi, ['.', '..'])) {


                $myfile = fopen("$base2", "r");

                $content = fread($myfile, filesize("$base2"));

                $content_without_tags = trim(strip_tags($content));

                $content_without_tags = trim(preg_replace(
                    "/[0-9]+/", '', $content_without_tags));

                $content_without_tags = trim(str_replace(['@csrf'
                    , "@section('content')"
                    , "@extends('web.layouts.master')",
                    "@endsection",
                    "@endif",
                    "@endforeach",
                    "@else",
                    "@endsection",
                    "!",
                    "#",
                    "$",
                    "%",
                    "()",
                    ")->",
                    ":",
                     ".",
                    "/[0-9]+/",
                ], '', $content_without_tags));
//            $content_without_tags = $this->remove_contents($content_without_tags, '<script>', '</script>');
//            $content_without_tags = $this->remove_contents($content_without_tags, '<script>', '</script>');

            $content_without_tags = $this->delete_all_between($content_without_tags, '@foreach(', ')');
            $content_without_tags = $this->delete_all_between($content_without_tags, '@php', 'endphp');
            $content_without_tags = $this->remove_contents($content_without_tags, '@if(', ')');
            $content_without_tags = $this->delete_all_between($content_without_tags, '{{', '}}');
            $content_without_tags = $this->delete_all_between($content_without_tags, 't_label(', ')');

                $list = explode('
                        ', $content_without_tags);
//                        $content_without_tags;
                $result = array_map('trim', $list);



                $list_of_new_string_to_trans = array_merge($list_of_new_string_to_trans, ["$base2" => $result]);


            }



//        }


        return $list_of_new_string_to_trans;

    }

    function tag_contents($string, $tag_open, $tag_close)
    {
        $result = [];
        foreach (explode($tag_open, $string) as $key => $value) {
            if (strpos($value, $tag_close) !== FALSE) {
                $result[] = substr($value, 0, strpos($value, $tag_close));;
            }
        }
        return $result;
    }

    function remove_contents($string, $tag_open, $tag_close)
    {
        $result = "";
        foreach (explode($tag_open, $string) as $key => $value) {
            if (strpos($value, $tag_close)) {

//                 dd($value, substr($value,  0, strpos($s, $tag_close)),  strpos($value, $tag_close));
                $result .= //substr($value,  0, strpos($value, $tag_close))
                    substr($value, strpos($value, $tag_close) + strlen($tag_close));;
            }
        }
        return trim($result);
    }


    function delete_all_between($string, $beginning, $end)
    {
        $beginningPos = strpos($string, $beginning);
        $endPos = strpos($string, $end);
        if ($beginningPos === false || $endPos === false) {
            return $string;
        }

        $textToDelete = substr($string, $beginningPos, ($endPos + strlen($end)) - $beginningPos);

        return $this->delete_all_between(str_replace($textToDelete, '', $string), $beginning, $end); // recursion to ensure all occurrences are replaced
    }
}
