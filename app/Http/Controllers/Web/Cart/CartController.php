<?php

namespace App\Http\Controllers\Web\Cart;

use App\Helpers\FCM;
use App\Http\Controllers\Controller;
use App\Models\Cart\Cart;
use App\Models\Item\Item;
use App\Models\Order\Order;
use App\Models\Order\OrderItemAdditionalServices;
use App\Models\Services\AdditionalServices;
use App\Models\Services\Service;
use App\Models\Settings\SystemSettings;
use App\Models\Transaction\Transaction;
use App\Models\User;
use App\Notifications\web\ServiceRequestsNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Stripe\StripeClient;
use Throwable;

class CartController extends Controller
{
    public function showCart()
    {

        $carts = Cart::with('service.AdditionalServices')
            ->where('user_id', auth('web')->user()->id)
            ->where('cart_id', $this->getCartId())
            ->get();
        $sub_total = $carts->sum(function ($item) {
            return @$item->service->currency_price * $item->quantity;
//            return $item->service->price * $item->quantity;
        });

        /*        $similar_services = Service::where('user_id', '!=', auth('web')->user()->id)
                    ->with('ServiceOwner')->inRandomOrder()->limit(10)->get();*/
        $settings = SystemSettings::first();

        $strip_tax = ($settings->stripe_tax ?? 3.9) / 100;
        $one_aed_in_current_currency = get_one_aed_in_current_currency();
        $country_tax = 5 / 100;

//        $sub_total =100;
        $tax = round($sub_total * $strip_tax, 0, PHP_ROUND_HALF_UP);
        $total_tax_strip = $tax + $one_aed_in_current_currency;

        $total_country_tax = ($total_tax_strip * $country_tax);

        $total_tax = $total_tax_strip + $total_country_tax;

        $total = $sub_total + $total_tax;
        $total = round($total, 2);

        return view('web.cart.cart', get_defined_vars());
    }

    public function getCartId()
    {

        $id = Cookie::get('cart_id');
        if (!$id) {
            $id = Str::uuid();
            Cookie::queue('cart_id', $id, 60 * 24 * 30,);
        }

        return $id;
    }

    public function store(Request $request)
    {
        $request->validate([
            'quantity' => 'required',
            'attachment' => 'required',
            'notes' => 'required',
        ]);
        $user_id = auth('web')->user()->id;

        $service = Service::where('admin_accept', 1)->where('id', $request->service_id)->first();

        $service_price = $service->price;  ///price  in usd

        $total_price = $request->quantity * $service_price; ///price  in usd

        $cart = Cart::where([
            'user_id' => $user_id
        ])->delete();
        /*        $cart = Cart::where([
                    'cart_id' => $this->getCartId(),
                    'service_id' => $request->service_id,
                ])->first();*/

        /*        if ($cart) {
                    $cart->increment('quantity', $request->quantity);
                } else {*/
        $attachment_file_name = saveImage($request->file('attachment'), 'uploads/attachment');
        $cart = Cart::create([
            'cart_id' => $this->getCartId(),
            'user_id' => $user_id,
            'service_id' => $request->service_id,
            'quantity' => $request->quantity,
            'price' => $service_price,
            'total_price' => $total_price,
            'notes' => $request->notes,
            'attachment' => $attachment_file_name,
        ]);
//    }

        /*        $cart->update([
                    'total_price' => $cart->quantity * $service_price
                ]);*/


        return redirect()->route('store.cart.show');//->with('alert-success', t_label("The order ready in the cart"));
//        return redirect()->back()->with('alert-success', 'تم اضافة الخدمة الى السلة');
    }


    public function checkout(Request $request)
    {


//        dd($request->all());
        $cart_service_ids = Cart::with('service')->where('cart_id', $this->getCartId())->get()->pluck('service_id')->toArray();
        $cart_service_users_ids = Service::where('admin_accept', 1)->whereIN('id', $cart_service_ids)->pluck('user_id')->toArray();
        $user_id = auth('web')->user();
        $carts = Cart::with('service')->where('cart_id', $this->getCartId())->get();
        $sub_total = $carts->sum(function ($item) {
            return $item->service->price * $item->quantity;
        });


        $sub_total_currency = $carts->sum(function ($item) {
            return $item->service->currency_price * $item->quantity;
        });


        if ($carts->count() == 0) {
            return redirect()->route('store.home');
        }


        DB::beginTransaction();
        try {


            $settings = SystemSettings::first();


            $country_tax = 5 / 100;
            $strip_tax = ($settings->stripe_tax ?? 3.9) / 100;
            $system_commission = ($settings->comission) / 100;
            $one_aed_in_current_currency = get_one_aed_in_current_currency();
            $one_aed_in_usd_currency = get_one_aed_in_current_currency("USD");


//        $sub_total =100;


            /** start add extra services amount to total ***/

            /*      $additional_sub_total = 0;
                  $additional_sub_total_c = 0;*/
            foreach ($request->additional_service as $order_id => $add_ids) {
                $ext_services = AdditionalServices::whereIn('id', $add_ids)->get();
                foreach ($ext_services as $ext_service) {
                    $sub_total += $ext_service->price;
                    $sub_total_currency += $ext_service->currency_price;
                }
            }

            /** end add extra services amount to total ***/

            /**** start calculate to  usd currency **/
            $tax = round($sub_total * $strip_tax, 0, PHP_ROUND_HALF_UP);
            $total_tax_strip = $tax + $one_aed_in_usd_currency;

            $total_country_tax = ($total_tax_strip * $country_tax);

            $total_tax = $total_tax_strip + $total_country_tax;

            $total = $sub_total + $total_tax;

            /** End calculate to  usd currency***/


            /**** Start calculate to  current currency **/
            $tax_c = round($sub_total_currency * $strip_tax, 0, PHP_ROUND_HALF_UP);
            $total_tax_strip_c = $tax_c + $one_aed_in_current_currency;

            $total_country_tax_c = ($total_tax_strip_c * $country_tax);

            $total_tax_c = $total_tax_strip_c + $total_country_tax_c;

            $total_c = $sub_total_currency + $total_tax_c;

            $request->session()->put([
                'tax' => $tax_c,
                'total_tax_strip_c' => $total_tax_strip_c,
                'total_country_tax_c' => $total_country_tax_c,
                'sub_total_currency' => $sub_total_currency,
                'total_tax_c' => $total_tax_c,
                'total_c' => $total_c,
            ]);


            /** End calculate to  usd currency***/


            $stripe = new StripeClient(env('STRIPE_SECRET'));

            $paymentIntents = $stripe->paymentIntents->create([
                'amount' => $total_c * 100,
                'currency' => current_currencies()->code,
                'customer' => $user_id->getStripeCustomerId(),
                'payment_method_types' => ['card'],
            ]);


            $system_commission_amount = $system_commission * $sub_total;
            $paymentIntentId = $paymentIntents->id;
            $order = new Order();
            $order->user_id = $user_id->id;
            $order->total_price = $total;
            $order->status = 'UNPAID';
            $order->charge_id = $paymentIntentId;
            $order->provider_amount = $sub_total - $system_commission_amount;
            $order->system_commission = $system_commission_amount;
            $order->system_commission_percentage = $system_commission;
            $order->tax = $total_tax;
            $order->country_tax = $country_tax;
            $order->payment_tax = $total_tax_strip;
            $order->save();
            foreach ($carts as $item) {
                $order->items()->create([
                    'service_id' => $item->service_id,
                    'quantity' => $item->quantity,
                    'price' => $item->service->price,
                    'user_owner_service_id' => $item->service->user_id,
                    'waiting_acceptance' => 1,
                    'is_complete' => 0,
                    'is_canceled' => 0,
                    'in_progress' => 0,
                    'notes' => $item->notes,
                    'attachment' => $item->attachment,
                ]);
            }
            if (!is_null($request->additional_service)) {
                foreach ($request->additional_service as $key => $add) {
                    foreach ($add as $ad) {
                        $new_order_add_service = new OrderItemAdditionalServices();
                        $new_order_add_service->order_id = $order->id;
                        $new_order_add_service->service_id = $key;
                        $new_order_add_service->additional_services_id = $ad;
                        $new_order_add_service->save();
                    }
                }
            }


            DB::commit();

            return redirect()->route('checkout.form', encrypt($paymentIntentId));


        } catch (Throwable $e) {
            DB::rollBack();
            return $e->getMessage();
        }
    }

    public function PaymentIntentForm($paymentIntentId)
    {
        $paymentIntentId = decrypt($paymentIntentId);
        return view('web.cart.checkout', get_defined_vars());

    }

    public
    function ConfirmPaymentIntent(Request $request)
    {
        $order = Order::where('charge_id', $request->paymentIntentId)->first();

        $stripe = new \Stripe\StripeClient(
            env('STRIPE_SECRET')
        );
        try {

            $stripe->paymentIntents->confirm(
                $request->paymentIntentId,
                ['payment_method' => 'pm_card_visa']
            );


        } catch (\Exception $exception) {
            Log::error($exception);
        }


        $order->update([
            'status' => 'COMPLATED',
        ]);



        $cart_service_ids = Cart::with('service')->where('cart_id', $this->getCartId())->get()->pluck('service_id')->toArray();
        $cart_service_users_ids = Service::where('admin_accept', 1)->whereIN('id', $cart_service_ids)->pluck('user_id')->toArray();

        $order_items = Item::where('order_id', $order->id)->with('service', 'order')->get();
//        $order_items_additional_services = OrderItemAdditionalServices::where('order_id', $order->id)->get();
//        foreach ($order_items_additional_services as $item){
//            $item->delete();
//        }
        foreach ($order_items as $item) {
            $user = User::where('id', $item->user_owner_service_id)->first();
            $user->notify(new ServiceRequestsNotification($item));
        }
        $services_owners_tokens = User::whereIn('id', $cart_service_users_ids)->get()->pluck('device_key')->toArray();
        FCM::push($services_owners_tokens, t_label("Request a service"), t_label('You have a new service request'));
//        FCM::push($services_owners_tokens, ' طلب خدمة ', 'لديك طلب خدمة');
        $carts = Cart::where('cart_id', $this->getCartId())->delete();

        return redirect()->route('store.home');


    }

    public
    function deleteCartItem($id)
    {
        $cart_item = Cart::where('user_id', auth('web')->user()->id)->where('service_id', $id)->first();
        if (!$cart_item) {
            return view('web.error.404');
        }
        $cart_item->delete();
        $order_items_additional_services = OrderItemAdditionalServices::where('service_id', $id)->get();
        foreach ($order_items_additional_services as $item) {
            $item->delete();
        }
        return redirect()->back()->with('alert-success', t_label("Deleted successfully"));
//        return redirect()->back()->with('alert-success', 'تم حذف الخدمة من السلة');

    }
}
