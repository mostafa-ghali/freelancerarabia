<?php

namespace App\Http\Controllers\Web\Packages;

use App\Http\Controllers\Controller;
use App\Models\Category\Category;
use App\Models\Category\SubCategory;
use App\Models\Languages;
use App\Models\Packages\Packages;
use App\Models\Packages\SubscribePackages;
use App\Models\Tags\UserTags;
use App\Models\User;
use App\Models\User\UserLanguages;
use App\Models\User\UserOccupation;
use App\Models\User\UserSubCategory;
use Carbon\Carbon;
use Dflydev\DotAccessData\Data;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Stripe\StripeClient;

class PackagesController extends Controller
{

    public function index()
    {
        $packages = Packages::get();
//        return view('web.testprice',get_defined_vars());

        return view('web.packages.packages', get_defined_vars());
    }


    public function subscribe(Request $request, $package_id)
    {

        $package = Packages::findOrFail($package_id);

        $request->session()->push('subscribe_package', $package);


        $total = $package->new_price;

        $stripe = new StripeClient(env('STRIPE_SECRET'));


        $paymentIntents = $stripe->paymentIntents->create([
            'amount' => $total * 100,
            'currency' => 'aed',
            'customer' => auth()->user()->getStripeCustomerId(),
            'payment_method_types' => ['card'],
        ]);


        $paymentIntentId = $paymentIntents->id;


        $subscribe_package_ = SubscribePackages::updateOrCreate([
            'package_id' => $package->id
            ,
            'user_id' => auth()->id(),
            'no_monthes' => $package->no_monthes
            ,
            'is_paid' => 0,
        ], [

            'price' => $total,
            'payment_id' => $paymentIntentId


        ]);


//        return view('web.testprice',get_defined_vars());

        return view('web.packages.subscribe', get_defined_vars());
    }

    public function checkout(Request $request)
    {


         $item = SubscribePackages::where('payment_id', $request->paymentIntentId)->first();
        $stripe = new \Stripe\StripeClient(
            env('STRIPE_SECRET')
        );

        try {

            $response = $stripe->paymentIntents->confirm(
                $request->paymentIntentId,
                ['payment_method' => 'pm_card_visa']
            );
//
             if (@$response && $response->status === 'succeeded') {

                $item->start_date = now();
                $item->end_date = now()->addMonths($item->no_monthes);
                $item->stripe_token = $request->stripeToken;
                $item->is_paid = 1;
                $item->save();

             $user = $item->user;
                $user->package_id = $item->package_id;
                $user->start_package_date = now();
                if ($user->end_package_date && $user->end_package_date > now()) {
                    $user->end_package_date = Carbon::parse($user->end_package_date)->addMonths($item->no_monthes);
                } else {
                    $user->end_package_date = $item->end_date;
                }

                $user->save();


                return redirect()->route('packages.index')->with('alert-success', t_label('Successfully Subscribe'));

            }

        } catch (\Exception $exception) {
            Log::error($exception);

        };

        return back()->with('alert-failed', t_label('Failed Subscribe'));

    }


}
