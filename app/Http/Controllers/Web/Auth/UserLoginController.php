<?php

namespace App\Http\Controllers\Web\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class UserLoginController extends Controller
{
    use AuthenticatesUsers;
    protected $redirectTo = 'store/home';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('guest:store')->except('logout');

    }
    public function login(Request $request)
    {
//
            $user = User::where('email',$request->email)->first();
            $email =$request->email;
            if (!is_null($user) && $user->is_verified != 1){
                return redirect()->back()->with(['alert-verification'=> t_label('Please verify your email')]);
//                return redirect()->back()->with(['alert-verification'=> 'لقد تم التسجيل يجب تأكيد الحساب']);
            }

        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.

        if (method_exists($this, 'hasTooManyLoginAttempts') &&
            $this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        if ($this->attemptLogin($request)) {
            $user->update(['online'=>1]);
            return redirect()->to('store/home');
        }


        $this->incrementLoginAttempts($request);

        return back()->with('alert-fails', t_label("Wrong username or password, please try again."));
//        return back()->with('alert-fails', 'Email or password in not correct');

    }
    protected function attemptLogin(Request $request)
    {
        return $this->guard()->attempt(
            $this->credentials($request), $request->filled('remember')
        );
    }
    public function logout()
    {

        $user = auth('web')->user();
        $user->update([
            'service_owner'=>0,
            'online'=>0,
            'logout_at'=>date('Y-m-d H:i:s')
        ]);
        //logout user
        auth()->logout();
        // redirect to homepage
        return redirect('/');
    }
    protected function credentials(\Illuminate\Http\Request $request)
    {
        return ['email' => $request->email, 'password' => $request->password, 'is_verified' => '1','active'=>'1'];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}
