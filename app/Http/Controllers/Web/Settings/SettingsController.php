<?php

namespace App\Http\Controllers\Web\Settings;

use App\Http\Controllers\Controller;
use App\Models\Category\Category;
use App\Models\Order\Order;
use App\Models\Settings\SystemSettings;
use App\Models\Transaction\Transaction;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Stripe\StripeClient;

class SettingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $labels = labels(['web.general']);
        $categories = Category::get();
        return view('web.settings.settings', get_defined_vars());
    }

    public function settings_notifications()
    {
        $labels = labels(['web.general']);
        $categories = Category::get();
        return view('web.settings.settings_notifications', get_defined_vars());
    }

    public function account_connect(Request $request)
    {
        $request->validate(['email' => 'required|unique:users.email',]);
        dd($request->all());

    }

    public function general_settings(Request $request)
    {
        $request->validate(['deactivation_reason' => 'required', 'deactivation_note' => 'required',]);
        $user = User::where('id', auth('web')->user()->id)->first();
        $user->active = $request->is_active;
        $user->deactivation_reason = $request->deactivation_reason;
        $user->deactivation_note = $request->deactivation_note;
        $user->update();
        Auth::logout();
        return redirect()->back()->with('alert-success', t_label("Your account deactivated successfully"));
//        return redirect()->back()->with('alert-success', 'تم الغاء الحساب بنجاح');
    }

    public function settings_payment()
    {
        $labels = labels(['web.general']);
        $categories = Category::get();
        $total = 0;
        $settings = SystemSettings::first();
        $user_orders = Transaction::where('user_id', \auth('web')->user()->id)
            ->where('available_withdraw', 1)
            ->get();


        //// money allowed  withdrew  after two days  from payment request
        $allowed_user_orders_to_withdrew = $user_orders->where('available_withdraw_date', ">", now());

        $total_price = $user_orders->sum('amount');
//        $system_tax = ($total_price * $settings->system_tax / 100);
//        $total = round($total_price - $system_tax);  /// total amount  to user


        /** calculate  valid  amount to withdrew */
        $allowed_amount_to_withdraw = $allowed_user_orders_to_withdrew->sum('amount');
//        $system_tax_allowed = ($allowed_total_price * $settings->system_tax / 100);
//        $allowed_amount_to_withdraw = round($allowed_total_price - $system_tax_allowed);
        /** calculate  valid  amount to withdrew */

//        dd($total);

//        foreach ($user_orders as $order){
//            $system_tax = $order->total_price - ($order->total_price * $settings->system_tax / 100);
////            $stripe_tax = $order->total - ($order->total_price * $settings->stripe_tax / 100);
//            $total += round($order->total_price - $system_tax);
//        }
        return view('web.settings.payment', get_defined_vars());
    }

    public function withdraw_payment(Request $request)
    {
        $total = 0;
        $settings = SystemSettings::first();
        $user_orders = Transaction::where('user_id', \auth('web')->user()->id)
            ->where('available_withdraw', 1)
            ->get();

        $total_price = $user_orders->sum('amount');
        $allowed_amount_to_withdraw = $user_orders->where('available_withdraw_date', ">", now());

//        $allowed_user_orders_to_withdrew = $user_orders->where('updated_at', "<", now()->subDays(1));

        /** calculate  valid  amount to withdrew */
//        $allowed_total_price = $allowed_user_orders_to_withdrew->sum('total_price');
//        $system_tax_allowed = ($allowed_total_price * $settings->system_tax / 100);
//        $allowed_amount_to_withdraw = round($allowed_total_price - $system_tax_allowed);
        /** calculate  valid  amount to withdrew */



//
//        $total_price = $user_orders->sum('total_price');
//        $system_tax = ($total_price / 100);
//        $total = round($total_price - $system_tax);
//        $total = round($total_price - $system_tax);
        if ($allowed_amount_to_withdraw > 0 && $request->withdrew_money>0 &&$request->withdrew_money<=$allowed_amount_to_withdraw ) {


             if ($request->withdrew_money ) {

                try {
                    \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));


                    /** Transfer allowed only in aed */
                    $transfer = \Stripe\Transfer::create([
                        'amount' => current_value_specific_currency($request->withdrew_money,'AED')*100,
                        'currency' => 'aed',
                        'destination' => \auth('web')->user()->getStripeAccountId(),
                    ]);






                    /****start tranfere amount**/
                    Transaction::create([

                        'user_id' => \auth('web')->id(),
//                        'order_id' => $order->id,
                        'type' => Transaction::TYPE['Withdraw'],
                        'amount' => -$request->withdrew_money ,
                        'description' => 'Paid Service',
                        'available_withdraw' => 1,
                        'available_withdraw_date' =>now(),
                    ]);


                    $msg = t_label("Transaction Successfully");
//                        'تمت عملية التحويل بنجاح';
//                    $msg = 'تمت عملية التحويل بنجاح';
                    $type = 'alert-success';

                    return redirect()->back()->with($type, $msg);
                } catch (\Exception $e) {
                    $msg = t_label('Transaction Failed ! please contact with customer support');
//                    $msg = 'فشلت عملية التحويل بالرجاء التواصل مع الدعم الفني ل معرفة المشكلة';
                    $type = 'alert-danger';
                    return redirect()->back()->with($type, $msg);
                }
            }
             else {
                $msg = t_label("Transaction Failed ! please contact with customer support");
//                $msg = 'فشلت عملية التحويل بالرجاء التواصل مع الدعم الفني ل معرفة المشكلة';
                $type = 'alert-danger';
                return redirect()->back()->with($type, $msg);
            }
        }


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
