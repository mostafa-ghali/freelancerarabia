<?php

namespace App\Http\Controllers\Web\SubCategory;

use App\Http\Controllers\Controller;
use App\Models\Category\Category;
use App\Models\Category\SubCategory;
use App\Models\Category\SubCategoryFiltersOptions;
use App\Models\Services\Service;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class SubCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index($id)
    {
        $main_category = Category::where('id', $id)->first();
        if (!$main_category) {
            return view('web.error.404');
        }
        $categories = Category::get();

        $subCategory = SubCategory::where('category_id', $id)->get();
//        $rabdom_categories = SubCategory::inRandomOrder()->limit(2)->get();
        $labels = labels(['web.general']);
        $rabdom_categories = SubCategory::where('is_popular', 1)->
        limit(5)->get();
        $breadcrumbs = [
            [
                'title' => @t_label('home'),
                'url' => url(''),
            ],
            [
                'title' => @$main_category['name_' . app()->getLocale()],
//                'url' => url('url'),
            ],
        ];
        return view('web.subCategory.cat', get_defined_vars());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function ServicesBaseSubCategory(Request $request, $id)
    {

        $sub_categories = SubCategory::with('filtersOptions.subCategoryFiltersOptions')->findOrFail($id);

        $main_category = $sub_categories->category;
        $services = Service::with('images')->where('sub_category_id', $id)->with('images', 'ServiceOwner')
            ->when($request->filled('min_price'), function ($query) use ($request) {
                $query->where('price', '>=', $request->min_price);
            })
            ->when($request->filled('max_price'), function ($query) use ($request) {
                $query->where('price', '<=', $request->max_price);
            })
            ->when($request->filled('delivery_time') && $request->get('delivery_time') == 'one_day', function ($query) use ($request) {
                $query->where('deliver_time', 1);
            })
            ->when($request->filled('delivery_time') && in_array($request->get('delivery_time'), ['up_3_days', 'up_7_days']), function ($query) use ($request) {
                $query->where('deliver_time', '>', @['up_3_days' => 3, 'up_7_days' => 7][$request->get('delivery_time')]);
            })
            ->when($request->filled('seller_type'), function ($query) use ($request) {
                $query->whereRelation('ServiceOwner', 'seller_type', $request->get('seller_type'));
            }
            )
            ->when($request->filled('online_seller'), function ($query) use ($request) {
                $query->whereRelation('ServiceOwner', 'online', 1);
            })->when($request->filled('tag'), function ($query) use ($request) {
                $query->whereHas('Tags', function ($query) use ($request) {
                    $query->whereIn('tag', $request->tag);

                });
            })
            ->when($request->filled('local_seller'), function ($query) use ($request) {
//                $query->whereRelation('ServiceOwner', 'online', 1);
            })
            ->get();
        $sub_category_id = $id;
        $sub_category_filter_options = SubCategoryFiltersOptions::where('sub_category_id', $id)->get();
        $rabdom_categories = SubCategory::inRandomOrder()->limit(2)->get();
        $labels = labels(['web.general']);
        $breadcrumbs = [
            [
                'title' => @t_label('home') ?? "Home",
                'url' => url('/'),
            ],
            [
                'title' => @$sub_categories->category->name,//['name_' . app()->getLocale()],
                'url' => route('store.sub_category', $sub_categories->category),

//                'url' => url('url'),
            ],
            [
                'title' => @$sub_categories['name_' . app()->getLocale()],
//                'url' => url('url'),
            ],
        ];

         return view('web.subCategory.ServicesBaseSubCategory', get_defined_vars());

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
