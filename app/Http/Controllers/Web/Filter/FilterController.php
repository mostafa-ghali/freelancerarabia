<?php

namespace App\Http\Controllers\Web\Filter;

use App\Http\Controllers\Controller;
use App\Models\Category\SubCategory;
use App\Models\Category\SubCategoryFiltersOptions;
use App\Models\Services\Service;
use App\Models\Services\ServiceTags;
use App\Models\User;
use Illuminate\Http\Request;

class FilterController extends Controller
{
    public function DeliverTimeFilter(Request $request)
    {
        $sub_categories = SubCategory::with('filtersOptions.subCategoryFiltersOptions')-> where('id',$request->sub_category_id)->first();
        $sub_category_id = $request->sub_category_id;
        $sub_category_filter_options= SubCategoryFiltersOptions::where('sub_category_id',$sub_category_id)->get()  ;
        $rabdom_categories = SubCategory::inRandomOrder()->limit(2)->get();

        $filter_type = $request->deliver_time_filter;
        $services = Service::get();
            // اعمل ل كل حالة من الحالات فنكشن و راوت خاص فيها للفلاتر
            if ($request->up_3_days == 'on') {
                $services = Service::where('deliver_time', '>', 3)->get();
            } elseif ($request->up_7_days == 'on') {
                $services = Service::where('deliver_time', '>', 7)->get();
            } elseif ($request->any_time == 'on') {
                $services = Service::get();
            } elseif ($request->one_day == 'on') {
                $services = Service::where('deliver_time', '1')->get();
            } else {
                $services = Service::get();
            }


        return view('web.subCategory.ServicesBaseSubCategory',get_defined_vars());


    }

    public function BudgetFilter(Request $request){
        $sub_categories = SubCategory::with('filtersOptions.subCategoryFiltersOptions')-> where('id',$request->sub_category_id)->first();
        $sub_category_id = $request->sub_category_id;
        $sub_category_filter_options= SubCategoryFiltersOptions::where('sub_category_id',$sub_category_id)->get()  ;
        $rabdom_categories = SubCategory::inRandomOrder()->limit(2)->get();
        $services = Service::whereBetween ('price',[$request->min_price , $request->max_price])->get();
        return view('web.subCategory.ServicesBaseSubCategory',get_defined_vars());
    }
    public function SellerDetailsFilter(Request $request){
        $sub_categories = SubCategory::with('filtersOptions.subCategoryFiltersOptions')-> where('id',$request->sub_category_id)->first();
        $sub_category_id = $request->sub_category_id;
        $sub_category_filter_options= SubCategoryFiltersOptions::where('sub_category_id',$sub_category_id)->get()  ;
        $rabdom_categories = SubCategory::inRandomOrder()->limit(2)->get();
        $seller_type = [$request->fresh_talent , $request->super_talent,$request->top_talent];
            $user_ids = User::whereIn('seller_type',$seller_type)->get()->pluck('id')->toArray();
        $services = Service::whereIn ('user_id', $user_ids)->get();
        return view('web.subCategory.ServicesBaseSubCategory',get_defined_vars());
    }
    public function ServiceOptions(Request $request){
        $sub_categories = SubCategory::with('filtersOptions.subCategoryFiltersOptions')-> where('id',$request->sub_category_id)->first();
        $sub_category_id = $request->sub_category_id;
        $sub_category_filter_options= SubCategoryFiltersOptions::where('sub_category_id',$sub_category_id)->get()  ;
        $rabdom_categories = SubCategory::inRandomOrder()->limit(2)->get();

        if ($request->has('tag')){
            $tags = ServiceTags::whereIn('tag',$request->tag)->get()->pluck('service_id')->toArray();

            $services = Service::whereIn ('id', $tags)->get();
        }else{
            $services  = Service::where('sub_category_id',$sub_category_id)->get();
        }

        return view('web.subCategory.ServicesBaseSubCategory',get_defined_vars());

    }

    public function OnLineSeller(Request $request){
        $on_line_seller = User::where('online',$request->seller_type)->get()->pluck('id')->toArray();
        $sub_categories = SubCategory::with('filtersOptions.subCategoryFiltersOptions')-> where('id',$request->sub_category_id)->first();
        $sub_category_id = $request->sub_category_id;
        $sub_category_filter_options= SubCategoryFiltersOptions::where('sub_category_id',$sub_category_id)->get()  ;
        $rabdom_categories = SubCategory::inRandomOrder()->limit(2)->get();
        $services = Service::whereIn ('user_id', $on_line_seller)->get();
        
        return view('web.subCategory.ServicesBaseSubCategory',get_defined_vars());


    }
}
