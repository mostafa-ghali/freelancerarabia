<?php

namespace App\Imports;

use App\Models\Category\SubCategory;
use App\Models\Label\Label;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class LabelImport implements ToModel, WithHeadingRow
{
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {

 //        t_label($row['id']);
//        if (empty($row['id']))return ;



        $id= trim($row['id']);
        if (empty($id))
            $id = Label::where('id',282)->first()->label_id;
        else
            $id = Label::where('id',$id)->first()->label_id;


        return Label::updateOrCreate([
                'screen_id' => 1,
                'language_id' => 4,
                'label_id' => $id,
            ],[
                'label_text_en' => $row['filipino'],
                'label_text_automated' => $row['filipino'],
                'label_text_override' => $row['filipino']

            ]);

/*        return 0;
        return Label::updateOrCreate([
            'screen_id' => 1,
            'language_id' => 4,
            'label_id' => $id,
          ],[
            'label_text_en' => $row['hindi'],
            'label_text_automated' => $row['hindi'],
            'label_text_override' => $row['hindi']

        ]);*/
    }
}
