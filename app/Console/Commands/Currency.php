<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;

class Currency extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'currency:save';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $date = date('Y-m-d');
        $currencies = \App\Models\Currency\Currency::get();

        $req_url = "https://api.exchangerate.host/{$date}?base=MRO";
        $response = Http::get($req_url)->json();

        $base_currency = $currencies->where('code', 'USD')->first();
        if (@$response['success'] && @$response['historical'] && @$response['rates'] && count(@$response['rates']) > 0)
            $rates = $response['rates'];

        /** start update  Currency Exchange **/
        foreach ($rates as $code => $value) {
            $cu = $currencies->where('code', $code)->first();
            if ($cu) \App\Models\Currency\CurrencyExchange::updateOrCreate([
                'from_currency_id' => $base_currency->id,
                'to_currency_id' => $cu->id
            ], [
                'value' => $value
            ]);

        }
        /** end update  Currency Exchange **/


        return 0;

    }
}
