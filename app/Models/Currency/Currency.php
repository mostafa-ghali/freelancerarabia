<?php

namespace App\Models\Currency;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    use HasFactory;

    protected $guarded = [];
    protected $table = 'currencies';
    protected $fillable = ['name', 'code', 'symbol', 'active'];



    public function fromCurrencyExchange(){
        return $this->hasOne(CurrencyExchange::class,'from_currency_id');
    }

    public function toCurrencyExchange(){
        return $this->hasOne(CurrencyExchange::class,'to_currency_id');
    }
}
