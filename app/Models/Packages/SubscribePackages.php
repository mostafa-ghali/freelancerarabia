<?php

namespace App\Models\Packages;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SubscribePackages extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'subscribe_packages';


    protected $fillable = [
        'id', 'package_id', 'user_id', 'no_monthes', 'stripe_token', 'is_paid', 'price', 'payment_id', 'start_date', 'end_date'
    ];

    public function package()
    {
        return $this->belongsTo(Packages::class);
    }

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class);
    }


}
