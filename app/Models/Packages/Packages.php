<?php

namespace App\Models\Packages;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Packages extends Model
{
    use HasFactory ,SoftDeletes;
    protected $table = 'packages';


    protected $fillable = [
        'id','package_name_ar','short_description_ar','short_description_en','old_price','new_price','package_name_en','no_months','created_at','updated_at','deleted_at'
    ];

    public function Features(){
        return $this->belongsTo(packagesFeatures::class , 'package_id','id');
    }


    function getPackageNameAttribute()
    {

        return @$this['package_name_'.app()->getLocale()]??$this['package_name_en'];

    }
    function getShortDescriptionAttribute()
    {

        return @$this['short_description_'.app()->getLocale()]??$this['short_description_en'];

    }




    public function getCurrencyNewPriceAttribute()
    {
        return current_value_in_currency($this->new_price);
    }

    public function getCurrencyNewPriceSymbolAttribute()
    {
        return current_value_in_currency($this->new_price, true);
    }


    public function getCurrencyOldPriceAttribute()
    {
        return current_value_in_currency($this->old_price);
    }

    public function getCurrencyOldPriceSymbolAttribute()
    {
        return current_value_in_currency($this->old_price, true);
    }
}
