<?php

namespace App\Models\Packages;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class packagesFeatures extends Model
{
    use HasFactory , SoftDeletes;
    protected $table = 'packages_features';
    protected $fillable = ['id','package_id','feature_ar','feature_en','value','price','created_at','updated_at','deleted_at'];
}
