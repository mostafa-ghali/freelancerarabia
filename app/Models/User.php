<?php

namespace App\Models;

use App\Models\Cart\Cart;
use App\Models\Comments\ServicesComments;
use App\Models\Comments\ServicesCommentsReplay;
use App\Models\Item\Item;
use App\Models\Messages\Messages;
use App\Models\Order\MovementOrderItem;
use App\Models\Order\Order;
use App\Models\Services\Service;
use App\Models\Stripe\StripeCustomerAccounts;
use App\Models\Tags\UserTags;
use App\Models\User\UserFav;
use App\Models\User\UserLanguages;
use App\Models\User\UserOccupation;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Laravel\Sanctum\HasApiTokens;
use Stripe\StripeClient;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use HasApiTokens, HasFactory, Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $table = 'users';
    /*    protected $fillable = [
            'name', 'email', 'password', 'service_owner', 'user_type', 'service_owner', 'country', 'description'
            , 'otp', 'is_verified', 'social_provider_id', 'messages_push_notifications', 'social_provider',
            'active', 'deactivation_reason', 'deactivation_note', 'image', 'work_title', 'graduation_date',
            'education', 'university', 'certificate_image', 'login_at', 'logout_at','online', 'device_key', 'response_speed','seller_type', 'created_at', 'updated_at','customer_stripe_account', 'deleted_at','stripe_customer_id','category_id', 'customer_type',
        ];*/


    protected $guarded = ['id'];
    protected $appends = ['created_at_readable', 'updated_at_readable', 'image', 'certificate_image'];
    protected $guard = 'web';


    const  SELLER_TYPE = [
        'fresh_talent',
        'super_talent',
        'top_talent',
    ];
    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'customer_type' => 'integer',
    ];

    const  Customer_Type = [
        'customer' => 1,
        'freelancer' => 2,
    ];

    public static function findOrCreateBySocial($social_user, $provider)
    {
        $authUser = User::where('social_provider', $provider)->where('social_provider_id', $social_user->id)->first();
        if ($authUser) {
            return $authUser;
        }

        if (User::where('email', $social_user->email)->count() > 0) {
            return 'user_email_exists';
        }

        $name = explode(' ', $social_user->name);

        $user = User::create([
            'name' => $name,
            'email' => $social_user->email,
            'password' => Hash::make(uniqid()),
            'social_provider' => $provider,
            'social_provider_id' => $social_user->id,
            'email_verified_at' => date('Y-m-d H:i:s'),
            'active' => 1,
            'messages_push_notifications' => 1,
            'user_type' => 3,
        ]);

        return $user;
    }


    public static function boot()
    {
        parent::boot(); // TODO: Change the autogenerated stub
        self::deleted(function ($model) {
            Cart::where('user_id', $model->id)->delete();
            ServicesCommentsReplay::where('user_id', $model->id)->delete();
            UserFav::where('user_id', $model->id)->delete();
            Order::where('user_id', $model->id)->delete();
            Item::where('user_owner_service_id', $model->id)->delete();
            Service::where('user_id', $model->id)->delete();
            ServicesComments::where('user_id', $model->id)->delete();
            UserTags::where('user_id', $model->id)->delete();
            Messages::where('sender_id', $model->id)->delete();
            Messages::where('receiver_id', $model->id)->delete();
        });
    }

    public function getCreatedAtReadableAttribute()
    {
        return date('d-m-Y', strtotime($this->created_at));
    }

    public function getUpdatedAtReadableAttribute()
    {
        return date('d-m-Y', strtotime($this->updated_at));
    }

    public function getImageAttribute()
    {
        return !empty($this->attributes['image']) ? app_url('uploads/user_image/' . $this->attributes['image']) : app_url('web_ar/img/default-avatar.jpg');
    }

    public function getCertificateImageAttribute()
    {
        return !empty($this->attributes['certificate_image']) ? app_url('uploads/user_certificate/' . $this->attributes['certificate_image']) : app_url('web_ar/img/25010166.jpg');
    }

    public function messeges()
    {
        return $this->belongsTo(Messages::class, 'id', 'sender_id');
    }

    public function services()
    {
        return $this->hasMany(Service::class);
    }

    public function userOccupation()
    {
        return $this->hasMany(UserOccupation::class);
    }

    public function userLanguages()
    {
        return $this->hasMany(UserLanguages::class);
    }

    public function injectToken($token)
    {
        return $this->token = $token;

    }


    // Rest omitted for brevity

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function getStripeCustomerId()
    {
        $stripe = new StripeClient(
            env('STRIPE_SECRET')
        );

        if (empty($this->stripe_customer_id)) {
            $stripe_customer_id = $stripe->customers->create(['name' => $this->attributes['name'], 'email' => $this->attributes['email'],])->id;
            $this->stripe_customer_id = $stripe_customer_id;
            $this->save();
        }
        return $this->stripe_customer_id;
    }

    public function getStripeAccountId()
    {
        $stripe = new StripeClient(
            env('STRIPE_SECRET')
        );
        if (empty($this->customer_stripe_account)) {
            $account_id = $stripe->accounts->create([
                'type' => 'standard',

            ]);
            $this->customer_stripe_account = $account_id->id;
//            $account_id = $stripe->accounts->create(['type' => 'express']);
//            $this->customer_stripe_account = $account_id->id;
            $this->save();
        }
        return $this->customer_stripe_account;
    }
}
