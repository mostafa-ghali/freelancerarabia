<?php

namespace App\Models\Category;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SubCategoryFiltersOptionsItems extends Model
{
    use HasFactory , SoftDeletes;
    protected $table = 'sub_category_filter_options_items';
    protected $fillable = [
        'id','sub_category_filters_options_id','option_filter_name_ar','option_filter_name_en',
        'value','created_at','updated_at','deleted_at'
    ];


    protected function getOptionFilterNameAttribute()
    {
        return @$this->attributes['option_filter_name_'.app()->getLocale()] ??$this->attributes['option_filter_name_en'];
    }
}
