<?php

namespace App\Models\Category;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SubCategoryFiltersOptions extends Model
{
    use HasFactory ,SoftDeletes;
    protected $table = 'sub_category_filter_options';
        protected $fillable = ['id','sub_category_option_name_ar','sub_category_option_name_en',
            'sub_category_id','value_filter','created_at','updated_at','deleted_at'];


        public function subCategoryFiltersOptions(){
            return $this->hasMany(SubCategoryFiltersOptionsItems::class ,);
        }


    protected function getSubCategoryOptionNameAttribute()
    {
        return @$this->attributes['sub_category_option_name_'.app()->getLocale()] ??$this->attributes['sub_category_option_name_en'];
    }
}
