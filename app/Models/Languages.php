<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Languages extends Model
{
    use HasFactory;
    protected $table = 'languages';
    protected $fillable = ['id' , 'lang_code' , 'lang_name' , 'lang_name_automated' , 'lang_flag'];

    public function getImageAttribute()
    {
        return !empty($this->attributes['lang_flag']) ? app_url('uploads/languages/'.$this->attributes['lang_flag']) : null;
    }
}
