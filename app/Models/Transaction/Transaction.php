<?php

namespace App\Models\Transaction;

use App\Models\Order\Order;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Transaction extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'transactions';
    const  TYPE = [
        'Charge' => 1,
        'Withdraw' => 2,
    ];
    protected $fillable = [
        'user_id',
        'order_id',
        'type',
        'amount',
        'description',
        'available_withdraw',
    ];

    protected $casts = [
        'amount' => 'float',
        'type' => 'integer',
    ];

    public function order()
    {

        return $this->belongsTo(Order::class);

    }


    public function user()
    {
        return $this->belongsTo(User::class);
    }


    public function getTypeLabelAttribute()
    {

        return array_search($this->type, self::TYPE);
    }


}
