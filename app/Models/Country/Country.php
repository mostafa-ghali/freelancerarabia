<?php

namespace App\Models\Country;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    use HasFactory;

    public $timestamps =false;
    protected $table = 'country';
//    protected $fillable = ['id', 'screen_id', 'language_id', 'label_id', 'label_text_en', 'label_text_automated', 'label_text_override'];
    public function getNameAttribute()
    {
        return @$this->attributes['name_' . app()->getLocale()] ?? $this->attributes['name_en'];
    }
}
