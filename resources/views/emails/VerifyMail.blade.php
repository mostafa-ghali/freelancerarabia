<!Doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Email Verification</title>
</head>
<body>
<h2>Hi,</h2>
<h3>{{$details['title']}}</h3>
<p>Hello,
    Freelancer Arabia strives to maintain the trust of its users and to protect their safety. By entering the verification code below, you can verify your email address:
    Email Verification Code
    Your Email verification code is: <b>{{$details['body']}}</b></p>
    The code doesn’t work? Request another one
    Please reach out if you have trouble verifying your email. You can contact us on Freelance-arabia.com
    Regards,
    Freelance Arabia Team

<p>Thank You</p>
</body>
</html>
