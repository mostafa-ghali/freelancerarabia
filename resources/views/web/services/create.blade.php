@extends('web.layouts.master')
@section('content')
    <!-- begin top-services -->
    {{-- <div class="top-services">
         <div class="owl-carousel top-services-carousel">
             @foreach($categories as $category)
                 @if(getLang() =='ar')
                     <div class="item text-center">{{$category->name_ar}}</div>
                 @else
                     <div class="item text-center">{{$category->name_en}}</div>
                 @endif
             @endforeach
         </div>
     </div>--}}
    <!-- end top-services -->

    <!-- begin main -->
    <main class="container create-service">
        @if(session()->has('alert-success'))
            <div class="custom-card-header">
                <div class="alert alert-success" style="text-align: center">{{session()->get('alert-success')}}</div>
            </div>
        @endif
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="site-padding mt-5">
            <form class="font-md edit-form" method="post" action="{{route('store.service.store')}}"
                  enctype="multipart/form-data">
                @csrf
                <div class="row section mb-5">
                    <div class="col-12">
                        <h4 class="text-start my-2 section-title">
                            {{t_label('add_new_service')}}
                        </h4>
                    </div>

                    <div class="container ">
                        <nav class="d-flex steps-header justify-content-between align-items-center">
                            <div class="nav w-75 nav-pills nav-fill mb-0" id="nav-tab">
                                <button class="btn in-progress step nav-link shadow-none" id="step1-tab"
                                        data-step="service" type="button">
                                    <span class="badge badge-pill">1</span>
                                    <span>{{t_label('Service Info')}}</span>
                                </button>
                                <button class="btn step nav-link disabled  shadow-none" id="step2-tab" data-step="media"
                                        type="button">
                                    <span class="badge badge-pill  bg-secondary">2</span>
                                    <span>{{t_label('Media')}}</span>
                                </button>
                                <button class="btn step nav-link disabled shadow-none" id="step3-tab"
                                        data-step="instructions" type="button">
                                    <span class="badge badge-pill bg-secondary">3</span>
                                    <span>{{t_label('Instructions')}}</span>
                                </button>
                            </div>
                            {{--                            <div class="progress-wrapper">--}}
                            {{--                                <span>Completion Rate: 30%</span>--}}
                            {{--                                <div class="progress">--}}
                            {{--                                    <div class="progress-bar" role="progressbar" aria-valuenow="33" aria-valuemin="0" aria-valuemax="100"></div>--}}
                            {{--                                </div>--}}
                            {{--                            </div>--}}
                        </nav>
                    </div>

                    <div class="container">
                        <div class="custom-card">
                            <div class="custom-card-body">
                                {{-- service tab --}}
                                <div id="service" class="step-content active">
                                    <div class="row">
                                        <div class="col-lg-6 col-12">
                                            <div class="form-group mb-3">
                                                <label for="title_letters"
                                                       class="mb-2"> {{t_label('service_title')}}</label>
                                                <textarea class="form-control" id="title_letters" name="service_title"
                                                          rows="5"></textarea>
                                                <div class="font-xs" id="count_title_letters"></div>
                                            </div>

                                            <div class="form-group mb-3">
                                                <label for="description"
                                                       class="form-label"> {{t_label('service_description')}}</label>
                                                <textarea class="form-control" id="description"
                                                          name="service_description" rows="5"></textarea>
                                                <div class="font-xs mt-2">
                                                    {{t_label('enter_accurate_service_description')}}
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="form-group mb-2">
                                                        <label for="period">{{t_label('delivery_time')}}</label>
                                                        <div class="input-group mb-3">
                                                            <input type="number" class="form-control" min="1"
                                                                   name="deliver_time" id="period">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text">{{t_label('Days')}}</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-lg-6">
                                                    <div class="form-group mb-4">
                                                        <label for="service_price">{{t_label('price')}}</label>
                                                        <div class="input-group mb-3">
                                                            <input type="number" class="form-control" min="1" id="service_price"
                                                                   name="service_price">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text">$</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-lg-6">
                                                    <input type="checkbox" name="chk"
                                                           id="chk" {{ old('chk') == 'checked' ? 'checked' : '' }} >
                                                    <label for="chk">{{t_label('additional_services')}}</label>
                                                </div>
                                            </div>

                                            <div class="card-body">
                                                <div id="fines-list">
                                                    <div id="fine-1-inputs" class="fine-inputs" style="display: none">
                                                        <div class="col-12 mt-2">
                                                            <label
                                                                class="form-label"> {{t_label('additional_services')}}</label>
                                                            <div class="bg-light  add-additional-service ">
                                                                <label class="d-block mt-1">
                                                                    <input
                                                                        class="form-check-input additional-service-check w-100"
                                                                        type="checkbox"
                                                                        value="" name="if_add_service"
                                                                        data-additional-service-id="1"/> خدمة
                                                                    إضافية 1
                                                                </label>
                                                                <div class="row additional-service d-none"
                                                                     data-additional-service-id="1">
                                                                    <div class="col-12 mt-1">
                                                                        <label for="additionalTitle1"
                                                                               class="form-label">{{t_label('service_title')}}</label>
                                                                        <textarea class="form-control"
                                                                                  id="additionalTitle1"
                                                                                  name="additionalTitle1[]"
                                                                                  rows="3"></textarea>
                                                                        <div class="font-xs"
                                                                             id="add_service_count_title_letters"></div>


                                                                    </div>
                                                                    <div class="col-12 mt-3">
                                                                        <label for="additionalProce1"
                                                                               class="form-label">تكلفة الاضافة
                                                                            $</label>
                                                                        <input type="text" class="form-control"
                                                                               id="additionalProce1"
                                                                               name="additionalProce1[]"
                                                                               placeholder="مقابل إضافة الخدمة"/>
                                                                    </div>
                                                                    <div class="col-12 mt-3">
                                                                        <label
                                                                            class="form-label me-3">{{t_label('Delivery time')}}</label>
                                                                        <div class="col-lg-6 col-xl-6">
                                                                            <select class="form-control"
                                                                                    name="additional_deliver_time[]"
                                                                                    id="discount_type">
                                                                                <option
                                                                                    value="1"> {{t_label('Same appointment')}}</option>
                                                                                <option
                                                                                    value="0"> {{t_label('Extra appointment')}}</option>
                                                                                {{--                                                                                <option value="1"> نفس الموعد</option>--}}
                                                                                {{--                                                                                <option value="0"> موعد اضافي</option>--}}
                                                                                {{--                                                                                <option value="0"> موعد اضافي</option>--}}
                                                                            </select>
                                                                            <span class="invalid-feedback" role="alert"><strong></strong></span>
                                                                        </div>

                                                                        {{--                                    <label class="d-block d-md-inline ">--}}
                                                                        {{--                                        <input class="form-check-input additional-service-period-add" name="additionalPeriodType1" type="radio" value="1"  checked/> تسليم في نفس الموعد--}}
                                                                        {{--                                    </label>--}}
                                                                        {{--                                    <label class="d-block d-md-inline ms-0 ms-md-5">--}}
                                                                        {{--                                        <input class="form-check-input additional-service-period-add" name="additionalPeriodType1" type="radio" value="2" /> إضافة وقت اضافي--}}
                                                                        {{--                                    </label>--}}
                                                                        <br>
                                                                        <div id="add_time">
                                                                            {{--                                         <span class="d-block d-md-inline ms-0 ms-md-3 d-none additional-service-period-count">--}}
                                                                            {{--                          <button type="button"  class="btn btn-primary counter-btn" data-inc="1"><i class="fas fa-plus"></i></button>--}}
                                                                            {{--                          <span  style="min-width:50px" class="d-inline-block text-center counter">1</span>--}}
                                                                            {{--                          <button type="button"  class="btn btn-primary counter-btn" data-inc="-1" disabled><i class="fas fa-minus"></i></button>--}}
                                                                            {{--                          --}}
                                                                            <span
                                                                                class="ms-4">{{t_label('Extra days for delivery')}}</span>
                                                                            {{--                                                                            <span class="ms-4">عدد الايام الاضافية للتسليم</span>--}}
                                                                            <input type="number" min="1"
                                                                                   class="form-control col-lg-6 col-xl-6"
                                                                                   name="addtional_time[]">
                                                                            {{--                         </span>--}}
                                                                        </div>

                                                                    </div>
                                                                </div>

                                                            </div>

                                                        </div>
                                                    </div>

                                                </div>
                                                <br>
                                                <button type="button" class="btn-primary addRow col-lg-3"
                                                        style="display: none">{{t_label('add_additional_service')}}</button>
                                            </div>


                                        </div>

                                        <div class="col-lg-5 col-12">


                                            <div class="form-group mb-2">
                                                <label for="service_category_id"
                                                       class="form-label">{{t_label('category')}}</label>
                                                <select class="form-control" name="service_category_id"
                                                        id="service_category_id">
                                                    <option selected disabled>{{t_label('choose_section')}}</option>
                                                    @foreach($categories as $category)
                                                        <option value="{{$category->id}}">{{$category->name}}</option>

                                                    @endforeach
                                                </select>
                                            </div>

                                            <div class="form-group mb-2">
                                                <label for="service_sub_category_id"
                                                       class="form-label">{{t_label('sub_cat')}}</label>
                                                <select class="form-control" name="service_sub_category_id"
                                                        id="service_sub_category_id">
                                                    @include('web.services.subCategoryService')
                                                </select>
                                            </div>


                                            <div class="form-gruop">
                                                <label class="form-label"
                                                       id="service_tags">{{t_label('keywords')}}</label>
                                                <div class="bg-light py-2 rounded">
                                                    <input class="form-control tagify" name="service_tags">
                                                </div>
                                            </div>


                                        </div>
                                    </div>
                                </div>


                                {{-- media tab --}}
                                <div id="media" class="step-content ">
                                    <div class="row">
                                        <div class="col-lg-12 col-12">
                                            <div class="form-group mb-2">
                                                <label class="form-label"> {{t_label('service_gallery')}}</label>
                                                <label class="bg-light border-grey p-1 text-center py-5 w-100"
                                                       id="galleryWrapper">
                                                    <div>
                                                        <input type="file"
                                                               accept="image/png,image/jpeg,image/jpg,application/pdf"
                                                               class="btn btn-primary d-none" name="media[]" multiple
                                                               id="galleryOpenBtn">
                                                    </div>

                                                    <div class="mt-3 font-size-xs w-100">
                                                        {{t_label('gallery_notes')}}
                                                        <br>
                                                        <span class="text-blue fs-xxl">click here</span>
                                                    </div>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                {{-- Occupation tab --}}
                                <div id="instructions" class="step-content">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="form-group mb-2">
                                                <label for="instructions"
                                                       class="form-label">{{t_label('instructions_service_buyer')}}</label>
                                                <textarea class="form-control" id="instructions"
                                                          name="details_for_customer"
                                                          rows="5"></textarea>
                                                <div class="font-xs">
                                                    {{t_label('details_client_should_provide')}}
                                                </div>
                                            </div>

                                            <div class="form-group mb-2">
                                                <label for="question1"
                                                       class="form-label">{{t_label('common_questions')}}</label>
                                                <input type="text" class="form-control" id="question1" name="question1"
                                                       placeholder=" {{t_label('add_question')}}"/>
                                                <textarea class="form-control mt-3" id="answer1" name="answer1" rows="5"
                                                          placeholder="{{t_label('add_answer')}}"></textarea>
                                            </div>


                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>


                    <div class="col-12 my-5 text-end">
                        <button type="button" id="nextStep2" style="min-width: 120px" class="btn btn-primary font-sm"
                                data-next="media">
                            {{--                            {{t_label('add_service')}}--}}
                            {{t_label('Next')}}
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </main>
    <!-- end main -->

@endsection
@section('scripts')
    <script>

        const nextStepBtn = document.querySelector('#nextStep2')
        const steps = document.querySelectorAll('.step')
        const stepContent = document.querySelectorAll('.step-content')
        // const completionStatusBar = document.querySelector('form.edit-form .progress-bar')
        // const completionAmount = document.querySelector('.progress-wrapper span:first-of-type')


        function handleActiveTab(id) {
            if (id === 'media') document.querySelector('#step1-tab').classList.add('done')
            if (id === 'instructions') document.querySelector('#step2-tab').classList.add('done')
            stepContent.forEach(step => {
                step.style.display = 'none'
                if (step.id === id) {
                    step.style.display = 'block'
                    const stepNum = document.querySelector(`[data-step='${id}']`)
                    stepNum.classList.add('in-progress')
                    stepNum.classList.remove('disabled')
                    stepNum.querySelector('span').classList.remove('bg-secondary')
                }
            })

        }

        function topFunction() {
            document.body.scrollTop = 0; // For Safari
            document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
        }

        const stepsList = {
            'service': {
                id: 'step1-tab',
                nextStep: 'media',
                completion: 30
            },
            'media': {
                id: 'step2-tab',
                nextStep: 'instructions',
                completion: 70
            },
            'instructions': {
                id: 'step3-tab',
                completion: 70
            }

        }

        steps.forEach(step => {
            step.addEventListener('click', function () {
                const selectedTab = this.dataset.step;
                handleActiveTab(selectedTab)

            })
        })


        // ['personal','educationInfo','occupation']

        nextStepBtn.addEventListener('click', function (e) {
            let nextTab = this.dataset.next;
            if ( stepsList[nextTab] ) {
                e.preventDefault()
            }
            this.textContent = stepsList[nextTab].id === 'step3-tab' ? '{{t_label('Add Service')}}' : '{{t_label('Next')}}'
            this.dataset.next = stepsList[nextTab].nextStep;
            this.type = stepsList[nextTab].id === 'step3-tab' ? 'submit' : 'button'
            // completionStatusBar.style.width = `${stepsList[nextTab].completion}%`
            // completionAmount.textContent = `${stepsList[nextTab].completion}%`

            handleActiveTab(nextTab)
            topFunction()
        })
    </script>
    <script>

        var inputElm = document.querySelector('.tagify'),
            tagify = new Tagify(inputElm);
        $(document).ready(function () {
            $('#add_time').hide();
        });
        $('#discount_type').change(function () {
            let val = $(this).val();
            if (val === '0') {
                $('#add_time').show();
            }
        });
    </script>
    <script>
        $('#chk').change(function () {
            $this = $(this);
            console.log($this);
            if ($this.is(':checked')) {
                addFineInputs();
            } else {
                $('#fines-list').html('');
                $('.addRow').hide();
            }
        });
        $('.addRow').on('click', function () {
            addFineInputs();
        });

        function addFineInputs() {

            let finesInputsCount = $('.fine-inputs').length;
            finesInputsCount += 1;
            let finesHtml = '<div id="fine-' + finesInputsCount + '-inputs" class="fine-inputs"><div class="form-group row">' +
                '   <div class="col-12 mt-2">' +
                ' <div class="row additional-service">' +
                '  <div class="col-12 mt-1">' +
                ' <label for="additionalTitle1" class="form-label">{{t_label('service_title')}}</label>' +
                '<textarea class="form-control" id="additionalTitle1" name="additionalTitle1[]" rows="3"></textarea>' +
                '<div class="font-xs" id="add_service_count_title_letters"></div>' +
                ' </div>' +
                '<div class="col-12 mt-3">' +
                '<label for="additionalProce1" class="form-label">{{t_label('add_cost').' '.'$'}} </label>' +
                ' <input type="text" class="form-control" id="additionalProce1" name="additionalProce1[]" />' +
                '  </div>' +
                '<div class="col-12 mt-3">' +
                ' <label  class="form-label me-3"> {{t_label('delivery_time')}}</label>' +
                '   <div class="col-lg-6 col-xl-6">' +
                '  <select class="form-control" name="additional_deliver_time[]" id="discount_type">' +
                '  <option value="1" > {{t_label('same_date')}}</option>' +
                '     <option value="0"  > {{t_label('extra_date')}}</option>' +
                '    </select>' +
                ' <span class="invalid-feedback" role="alert"><strong></strong></span>' +
                '  </div>' +
                ' <br>' +
                ' <div id="add_time">' +
                ' <span class="ms-4">{{t_label('extra_days')}}</span>' +

                '<input type="number" min="1" class="form-control col-lg-6 col-xl-6" name="addtional_time[]" >' +
                ' </div>' +
                '<br>' +

                '<button type="button" class="d-inline btn-delete-fi btn btn-danger " data-id="' + finesInputsCount + '" id="btn-delete-fi-' + finesInputsCount + '">{{t_label('delete')}}</button>' +

                '   </div>' +

                '</div>' +

                ' </div>' +

                ' </div>' +
                '</div>' +
                '</div>';

            $('#fines-list').append(finesHtml);
            $('.addRow').show();
        }

        $(document).on('click', '.btn-delete-fi', function () {
            let ID = $(this).attr('data-id');
            $('#fine-' + ID + '-inputs').remove();
        });

        //
        // $(document).ready(function (){
        //    $('#fine_ticket_no').style(hide);
        //    $('#fine_amount').style(hide);
        //    $('#fine_reason').style(hide);
        // });

    </script>
    <script>
        var text_max = 100;
        $('#count_title_letters').html(text_max + ' {{t_label('remaining')}}');

        $('#title_letters').keyup(function () {
            var text_length = $('#title_letters').val().length;
            var text_remaining = text_max - text_length;
            $('#count_title_letters').html(text_remaining + ' {{t_label('remaining')}}');
        });

        var text_max_add_service = 100;
        $('#add_service_count_title_letters').html(text_max_add_service + ' {{t_label('remaining')}}');

        $('#additionalTitle1').keyup(function () {
            var text_length = $('#additionalTitle1').val().length;
            var text_remaining = text_max_add_service - text_length;
            $('#add_service_count_title_letters').html(text_remaining + ' {{t_label('remaining')}}');
        });
    </script>
    <script>
        $('#service_category_id').change(function () {
            let category_id = $(this).val();
            let actionURL = '{{route('services.getSubCategoryBaseCategory')}}' + '/' + category_id;
            $.get(actionURL, function (response) {
                if (response.status) {
                    $('#service_sub_category_id').html(response.items_html);
                }
            });
        })
    </script>

@endsection

