<div class="card service-card item ">
    <a href="{{route('store.service.details',$service->id)}}">
        <div class="card-img-top"

        >
            <div id="carouselExampleControls" class="carousel slide"
                 data-bs-ride="carousel">
                <div class="carousel-inner">
                    @foreach($service->images as $key => $slider)
                        <div class="carousel-item {{$key == 0 ? 'active' : '' }}">
                            <img src=" {{asset($slider->image)}}"
                                 class="d-block w-100"
                                 alt="...">
                        </div>
                    @endforeach
                </div>
                <button class="carousel-control-prev" type="button"
                        data-bs-target="#carouselExampleControls"
                        data-bs-slide="prev">
                                                        <span class="carousel-control-prev-icon"
                                                              aria-hidden="true"></span>
                    <span class="visually-hidden">{{t_label('Previous')}}</span>
                </button>
                <button class="carousel-control-next" type="button"
                        data-bs-target="#carouselExampleControls"
                        data-bs-slide="next">
                                                        <span class="carousel-control-next-icon"
                                                              aria-hidden="true"></span>
                    <span class="visually-hidden">{{t_label('Next')}}</span>
                </button>
            </div>
        </div>
    </a>
    <div class="card-body py-0">
        <h5 class="card-title">
            <div class="avatar">
                {{--                                                <a href="profile-seller.html">--}}
                <a href="{{route('store.profile_seller',$service->ServiceOwner->id)}}">
                    <img src="{{asset($service->ServiceOwner->image)}}"
                         class="avatar-image"/>
                    <span class="avatar-title ms-2">
                 {{$service->ServiceOwner->name}}
                    </span>
                </a>
            </div>
        </h5>
        <p class="card-text">
            {{$service->title}}
        </p>
        <div class="service-card-action">

            {{--                                            <div class="service-card-favorite active">--}}
            {{--                                                <i class="fas fa-heart"></i>--}}
            {{--                                            </div>--}}
        </div>
    </div>
    <a href="{{route('store.service.details',$service->id)}}"
       class="btn btn-primary service-card-btn"
    >{{t_label('service_details')}}</a
    >
</div>
