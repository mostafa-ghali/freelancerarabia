@extends('web.layouts.master')
@section('content')
    <!-- begin top-services -->
  {{--  <div class="top-services">
        <div class="owl-carousel top-services-carousel">
            @foreach($categories as $category)
                @if(getLang() =='ar')
                    <div class="item text-center">{{$category->name_ar}}</div>
                @else
                    <div class="item text-center">{{$category->name_en}}</div>
                @endif
            @endforeach
        </div>
    </div>--}}
    <!-- end top-services -->

    <!-- begin main -->
    <main class="container-fluid seller_profile">
        <div class="site-padding">
            <div class="row g-4">
                <div class="col-12  mt-5 ">
                    <div class=" prof-id">
                        <div class=" mt-5">
                            <div class="row">
                                <div class="col-12 col-md-12 mb-3 ">
                                    <div class="avatar avatar-big">
                                        <img
                                            src="{{auth()->user()->image}}"
                                            class="avatar-image"
                                            alt=""
                                        />
                                    </div>
                                </div>
                                <div class="col-12 col-md-12 ">
                                    <div class="text-center text-black  font-xl">
                                        {{auth('web')->user()->name}}
                                        <img src="{{asset('web_ar/img/verified.png')}}" alt="" class="ms-2">
                                    </div>
                                    <div class=" mt-2 mt-sm-2 font-light text-center   text-warning">
                                        <span>
                                            <i class="fas fa-star fa-sm p-0"></i>
                                            <i class="fas fa-star fa-sm p-0"></i>
                                            <i class="fas fa-star fa-sm p-0"></i>
                                            <i class="fas fa-star fa-sm p-0"></i>
                                            <i class="fas fa-star fa-sm p-0"></i>
                                        </span>
                                        <span class="text-secondary">(5)</span>
                                    </div>
                                    <div class="text-center  mt-2" style="font-size:13px">
                                        <i class="fas fa-briefcase"></i>
                                        <span style="font-size:13px">{{auth('web')->user()->work_title}}</span>
                                        <i class="fas fa-map-marker-alt"></i>
                                        {{auth('web')->user()->country}}
                                    </div>

                                </div>
                                <div class="">

                                    <a href="{{route('profile.index')}}"
                                       class="btn btn-primary botton-right"> {{t_label('edit_profile')}} </a>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-lg-4 mt-4 mb-3">
                    <div class="row">
                        <div class="col-12">
                            <div class="custom-card mb-4">
                                <div class="custom-card-body">
                                    <div class="d-flex justify-content-between">
                                        <span class="font-md" style="color:black">
                                            {{t_label('description')}}
                                        </span>
                                        <span>
                                            <a href="{{route('profile.index')}}" class="text-grey"> <i
                                                    class="fas fa-edit"></i></a>
                                        </span>
                                    </div>
                                    <div class="">
                                        <p class="py-3" >
                                            {{auth('web')->user()->description}}
                                        </p>
                                        <p class="show-more cursor-pointer text-primary font-md">more</p>
                                    </div>


                                    {{-- <div class="d-flex justify-content-between align-items-center">
                            <span class="font-md " >
                             {{t_label('description')}}
                            </span>
                                        <span class="mb-1">
                            <a href="#" class="text-grey"> <i class="fas fa-edit"></i></a>
                            </span>
                                    </div>
                                    <div class="show-more-box">
                                        <p class="mt-3">
                                            {{auth('web')->user()->description}}
                                        </p>
                                        <p class="show-more cursor-pointer text-primary font-sm">المزيد</p>
                                    </div> --}}
                                </div>
                            </div>

                            <div class="custom-card mb-4">
                                <div class="custom-card-body">
                                    <div class="d-flex justify-content-between">
                                        <span class="font-md" style="color:black">
                                            {{t_label('skills')}}
                                        </span>
                                        <span>
{{--                           <a href="#" class="text-grey"> <i class="fas fa-edit"></i></a>--}}
                                        </span>
                                    </div>
                                    <div class="mt-2 d-flex flex-wrap gap-2 p-0">
                                        @foreach($user_tags as $tag)
                                            <span
                                                class="badge badge-pill bg-outline-secondary cursor-pointer" >{{$tag->tag}}</span>
                                        @endforeach
                                    </div>
                                </div>
                            </div>

                            <div class="custom-card mb-2r">
                                <div class="custom-card-body">
                                    <div class="d-flex justify-content-between">
                                        <span class="font-md" style="color:black">
                                            {{t_label('education')}}
                                        </span>
                                        <span>
{{--                           <a href="#" class="text-grey"> <i class="fas fa-edit"></i></a>--}}
                                        </span>
                                    </div>
                                    <div class="mt-2 d-flex flex-wrap flex-column gap-2 p-0">
                                        <div class="font-sm d-block">
                                            {{auth('web')->user()->education}}
                                        </div>
                                        <div class="font-xs font-light">
                                            {{auth('web')->user()->university.' ' .'-'.' ' . date('Y', strtotime(auth('web')->user()->graduation_date))   }}
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="custom-card mb-lg-0 mb-4">
                                <div class="custom-card-body row  mx-0">
                                    <div class=" font-md mt-2 " style="color:black">{{t_label('date_registration')}}</div>
                                    <div class=" mt-0 mt-sm-2  font-light">
                                        {{auth('web')->user()->created_at_readable }}</div>
                                    <div class=" font-md mt-2 ">{{t_label('last_seen')}}</div>
                                    <div class=" mt-0 mt-sm-2  font-light">
                                        {{time_elapsed_string(auth('web')->user()->logout_at)}}
                                    </div>

                                    <hr class="mt-3">
                                    <div class="d-flex justify-content-between">
                                        <span class="font-md">
                                            {{t_label('languages')}}
                                        </span>
                                        <span>
{{--                           <a href="#" class="text-grey"> <i class="fas fa-edit"></i></a>--}}
                                        </span>
                                    </div>
                                    <div class="mt-2 d-flex flex-wrap gap-2 p-0">
                                        <span class="badge bg-secondary cursor-pointer">العربية</span>
                                        <span class="badge bg-secondary cursor-pointer">English</span>
                                    </div>
                                </div>
                            </div>

                            {{--                            <div class="custom-card mb-4">--}}
                            {{--                                <div class="custom-card-body">--}}

                            {{--                                    <div class="d-flex justify-content-between">--}}
                            {{--                          <span class="font-md">--}}
                            {{--                            الشهادات--}}
                            {{--                          </span>--}}
                            {{--                                        <span>--}}
                            {{--                           <a href="#" class="text-grey"> <i class="fas fa-edit"></i></a>--}}
                            {{--                          </span>--}}
                            {{--                                    </div>--}}
                            {{--                                    <div class="mt-2" style="direction: ltr;">--}}
                            {{--                                        <div class="font-sm">--}}
                            {{--                                            <img src="{{asset('web_ar/img/adobe.png')}}" alt=""> <a href="#">Adobe Certified Expert</a>--}}
                            {{--                                        </div>--}}
                            {{--                                        <div class="font-xs font-light">--}}
                            {{--                                            Demonstrates professional level in proficiency with one or more Adobe software products.--}}
                            {{--                                        </div>--}}
                            {{--                                    </div>--}}

                            {{--                                </div>--}}
                            {{--                            </div>--}}
                        </div>
                    </div>
                </div>

                <div class="col-12 col-lg-8  mt-4 mb-3">
                    <div class="custom-card h-100">
                        <div class="custom-card-body h-100">
                            <div class="row pb-5">
                                <div class="font-md col-12 pb-3">
                                    <div class="d-flex align-items-end justify-content-between">
                                        <h4 class="mb-0 text-black" >{{t_label('service_models')}}</h4>
                                        <a href="{{route('store.service.create')}}"
                                           class="btn px-2 py-1 btn-primary ">
                                            <i class="fas fa-plus"></i>
                                            <span>   {{t_label('add_new_service')}}</span>
                                        </a>


                                    </div>

                                    <div class="">
                                        <div class="subcat py-3">

                                            <div class="row">
                                                @if($services->count()>0)
                                                    @foreach( $services as $service)
                                                        <div class="my-2 col-md-4 col-sm-12">
                                                            <div class="card new-shadow">
                                                                <div id="carouselExampleControls"
                                                                     class="carousel slide w-100 drop-shadow rounded"
                                                                     data-bs-ride="carousel">
                                                                    <div class="carousel-inner">
                                                                        @foreach($service->images as  $key => $slider)
                                                                            <div class="carousel-item {{$key == 0 ? 'active' : '' }}">
                                                                                <img src=" {{$slider->image}}"
                                                                                     class="d-block w-100" alt="...">
                                                                            </div>
                                                                        @endforeach
                                                                    </div>
                                                                    <button class="carousel-control-prev" type="button"
                                                                            data-bs-target="#carouselExampleControls"
                                                                            data-bs-slide="prev">
                                                                        <span class="carousel-control-prev-icon"
                                                                              aria-hidden="true"></span>
                                                                        <span class="visually-hidden">Previous</span>
                                                                    </button>
                                                                    <button class="carousel-control-next" type="button"
                                                                            data-bs-target="#carouselExampleControls"
                                                                            data-bs-slide="next">
                                                                        <span class="carousel-control-next-icon"
                                                                              aria-hidden="true"></span>
                                                                        <span class="visually-hidden">Next</span>
                                                                    </button>
                                                                </div>


                                                                <!-- end swiper -->
                                                                <div class="down ">
                                                                    <div class="px-1">
                                                                        <div class="border-bottom">
                                                                            <div
                                                                                class="desc  pt-3 pb-0 d-flex justify-content-between">
                                                                                <a href="#" >{{$service->title}} </a>

                                                                            </div>

                                                                            <span class="service-description">
                                                                             {{$service->description }}
                                                                            </span>
                                                                        </div>


                                                                        <div
                                                                            class="d-flex align-items-baseline justify-content-between pt-1">
                                                                            <p class="str pt-2 mb-2 px-1">
                                                                                <span
                                                                                    class="fa fa-star fa-xs checked"></span>
                                                                                <span class="">
                                                                                {{$service->rate}}
                                                                            </span>
                                                                            </p>
                                                                            <ul class="p-0 mb-0 gap-1 d-flex">
                                                                                <li class="pt-1 options-icons">
                                                                                    <a href="{{route('store.user.add_fav',$service->id)}}">@if ($service->is_favourite)
                                                                                            <i style="color:  red"
                                                                                               class="fas fa-heart"></i> @else
                                                                                            <i style="color: #a7a5a5"
                                                                                               class="fas fa-heart"> </i>@endif
                                                                                    </a>

                                                                                </li>
                                                                                <li class="pt-1">
                                                                                    <a href="#"><i class="fas fa-share"
                                                                                                   style="color: #a7a5a5"></i></a>

                                                                                </li>
                                                                            </ul>
                                                                         </div>

                                                                    </div>


                                                                    <div class="dash p-2">
                                                                        <a href="{{route('store.service.details',$service->id)}}"
                                                                           class=" w-100 d-flex fs-6 strt justify-content-center">  STARTING AT<p class="mb-0">{{$service->currency_price_symbol}}</p></a>


                                                                    </div>
                                                                </div>
                                                            </div><!-- end card -->
                                                        </div>
                                                    @endforeach
                                                @endif
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="text-center my-5">


                        {{ $services->links('web.home.custom_paginate') }}

                    </div>
                </div>

            </div>

        </div>
    </main>
    <!-- end main -->

@endsection
@section('scripts')
    <script>
        var swiper = new Swiper(".mySwiper", {
            slidesPerView: 1,
            slidesPerGroup: 1,
            loop: true,

            loopFillGroupWithBlank: true,
            navigation: {
                nextEl: ".swiper-button-next",
                prevEl: ".swiper-button-prev",
            },

        });
    </script>
@endsection
