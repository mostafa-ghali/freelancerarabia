<div class="service-sample">
    <div id="carouselExampleControls"
         class="carousel slide w-100 drop-shadow rounded"
         data-bs-ride="carousel">
        <div class="carousel-inner">
            @foreach($service->images as $key => $slider)
                <div
                    class="carousel-item {{$key == 0 ? 'active' : '' }}">
                    <img src=" {{asset($slider->image)}}"
                         class="d-block w-100" alt="..." height="200px">
                </div>
            @endforeach
        </div>
        <button class="carousel-control-prev" type="button"
                data-bs-target="#carouselExampleControls"
                data-bs-slide="prev">
                                                            <span class="carousel-control-prev-icon"
                                                                  aria-hidden="true"></span>
            <span class="visually-hidden">{{t_label('Previous')}}</span>
        </button>
        <button class="carousel-control-next" type="button"
                data-bs-target="#carouselExampleControls"
                data-bs-slide="next">
                                                            <span class="carousel-control-next-icon"
                                                                  aria-hidden="true"></span>
            <span class="visually-hidden">{{t_label('Next')}}</span>
        </button>
    </div>

    <div class="font-bold font-md mt-3 px-3">
        {{$service->title}}
    </div>
    <div class="font-bold font-md mt-3 px-3">
        {{--                                                        $--}}
        {{$service->currency_price_symbol}}
    </div>
    <div>
        <a href="{{route('store.service.details',$service->id)}}"
           class="btn btn-primary w-100 mt-3 font-sm service-sample-btn">
            {{t_label('service_details')}}
        </a>
    </div>
</div>
