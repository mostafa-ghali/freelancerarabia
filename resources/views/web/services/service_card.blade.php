<div class="card  {{@$classes}}">
    <div id="carouselExampleControls"
         class="carousel slide w-100 drop-shadow rounded"
         data-bs-ride="carousel">
        <div class="carousel-inner">
            @foreach($service->images as  $key => $slider)
                <div class="carousel-item {{$key == 0 ? 'active' : '' }}">
                    <img src=" {{$slider->image}}" class="d-block w-100" alt="...">
                </div>
            @endforeach
        </div>
        <button class="carousel-control-prev" type="button"
                data-bs-target="#carouselExampleControls" data-bs-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Previous</span>
        </button>
        <button class="carousel-control-next" type="button"
                data-bs-target="#carouselExampleControls" data-bs-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Next</span>
        </button>
    </div>

    <div class="down ">
        <div class="avatarr px-2 py-2">
            <img alt src="{{$service->ServiceOwner->image}}" class="avatar-image"/>
            <a>{{$service->ServiceOwner->name}}</a>
        </div>
        <div class="desc  px-2">
            <div class="border-bottom">
                <a class="service-description  mt-1"
                   href="{{route('store.service.details',$service->id)}}">{{$service->title}} </a>
            </div>
        </div>

        <div class="d-flex align-items-baseline justify-content-between px-2 pt-1">
            <p class="str mb-1 pt-2 px-2">
                <span class="fa fa-star checked"></span>
                <span class="">
                                                        {{$service->rate}}

                                                    </span>
            </p>
            <ul class="p-0 mb-0 d-flex">
                <li class="pt-1">
                    <a href="{{route('store.user.add_fav',$service->id)}}">
                        @if ($service->is_favourite)
                            <i style="color:  red" class="fas fa-heart"></i>
                        @else
                            <i style="color: #a7a5a5" class="fas fa-heart"> </i>
                        @endif</a>

                </li>
                <li class="pt-1">
                    <a href="#"><i style="color: #a7a5a5" class="fas fa-share"></i></a>
                </li>
            </ul>
        </div>


        <div class="bar mt-1">
            <a href="{{route('store.service.details',$service->id)}}"
               class="btn w-100">service details</a>
        </div>
    </div>
</div><!-- end card -->
