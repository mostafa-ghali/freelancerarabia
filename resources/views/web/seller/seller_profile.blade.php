@extends('web.layouts.master')
@section('content')

    <div class="container py-5">

        <div class="custom-card-header mt-5">
            @if(session()->has('alert-success'))
                <div class="alert alert-success" style="text-align: center">{{session()->get('alert-success')}}</div>
            @endif
        </div>

        <form method="post" class="edit-form" enctype="multipart/form-data">

            @csrf
            <div class="row section mb-5">
                <div class="container mb-4">
                    <nav class="d-flex steps-header justify-content-between align-items-center">
                        <div class="nav w-75 nav-pills nav-fill mb-0" id="nav-tab">
                            <button class="btn in-progress step nav-link shadow-none" id="step1-tab"
                                    data-step="personal" type="button">
                                <span class="badge badge-pill">1</span>
                                <span>{{t_label('Personal Info')}}</span>
                            </button>
                            <button class="btn step nav-link disabled  shadow-none" id="step2-tab"
                                    data-step="educationInfo" type="button">
                                <span class="badge badge-pill  bg-secondary">2</span>
                                <span>{{t_label('Education')}}</span>
                            </button>
                            <button class="btn step nav-link disabled shadow-none" id="step3-tab" data-step="languages"
                                    type="button">
                                <span class="badge badge-pill bg-secondary">3</span>
                                <span>{{t_label('Languages')}}</span>
                            </button>
                            <button class="btn step nav-link disabled shadow-none" id="step4-tabstep4-tab" data-step="occupation"
                                    type="button">
                                <span class="badge badge-pill bg-secondary">4</span>
                                <span>{{t_label('Occupation')}}</span>
                            </button>
{{--
                            <button class="btn step nav-link disabled shadow-none" id="step5-tab" data-step="social"
                                    type="button">
                                <span class="badge badge-pill bg-secondary">5</span>
                                <span>{{t_label('Linked Accounts')}}</span>
                            </button>--}}
                        </div>
                        <div class="progress-wrapper">
                            <span>{{t_label('Completion Rate')}}: 20%</span>
                            <div class="progress">
                                <div class="progress-bar" role="progressbar" aria-valuenow="33" aria-valuemin="0"
                                     aria-valuemax="100"></div>
                            </div>
                        </div>
                    </nav>
                </div>

                <div class="container">
                    <div class="custom-card">
                        <div class="custom-card-body  card-border">
                            {{-- Personal tab --}}
                            <div id="personal" class="step-content active">
                                <div class="row">
                                    <div class="col-lg-2 col-12 mb-md-0 mb-3">
                                        <label for="" class="mb-2">{{t_label('personal_photo')}}</label>
                                        <br>
                                        <label for="personal_image">
                                            @if(!is_null(auth('web')->user()->image))
                                                <img data-src="{{asset(auth('web')->user()->image)}}" width="150px"
                                                     height="150px"
                                                     style="border-radius:25px;object-fit: cover;object-position: center">
                                            @else
                                                <img src="{{asset('web_ar/img/default-avatar.jpg')}}" width="150px"
                                                     height="150px"
                                                     style="border-radius:25px;object-fit: cover;object-position: center ">
                                            @endif
                                        </label>
                                        <input type="file" id="personal_image" class="form-control" name="image"
                                               style="display: none">
                                    </div>

                                    <div class="col-lg-5 col-12">
                                        <div class="form-group mb-4">
                                            <label>{{t_label('name')}}</label>
                                            <input class="form-control" name="user_name" id="name"
                                                   value="{{auth('web')->user()->name}}">
                                        </div>

                                        <div class="form-group mb-4">
                                            <label for="country">{{t_label('country')}}</label>

                                            <select class="form-control  " name="country_id">

                                                @foreach($countries as $country)
                                                    <option value="{{$country->id}}">{{$country->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>


                                        <div class="form-group mb-3">
                                            <label for="description" class="mb-2"> {{t_label('about')}}  </label>
                                            <textarea class="form-control" rows="3" id="description"
                                                      name="description"> {{auth('web')->user()->description}} </textarea>
                                        </div>

                                    </div>

                                    <div class="col-lg-4">
                                        <div class="form-group mb-4">
                                            <label for="work_title">{{t_label('job')}}  </label>
                                            <input type="text" class="form-control" name="work_title" id="work_title"
                                                   value="{{auth('web')->user()->work_title}}">
                                        </div>

                                        <div class="form-group mb-4">
                                            <label>{{t_label('skills')}}</label>
                                            <input class="form-control tagify" name="skills" value="{{@$tags}}">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            {{-- Education tab --}}
                            <div id="educationInfo" class="step-content">
                                <div class="row">

                                    <div class="col-lg-5 col-12">
                                        <div class="form-group mb-4">
                                            <label for="education">{{t_label('education')}}</label>
                                            <input class="form-control" name="education" id="education"
                                                   value="{{auth('web')->user()->education}}">
                                        </div>

                                        <div class="form-group mb-4">
                                            <label for="university">{{t_label('university')}}</label>
                                            <input class="form-control" type="text" name="university" id="university"
                                                   value="{{auth('web')->user()->university}}">
                                        </div>

                                        <div class="form-group mb-4">
                                            <label for="graduation_date">{{t_label('graduation_date')}}</label>
                                            <input type="date" class="form-control" name="graduation_date"
                                                   id="graduation_date"
                                                   value="{{auth('web')->user()->graduation_date}}">
                                        </div>

                                        <div class="form-group">
                                            <label for="" class="mb-2">{{t_label('scientific_certificate')}}</label>
                                            <br>
                                            <label for="certificates">
                                                @if(!is_null(auth('web')->user()->certificate_image))
                                                    <img src="{{asset(auth('web')->user()->certificate_image)}}"
                                                         width="150px"
                                                         height="150px" style="border-radius:25px ">
                                                @else
                                                    <label for="certificates"><img
                                                            src="{{asset('web_ar/img/25010166.jpg')}}"
                                                            width="150px" height="150px"
                                                            style="border-radius:25px "> </label>
                                                @endif
                                            </label>

                                            <input type="file" id="certificates" class="form-control"
                                                   name="certificates"
                                                   style="display: none">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            {{-- Languages tab --}}
                            <div id="languages" class="step-content">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label> {{t_label('languages')}}  </label>

                                        <select class="form-control languages_input" name="languages_input">

                                            @foreach($Languages as $lang)
                                                <option value="{{$lang->id}}">{{$lang->lang_name_automated}}</option>
                                            @endforeach
                                        </select>

                                    </div>
                                    <div class="col-md-4">
                                        <label> {{t_label('level')}}  </label>

                                        <select class="form-control level_input" name="level_input">

                                            @foreach($languages_level as $level=>$value)
                                                <option value="{{$value}}">{{ t_label($level)}}</option>
                                            @endforeach
                                        </select>

                                    </div>
                                    <div class="col-md-4  pt-3">
                                        <a type="button" class="btn btn-primary add_languages">{{t_label('Add')}} <i
                                                class="fa fa-plus"></i>
                                        </a>
                                    </div>
                                </div>

                                <hr>
                                <div class="languages_list">
                                    @foreach($user_languages as $index=>$user_language )

                                        <div class="row lang_item" data-id="{{$user_language->language_id}}">
                                            <div class="col-md-4">
                                                <label> {{t_label('languages')}}  </label>
                                                <input type='hidden' name="languages[{{$index}}][language_id]"
                                                       value="{{$user_language->language_id}}">

                                                <select class="form-control lang_section" disabled>

                                                    @foreach($Languages as $lang)
                                                        <option
                                                            value="{{$lang->id}}" {{$user_language->language_id ==$lang->id?"selected":""}} >{{$lang->lang_name_automated}}</option>
                                                    @endforeach
                                                </select>

                                            </div>
                                            <div class="col-md-4">
                                                <label> {{t_label('level')}}  </label>

                                                <select class="form-control" name="languages[{{$index}}][level]">

                                                    @foreach($languages_level as $level=>$value)
                                                        <option
                                                            value="{{$value}}" {{$user_language->level ==$value?"selected":""}}> {{ t_label($level)}}</option>
                                                    @endforeach
                                                </select>

                                            </div>
                                            <div class="col-md-4 d-flex align-items-center pt-3">
                                                <button class="btn btn-outline-danger delete_lang" type="button"><i
                                                        class="fa fa-trash"></i></button>


                                            </div>
                                        </div>

                                    @endforeach

                                </div>
                            </div>

                            <div id="occupation" class="step-content">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label> {{t_label('category')}}  </label>

                                        <select class="form-control occupation" name="occupation[0][category_id]">

                                            @foreach($categories as $cat)
                                                <option
                                                    value="{{$cat->id}}" {{@$user_occupations->category_id == $cat->id?'selected':""}}>{{$cat['name_'.app()->getLocale()]}}</option>
                                            @endforeach
                                        </select>

                                    </div>
                                    <div class="col-md-4">
                                        <label> {{t_label('from')}}  </label>

                                        <input type="year" name="occupation[0][from]" class="form-control"
                                               placeholder="" value="{{@$user_occupations->from}}">


                                    </div>
                                    <div class="col-md-4">
                                        <label> {{t_label('to')}}  </label>

                                        <input type="year" name="occupation[0][to]" class="form-control"
                                               value="{{@$user_occupations->to}}" placeholder="">


                                    </div>


                                    <div class="col-md-12 sub_categories_list row mt-2">

                                        @foreach($sub_categories_list as $sub_i )

                                            <div class="col-md-3 form-group">
                                                <div class="form-check">
                                                    <input class="form-check-input"
                                                           {{in_array($sub_i->id,$user_sub_categories )?"checked":""}} type="checkbox"
                                                           name="sub_categories[]" id="gridCheck${{$sub_i->id}}"
                                                           value='{{$sub_i->id}}'>
                                                    <label class="form-check-label" for="gridCheck${{$sub_i->id}}">
                                                        {{$sub_i['name_' .app()->getLocale()]}}
                                                    </label>
                                                </div>
                                            </div>
                                        @endforeach

                                    </div>

                                </div>
                            </div>
{{--
                            <div id="social" class="step-content">
                                <div class="row my-2">
                                    <div class="col-md-6">
                                        <label> {{t_label('Like our account on Facebook')}}  </label>


                                    </div>
                                    <div class="col-md-4">

                                        <a class="btn btn-primary form-control like_fb_account">
                                            <i class="fa fa-facebook-f"></i>
                                        </a>


                                    </div>

                                </div>
                                <div class="row my-2">
                                    <div class="col-md-6">
                                        <label> {{t_label('Like our account on Instagram')}}  </label>


                                    </div>
                                    <div class="col-md-4">

                                        <a class="btn btn-primary form-control like_instagram_account">
                                            <i class="fa fa-instagram"></i>
                                        </a>


                                    </div>

                                </div>
                                <div class="row my-2">
                                    <div class="col-md-6">
                                        <label> {{t_label('Like our account on Linkedin')}}  </label>


                                    </div>
                                    <div class="col-md-4">

                                        <a class="btn btn-primary form-control like_linkedin_account">
                                            <i class="fa fa-linkedin"></i>
                                        </a>


                                    </div>

                                </div>
                                <div class="row my-2">
                                    <div class="col-md-6">
                                        <label> {{t_label('Like our account on Linkedin')}}  </label>


                                    </div>
                                    <div class="col-md-4">

                                        <a class="btn btn-primary form-control like_twitter_account">
                                            <i class="fa fa-twitter"></i>
                                        </a>


                                    </div>

                                </div>
                            </div>--}}

                        </div>

                        <div class="text-end my-3">
                            <button class="btn badge-success text-white" type="button" id="nextStep"
                                    data-next="educationInfo">
                                {{t_label('Next')}}
                            </button>
                        </div>
                    </div>
                </div>

            </div>

        </form>

    </div>


@endsection

@section('scripts')

    <script>
        const nextStepBtn = document.querySelector('#nextStep')
        const steps = document.querySelectorAll('.step')
        const stepContent = document.querySelectorAll('.step-content')
        const completionStatusBar = document.querySelector('form.edit-form .progress-bar')
        const completionAmount = document.querySelector('.progress-wrapper span:first-of-type')

        function handleActiveTab(id) {
            if (id === 'educationInfo') document.querySelector('#step1-tab').classList.add('done')
            if (id === 'languages') document.querySelector('#step2-tab').classList.add('done')
            if (id === 'occupation') document.querySelector('#step3-tab').classList.add('done')
            stepContent.forEach(step => {

                step.style.display = 'none'
                if (step.id === id) {
                    step.style.display = 'block'
                    const stepNum = document.querySelector(`[data-step='${id}']`)
                    stepNum.classList.add('in-progress')
                    stepNum.classList.remove('disabled')
                    stepNum.querySelector('span').classList.remove('bg-secondary')
                }
            })

        }

        const stepsList = {
            'personal': {
                id: 'step1-tab',
                nextStep: 'educationInfo',
                completion: 30
            },
            'educationInfo': {
                id: 'step2-tab',
                nextStep: 'languages',
                completion: 20
            },
            'languages': {
                id: 'step3-tab',
                nextStep: 'occupation',
                completion: 40
            },
            'occupation': {
                id: 'step4-tab',
                nextStep: 'social',

                completion: 70
            },
      /*      'social': {
                id: 'step5-tab',
                completion: 90
            },*/


        }

        $(document).on('click', '#nav-tab   button', function () {
            id = $(this).data('step');

            // handleActiveTab(id);

            let _next_s = stepsList[id];

            if (typeof _next_s != 'undefined') {

                nextStepBtn.textContent = _next_s.id === 'step4-tab' ? "{{t_label('Send')}}" : "{{t_label('Next')}}"
                {{--nextStepBtn.textContent = _next_s.id === 'step5-tab' ? "{{t_label('Send')}}" : "{{t_label('Next')}}"--}}
                nextStepBtn.type = _next_s.id === 'step4-tab' ? 'submit' : 'button'
                // nextStepBtn.type = _next_s.id === 'step5-tab' ? 'submit' : 'button'
                nextStepBtn.dataset.next = _next_s.nextStep;
                completionStatusBar.style.width = `${_next_s.completion}%`
                completionAmount.textContent = `${_next_s.completion}%`

                handleActiveTab(id)
                topFunction()
            }

        });

        function topFunction() {
            document.body.scrollTop = 0; // For Safari
            document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
        }


        steps.forEach(step => {
            step.addEventListener('click', function () {
                const selectedTab = this.dataset.step;
                handleActiveTab(selectedTab)

            })
        })


        // ['personal','educationInfo','occupation']

        nextStepBtn.addEventListener('click', function (e) {
            let nextTab = this.dataset.next;
            let _next_s = stepsList[nextTab];

            if (typeof _next_s != 'undefined') {
                e.preventDefault()

                nextStepBtn.textContent = _next_s.id === 'step5-tab' ? "{{t_label('Send')}}" : "{{t_label('Next')}}"
                nextStepBtn.type = _next_s.id === 'step5-tab' ? 'submit' : 'button'
                nextStepBtn.dataset.next = _next_s.nextStep;
                completionStatusBar.style.width = `${_next_s.completion}%`
                completionAmount.textContent = `${_next_s.completion}%`

                handleActiveTab(nextTab)
                topFunction()
            }
        })
    </script>
    <script>
        var inputElm = document.querySelector('.tagify'),
            tagify = new Tagify(inputElm);
    </script>


    <script>


        var lang_index = {!! count($languages_level) !!} + 1
        var languages = {!! $Languages !!}
        $(document).on('click', '.add_languages', function (event) {

            event.preventDefault();
            let languages_val = $('[name=languages_input]').val();
            let level_val = $('[name=level_input]').val();
            if (!languages_val || !level_val) return;


            console.log('languages_list', languages_val);
            $('.languages_list').append(`
                                                <div class="row lang_item" data-id="${languages_val}">
                                        <div class="col-md-4">
                                            <label> {{t_label('languages')}}  </label>

                                                  <input type='hidden' name='languages[${lang_index}][language_id]' value="${languages_val}">
                                            <select class="form-control lang_section" name="languages[${lang_index}][language_id]" disabled>

                                                @foreach($Languages as $lang)
            <option
                value="{{$lang->id}}" ` + ({!!  $lang->id!!} == languages_val ? "selected" : "") + `>{{$lang->lang_name_automated}}</option>
                                                @endforeach
            </select>

        </div>
        <div class="col-md-4">
            <label> {{t_label('level')}}  </label>

                                            <select class="form-control" name="languages[${lang_index}][level]">

                                                @foreach($languages_level as $level=>$value)
            <option value="{{$value}}"   ` + ({!!  $value!!} == level_val ? "selected" : "") + `> {{ t_label($level)}}</option>
                                                @endforeach
            </select>

        </div>
        <div class="col-md-4 d-flex align-items-center pt-3">
            <button class="btn btn-outline-danger delete_lang" type="button"><i class="fa fa-trash"></i></button>


        </div>
    </div>
`);

            refreshLangSelect();

            lang_index++;


        })
        $(document).on('click', '.delete_lang', function (event) {
            event.preventDefault();

            let parent = $(this).parents('.lang_item').fadeOut(function () {
                $(this).remove();

                refreshLangSelect();

            });
        });
        $(document).on('change', '.occupation', function (event) {
            // event.preventDefault();


            console.log('occupation');
            fillSubcategory();


        });


        function refreshLangSelect() {

            $select_langoages = [];

            $('.languages_list .lang_item').each(function (index, item) {
                // console.log(item   index)
                let val = $(item).data('id');
                console.log(val)

                if (val != "undefined")
                    $select_langoages.push(val);


            });
            $select_langoages
            console.log($select_langoages)

            $options = '';


            languages.forEach(function (item) {
                if (!$select_langoages.includes(item.id)) {
                    $options += `<option value='${item.id}'>${item.lang_name_automated}</option>`;
                }


                $('.languages_input').html($options);

            });
        }

        function fillSubcategory() {

            let subCategories = {!!$sub_categories  !!};
            let val = $('.occupation').val();


            $options = "";
            subCategories.forEach(function (item) {
                if (val == item.category_id) {
                    $options += `  <div class="col-md-3 form-group">
    <div class="form-check">
      <input class="form-check-input" type="checkbox" name="sub_categories[]" id="gridCheck${item.id}"  value='${item.id}'>
      <label class="form-check-label" for="gridCheck${item.id}">
       ${item['name_' + "{{app()->getLocale()}}"]}
      </label>
    </div>
  </div>`
                }


            });

            console.log('sub_categories_list', $options)

            $('.sub_categories_list').html($options);

        }

        refreshLangSelect()
    </script>
@endsection
