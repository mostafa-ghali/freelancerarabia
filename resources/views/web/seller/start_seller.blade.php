@extends('web.layouts.master')
@section('content')
    <main>
        <!-- <video src="https://sg.fiverrcdn.com/packages_lp/cover_video.mp4" width="100%" height="580px" autoplay>
    </video> -->

        <div class="panel mb-5">
            <p class="hed2"> {{t_label('Join as a freelance')}}r</p>
            <p class="titl">{{t_label("It's your skill that makes the difference. Let us make earning easy for you")}}</p>
        </div>


{{--
        <div class="col-md-6 d-flex flex-column  align-items-center ">
            <div class="card p-0 border-0"
                 style="width: 13rem; overflow:hidden ; box-shadow:0 4px 8px 0 rgb(0 0 0 / 8%)">
                <img class="card-img-top person-img"
                     src="https://bs-uploads.toptal.io/blackfish-uploads/components/image/content/file_file/file/157768/hero_section_small-9455e22140ef25a9da2e7eb99c1b1be6.jpg"
                     alt="Card image cap">
                <span class="person-span"></span>
                <div class="card-body">
                    <h5 class="card-title  text-blue">Ruth Madrigal</h5>
                    <p class="card-text">Scrum Master</p>
                </div>
            </div>


            <div class="card p-0 border-0"
                 style="width: 13rem; overflow:hidden;margin:-12px; margin-right:282px; box-shadow:0 4px 8px 0 rgb(0 0 0 / 8%)">
                <img class="card-img-top person-img"
                     src="https://bs-uploads.toptal.io/blackfish-uploads/components/image/content/file_file/file/157768/hero_section_small-9455e22140ef25a9da2e7eb99c1b1be6.jpg"
                     alt="Card image cap">
                <span class="person-span"></span>
                <div class="card-body">
                    <h5 class="card-title  text-blue">ahmad saddi</h5>
                    <p class="card-text">Ui Ux Designer</p>
                </div>
            </div>

        </div>--}}
        <div class="container">

            <div class="custom-card-header">
                @if(session()->has('alert-success'))
                    <div class="alert alert-success"
                         style="text-align: center">{{session()->get('alert-success')}}</div>
                @endif
            </div>


            <div class="row mt-5 pt-5">


                @foreach($categories as $category)

                    <div class="col-md-3 mb-3">

                        <div class="card category-box drop-shadow">

                            {{--                            <h5 class="card-title">{{{$category['name_'.app()->getLocale()]}}}</h5>--}}
                            {{--                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>--}}

                            <a
                                @if(auth()->check())
                                href="{{route('seller.seller_on_boarding',['category'=>$category])}}"
                                @else
                                data-bs-target="#LoginModal"
                                data-bs-toggle="modal"
                                @endif
                                class="btn bg-transparent h-100 p-0 rounded-0 border-0 shadow-none">


                                <img class="card-img-top lazy" data-src="{{$category->image}}" alt="Card image cap">
                                <h4 class="px-1 pt-2 h-auto">
                                    {{{$category['name_'.app()->getLocale()]}}}
                                </h4>

                                {{--                                        <div class="card-body p-0">--}}



                                {{--                                        </div>--}}
                            </a>


                        </div>
                    </div>
                @endforeach
            </div>

        </div>


    </main>

@endsection

@section('scripts')
    <script>

    </script>
@endsection
