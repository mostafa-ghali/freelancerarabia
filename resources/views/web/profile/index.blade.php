@extends('web.layouts.master')
@section('content')
    <main class="container edit_profile">
        <div class="custom-card-header">
            @if(session()->has('alert-success'))
                <div class="alert alert-success" style="text-align: center">{{session()->get('alert-success')}}</div>
            @endif
        </div>

        <form action="{{route('profile.update')}}" method="post" class="edit-form" enctype="multipart/form-data">
            @csrf
            <input name="id" type="hidden" value="{{auth('web')->user()->id}}">

            <div class="container">
                <nav class="d-flex steps-header justify-content-between align-items-center">
                    <div class="nav w-75 nav-pills nav-fill mb-0" id="nav-tab">
                        <button class="btn in-progress step nav-link shadow-none" id="step1-tab" data-step="personal" type="button">
                            <span class="badge badge-pill">1</span>
                            <span>{{t_label('Personal Info')}}</span>
                        </button>
                        <button class="btn step nav-link disabled  shadow-none" id="step2-tab" data-step="educationInfo" type="button">
                            <span class="badge badge-pill  bg-secondary">2</span>
                            <span>{{t_label('Education')}} </span>
                        </button>
                        <button class="btn step nav-link disabled shadow-none" id="step3-tab" data-step="occupation" type="button">
                            <span class="badge badge-pill bg-secondary">3</span>
                            <span>{{t_label('Occupation')}}</span>
                        </button>
                    </div>
                    <div class="progress-wrapper">
                        <span>{{t_label('Completion Rate')}}</span>
                        <div class="progress">
                            <div class="progress-bar" role="progressbar" aria-valuenow="33" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                    </div>
                </nav>
            </div>

            <div class="container">
                <div class="custom-card">
                    <div class="custom-card-body">
                        {{-- Personal tab --}}
                        <div id="personal" class="step-content active">
                            <div class="row justify-content-center align-items-center">
                                <div class="col-lg-12 col-12 mb-md-0 mb-3">
                                    <div class="row justify-content-center align-items-center">


                                    <br>
                                    <label for="personal_image" >
                                        <div class="row justify-content-center align-items-center">
                                                            <div class="col-2">
                                        @if(!is_null(auth('web')->user()->image))
                                            <img src="{{asset(auth('web')->user()->image)}}" width="150px" height="150px"
                                                 style="border-radius:50%;object-fit: cover;object-position: center">
                                        @else
                                            <img src="{{asset('web_ar/img/default-avatar.jpg')}}" width="150px" height="150px"
                                                 style="border-radius:50%;object-fit: cover;object-position: center ">
                                        @endif
                                        </div>
                                        </div>

                                    </label>
                                    <input type="file" id="personal_image" class="form-control" name="image"
                                           style="display: none">
                                </div>
                                </div>
                                <div class="col-lg-7 col-12">
                                    <div class="row">
                                        <div class="col mt-4">
                                        <div class="form-group mb-4">
                                        <label for="name">{{t_label('name')}}</label>
                                        <input class="form-control" name="user_name" id="name"
                                               value="{{auth('web')->user()->name}}">
                                    </div>

                                        </div>
                                        <div class="col mt-4">
                                        <div class="form-group mb-4">
                                        <label for="country">{{t_label('country')}}</label>
                                        <input class="form-control" type="text" name="country" id="country"
                                               value="{{auth('web')->user()->country}}">
                                    </div>
                                        </div>
                                    </div>



                                    <div class="form-group mb-3">
                                        <label for="description" class="mb-2"> {{t_label('about')}}  </label>
                                        <textarea class="form-control" rows="3" id="description"
                                                  name="description"> {{auth('web')->user()->description}} </textarea>
                                    </div>


                                </div>
                            </div>
                        </div>

                        {{-- Education tab --}}
                        <div id="educationInfo" class="step-content">
                            <div class="row justify-content-center">

                                <div class="col-lg-5 col-12">
                                    <div class="form-group mb-4">
                                        <label for="education">{{t_label('education')}}</label>
                                        <input class="form-control" name="education" id="education"
                                               value="{{auth('web')->user()->education}}">
                                    </div>

                                    <div class="form-group mb-4">
                                        <label for="university">{{t_label('university')}}</label>
                                        <input class="form-control" type="text" name="university" id="university"
                                               value="{{auth('web')->user()->university}}">
                                    </div>

                                    <div class="form-group mb-4">
                                        <label for="graduation_date">{{t_label('graduation_date')}}</label>
                                        <input type="date" class="form-control" name="graduation_date" id="graduation_date"
                                               value="{{auth('web')->user()->graduation_date}}">
                                    </div>

                                    <div class="form-group">
                                        <label for=""  class="mb-2">{{t_label('scientific_certificate')}}</label>
                                        <br>
                                        <label for="certificates">
                                            @if(!is_null(auth('web')->user()->certificate_image))
                                                <img src="{{asset(auth('web')->user()->certificate_image)}}" width="150px"
                                                     height="150px" style="border-radius:25px ">
                                            @else
                                                <label for="certificates"><img src="{{asset('web_ar/img/25010166.jpg')}}"
                                                                               width="150px" height="150px"
                                                                               style="border-radius:50% "> </label>
                                            @endif
                                        </label>

                                        <input type="file" id="certificates" class="form-control" name="certificates"
                                               style="display: none">
                                    </div>
                                </div>
                            </div>
                        </div>

                        {{-- Occupation tab --}}
                        <div id="occupation" class="step-content">
                            <div class="row">
                                <div class="col-lg-5">
                                   <div class="form-group mb-4">
                                       <label for="work_title">{{t_label('job')}}  </label>
                                       <input type="text" class="form-control" name="work_title" id="work_title"
                                              value="{{auth('web')->user()->work_title}}">

                                   </div>

                                    <div class="form-group">
                                        <label for="skills" class="mb-3">{{t_label('skills')}}</label>
                                        <input class="form-control tagify" name="skills" id="skills" value="{{$tags}}">
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="text-end my-3">
                        <button class="btn badge-success text-white" type="button" id="nextStep" data-next="educationInfo">
                            Next
                        </button>
                    </div>
                </div>
            </div>

        </form>
    </main>

@endsection

@section('scripts')
    <script>
        var inputElm = document.querySelector('.tagify'),
            tagify = new Tagify(inputElm);

        const nextStepBtn = document.querySelector('#nextStep')
        const steps = document.querySelectorAll('.step')
        const stepContent = document.querySelectorAll('.step-content')
        const completionStatusBar = document.querySelector('form.edit-form .progress-bar')
        const completionAmount = document.querySelector('.progress-wrapper span:first-of-type')

        function handleActiveTab(id){
            console.log(id)
            if(id === 'educationInfo') document.querySelector('#step1-tab').classList.add('done')
            if(id === 'occupation') document.querySelector('#step2-tab').classList.add('done')
            stepContent.forEach(step=> {

                step.style.display = 'none'
                if(step.id === id) {
                    step.style.display = 'block'
                    const stepNum =  document.querySelector(`[data-step='${id}']`)
                    console.log(stepNum)
                    stepNum.classList.add('in-progress')
                    stepNum.classList.remove('disabled')
                    stepNum.querySelector('span').classList.remove('bg-secondary')
                }
            })

        }

        function topFunction() {
            document.body.scrollTop = 0; // For Safari
            document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
        }

        const stepsList = {
            'personal': {
                id: 'step1-tab',
                nextStep: 'educationInfo',
                completion: 30
            },
            'educationInfo': {
                id: 'step2-tab',
                nextStep: 'occupation',
                completion: 70
            },
            'occupation': {
                id: 'step3-tab',
                completion: 70
            }

        }

        steps.forEach(step=>{
            step.addEventListener('click',function (){
                const selectedTab = this.dataset.step;
                handleActiveTab(selectedTab)

            })
        })


            // ['personal','educationInfo','occupation']

        nextStepBtn.addEventListener('click',function (e){
            e.preventDefault()
            let nextTab = this.dataset.next;
            this.textContent = stepsList[nextTab].id === 'step3-tab' ? 'Update' : 'Next'
            this.type = stepsList[nextTab].id === 'step3-tab' ? 'submit' : 'button'
            this.dataset.next = stepsList[nextTab].nextStep;
            completionStatusBar.style.width = `${stepsList[nextTab].completion}%`
            completionAmount.textContent = `${stepsList[nextTab].completion}%`

            handleActiveTab(nextTab)
            topFunction()
        })
    </script>
@endsection
