@extends('web.settings.setting-layout')

@section('active_section_notifications' , 'active')
@section('content2')
    <div class="custom-card h-100" style="min-height: 60vh;">
        @if(session()->has('alert-success'))
            <div class="alert alert-success" style="text-align: center">{{session()->get('alert-success')}}</div>
        @endif
        <div class="custom-card-body card-border h-100">
            <div class="row">
                <div class="col-12 col-md-4">
                    <div class="font-md">
                        {{t_label('Notifications')}}
                    </div>
                    {{--                                <div class="font-xs font-light">--}}
                    {{--                                    للحصول على الاشعارات مهمة بخصوص نشاطك الخاص بك. لا يمكن تعطيل بعض الإشعارات--}}
                    {{--                                </div>--}}
                </div>
                <form action="{{route('notification.stop_notifications')}}" method="post"  >
                    @csrf
                    <div class="col-12 col-md-8 mt-0 mt-md-4">
                        <input type="checkbox" @if(auth('web')->user()->messages_push_notifications == 1) checked @else @endif name="push_notifications" data-toggle="toggle" data-on="{{$labels['enable']}}" on="1" off="0"  data-off="{{$labels['not_enabled']}}" data-size="sm">
                    </div>
                    <div class="col-12 text-end mt-5">
                        <button class="btn btn-primary" type="submit">

                            {{t_label('Save')}}
                        </button>
                    </div>
                </form>

            </div>
        </div>
    </div>

@endsection


