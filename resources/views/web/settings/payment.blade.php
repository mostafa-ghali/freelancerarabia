@extends('web.settings.setting-layout')

@section('active_section_payment' , 'active')
@section('content2')
    <div class="custom-card h-100" style="min-height: 60vh">
        @if(session()->has('alert-success'))
            <div class="alert alert-success"
                 style="text-align: center">{{session()->get('alert-success')}}</div>
        @endif
        @if(session()->has('alert-danger'))
            <div class="alert alert-danger"
                 style="text-align: center">{{session()->get('alert-danger')}}</div>
        @endif
        <div class="custom-card-body card-border h-100">
            <div class=" row">
                <form action="{{route('store.settings.withdraw_payment')}}" method="post">
                    @csrf
                    {{--                                    <input type="hidden" name="witdrow_mony" value="{{$total}}">--}}

                    <div class="row d-flex flex-center align-items-center">
                        <div class="col-md-6 col-md-6 font-md ">

                            <div class="card  mb-lg-0">
                                <div class="card-body">
                                    <h5 class="card-title text-muted text-uppercase text-center">     {{t_label('current_balance')}}</h5>
                                    <h6 class="card-price text-center">${{$total}}</h6>
                                    <hr>
                                </div>
                            </div>

                        </div>
                        <div class="col-md-6 col-md-6 font-md ">

                            <div class="card  mb-lg-0">
                                <div class="card-body">
                                    <h5 class="card-title text-muted text-uppercase text-center">{{t_label('available_balance')}}</h5>
                                    <h6 class="card-price text-center">
                                        ${{$allowed_amount_to_withdraw}}</h6>
                                    <hr>
                                </div>
                            </div>

                        </div>

                    </div>

                    <div class="row  mt-5">


                        <div class="col-6 col-md-6 text-center ">
                            <input type="number" min="1" @if($allowed_amount_to_withdraw == 0 ) title="{{t_label("Sorry! There is not enough balance withdraw")}}"  disabled @endif class="form-control" max="{{$allowed_amount_to_withdraw}}" name="withdrew_money" value="{{$allowed_amount_to_withdraw}}">

                        </div>
                        <div class="col-6 col-md-6 text-start">

                            <button @if($allowed_amount_to_withdraw == 0 ) title="{{t_label("Sorry! There is not enough balance withdraw")}}"   disabled type="button"  @else type="submit"  @endif
                            class="btn btn-primary">{{t_label('Withdraw amount')}}</button>
                        </div>


                    </div>
                    {{--                                    @if($allowed_amount_to_withdraw == 0 )--}}
                    {{--                                    <div class="alert-info alert mt-4">--}}
                    {{--                                        {{t_label("Sorry! There is not enough balance withdraw")}}--}}
                    {{--                                    </div>--}}
                    {{--                                    @endif--}}
                </form>
            </div>
        </div>
    </div>

@endsection
