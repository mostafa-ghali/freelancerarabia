@extends('web.layouts.master')
@section('style')
    <link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css"
          rel="stylesheet">
@endsection
@section('content')

    <main class="container-fluid mt-5">
        <div class="site-padding">
            <div class="row mb-5">
                <div class="section col-12 ">
                    <h4 class="font-xl section-title mb-3  mt-2">
                        {{$labels['settings']}}
                    </h4>
                </div>
                <div class="col-12 col-md-3 col-lg-2 mb-md-0 mb-3">
                    <div class="custom-card h-100" style="min-height: 60vh">
                        <div class="custom-card-body settings-aside-menu card-border p-0 h-100">
                            <ul class="side-nav">
                                <li class="side-nav-item">
                                    <a class="side-nav-link @yield('active_section_setting')" href="{{route('store.settings.show')}}">
                                        {{$labels['account']}}
                                    </a>
                                </li>
                                <li class="side-nav-item">
                                    <a class="side-nav-link @yield('active_section_notifications')" href="{{route('store.settings.settings_notifications')}}">
                                        {{$labels['notifications']}}
                                    </a>
                                </li>
                                <li class="side-nav-item ">
                                    <a class="side-nav-link  @yield('active_section_payment')" href="{{route('store.settings.payment')}}">
                                        {{t_label('Payment')}}
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>

                </div>

                <div class="col-12 col-md-9 col-lg-10 ">

                            @yield('content2')

                </div>
            </div>
        </div>
    </main>
    <!-- end main -->
@endsection
@section('scripts')
    <script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>

@endsection
