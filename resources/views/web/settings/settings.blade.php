@extends('web.settings.setting-layout')

@section('active_section_setting' , 'active')
@section('content2')

    <div class="custom-card">
        @if(session()->has('alert-success'))
            <div class="alert alert-success"
                 style="text-align: center">{{session()->get('alert-success')}}</div>
        @endif
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif


        <div class="custom-card-body card-border">
            <form action="{{route('store.settings.general_settings.update')}}" method="POST">

                @csrf

                <input type="hidden" name="is_active" value="0">
                <div class="col-12 col-md-4 font-md">{{t_label('cancel_account')}}</div>
                <div class="col-12 col-md-8">
                    <ul class="bullet-list">
                        <li>{{t_label('deactivate_account')}}</li>
                    </ul>
                </div>
                <div class="col-12 col-md-4 font-md">{{t_label('delete_account_reasone')}}</div>

                <div class="col-12 col-md-8">
                    <select class="form-control" name="deactivation_reason">
                        <option selected disabled>{{t_label('select_reason')}}</option>
                        <option
                            value="{{t_label('i_have_another_account')}}">{{t_label('i_have_another_account')}}</option>
                        <option
                            value="{{t_label('i_want_change_user_name')}}">{{t_label('i_want_change_user_name')}}</option>
                        <option
                            value="{{t_label('i_not_getting_good_offers')}}">{{t_label('i_not_getting_good_offers')}}</option>
                        <option value="{{t_label('site_difficult_use')}}">{{t_label('site_difficult_use')}}</option>
                        <option
                            value="{{t_label('site_policies_inconvenient')}}">{{t_label('site_policies_inconvenient')}}</option>
                    </select>
                </div>
                <div class="col-12 col-md-4 mt-4 font-md">
                    <div>
                        {{t_label('your_feedback')}}
                    </div>
                </div>
                <div class="col-12 col-md-8 mt-0 mt-md-4">
                    <textarea class="form-control" name="deactivation_note" rows="3"></textarea>
                </div>
                <div class="col-12 text-end mt-5">
                    <button type="submit" class="btn btn-primary">
                        {{t_label('disable_account')}}
                    </button>
                </div>

            </form>
        </div>
    </div>
 @endsection


