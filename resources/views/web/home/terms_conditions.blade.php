

@extends('web.layouts.master')
@section('content')
    <main dir="ltr">
        <div class="container my-5">
            <div class="row">
                <div class="col-12">
                    <div class="Terms mt-5 mb-5 bg-white border border-1" style="border-radius: 5px;padding: 30px 40px;">
                        <div class="header mb-4">
                            <h2 class="text-dark mb-0"><b>FreelancerArabic Term & Conditions</b> </h2>
                            <span class=""><b>Last Update: March 2022</b></span>
                        </div>
                        <div class="descriptions">
                            <div class="welcome">
                                <p class="mb-2">
                                    <b>Welcome to Freelance-Arabic.com</b>
                                </p>
                            </div>
                            <div class="describe">
                                <div class="describe1 mb-4">
                                    Freelance Arabic maintains the Freelance-Arabia.com Website ("FreelanceArabia"). The following
                                    are the terms of use that govern use of the Site ("<b>Terms of Use</b>"). By using
                                    the Site you expressly agree to be bound by these Terms of Use and the Freelance-Arabia.com
                                    privacy policy and to follow these Terms of Use and all applicable laws and regulations
                                    governing use of the Site. Freelance Arabia reserves the right to change these Terms of Use at any time,
                                    effective immediately upon posting on the Site. Please check this page of the Site periodically.
                                    We will note when there are updates to the Terms of Use at the bottom of the Terms of Use.
                                    If you violate these Terms of Use, COMPANY may terminate your use of the Site, bar you from
                                    future use of the Site, and/or take appropriate legal action against you.
                                </div>

                                <div class="describe2 mb-4">
                                    <h4 class="text-dark"><b>LIMITED LICENSE.</b></h4>
                                    You are granted a limited, non-exclusive, revocable and non-transferable
                                    license to utilize and access the Site pursuant to the requirements and restrictions of
                                    these Terms of Use. COMPANY may change, suspend, or discontinue any aspect of the Site at
                                    any time. COMPANY may also, without notice or liability, impose limits on certain features
                                    and services or restrict your access to all or portions of the Site. You shall have no rights
                                    to the proprietary software and related documentation, if any, provided to you in order to
                                    access the Site. Except as provided in the Terms of Use, you shall have no right to directly
                                    or indirectly, own, use, loan, sell, rent, lease, license, sublicense, assign, copy, translate,
                                    modify, adapt, improve, or create any new or derivative works from, or display, distribute,
                                    perform, or in any way exploit the Site, or any of its contents (including software) in whole
                                    or in part.
                                </div>

                                <div class="describe3 ps-4 mb-4">
                                    <div class="Item1 mb-4">
                                        <h5 class="text-dark"> <b>1- SITE OPERATION:</b></h5>
                                        <ul>
                                            <li>
                                                United Arab Emirates is our Country of Domicile. COMPANY controls this Site from the U.A.E.
                                                COMPANY makes no representation that this Site is appropriate for use in other locations.
                                                If you use this Site from other locations you are responsible for ensuring compliance with
                                                local laws. You may not use, export or re-export any materials from this Site in violation
                                                of any applicable laws or regulations, including, but not limited to any U.A.E export laws and regulations.
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="Item2 mb-4">
                                        <h5 class="text-dark"> <b>2- APPLICABLE LAW:</b></h5>
                                        <ul>
                                            <li>
                                                The Laws of the United Arab Emirates shall govern the use of the Site and the
                                                Terms of Use, without regards to conflict of laws principals.
                                                All disputes arising in connection therewith shall be heard only by a
                                                court of competent jurisdiction in U.A.E.
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="Item3 mb-4">
                                        <h5 class="text-dark"> <b>3- MULTI-CURRENCY PRICED TRANSACTION:</b></h5>
                                        <ul>
                                            <li>
                                                the displayed price and currency selected by you, will be the same price and currency charged to
                                                the Card and printed on the Transaction Receipt.
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="Item4 mb-4">
                                        <h5 class="text-dark"> <b>4- PURCHASES:</b></h5>
                                        <ul>
                                            <li>
                                                COMPANY accepts payment by Visa or Mastercard debit and credit cards in AED for
                                                its products and services. All online purchases are also governed by the terms
                                                and conditions of respective merchant service providers. Please review respective
                                                merchant service provider’s user agreement and privacy policy before entering any
                                                transaction. Interpretation. These Terms of Use supersede all other written and oral
                                                communications or agreements with regard to the subject matters discussed in these Terms of Use.
                                                A waiver or modification of these Terms of Use will only be effective if made in writing signed
                                                by an authorized officer of COMPANY. All refunds will be made onto the original mode of payment.
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="Item5 mb-4">
                                        <h5 class="text-dark"> <b>5- OFFICE FOREIGN ASSETS CONTROL (OFAC) SANCTIONED COUNTRIES:</b></h5>
                                        <ul>
                                            <li>
                                                COMPANY will not trade with or provide any services to individuals and companies owned or controlled by,
                                                or acting for or on behalf of, OFAC targeted countries AND individuals,
                                                groups, and entities, such as terrorists and narcotics traffickers designated under the OFAC programs that are not country specific
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="Item6 mb-4">
                                        <h5 class="text-dark"> <b>6- REPRESENTATIONS BY YOU:</b></h5>
                                        <ul>
                                            <li>
                                                By visiting the Site, you represent, warrant and covenant that (a) you are at least 18 years old; (b) that all materials of
                                                any kind submitted by you to COMPANY through the Site or for inclusion on the Site will not plagiarize, violate or infringe
                                                upon the rights of any third-party including trade secret, copyright, trademark, trade dress, privacy, patent, or other personal or
                                                proprietary rights. The customer using the website
                                                who are Minor /under the age of 18 shall not register as a User of the website and shall not transact on or use the website.
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="Item7 mb-4">
                                        <h5 class="text-dark"> <b>7- PERMITTED USE:</b></h5>
                                        <ul>
                                            <li>
                                                You agree that you are only authorized to visit, view and to retain a copy of pages of
                                                this Site for your own personal use, that you shall not duplicate, download, publish,
                                                modify or otherwise distribute the material on this Site for any purpose other than for
                                                personal use, unless otherwise specifically authorized by COMPANY to do so. You also agree
                                                not to deep-link to the site for any purpose, unless specifically authorized by COMPANY to do so.
                                                The content and software on this Site is the property of COMPANY. The cardholder must retain a copy
                                                of transaction records and Merchant policies and rules.
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="Item8 mb-4">
                                        <h5 class="text-dark"> <b>8- YOUR ACCOUNT:</b></h5>
                                        <ul>
                                            <li>
                                                If you use COMPANY Site, you are responsible for maintaining the confidentiality of your
                                                account and password and for restricting access to your account from any devices, and you
                                                agree to accept responsibility for all activities that occur under your account or password.
                                                The Site shall not be responsible or liable, directly or indirectly, in any way for any loss
                                                or damage of any kind incurred as a result of, or in connection with, your failure to comply
                                                with this section.
                                            </li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="describe4 mb-4">
                                    <h4 class="text-dark"><b>NO COMMERCIAL USE.</b></h4>
                                    This Site may not be used by you for any commercial purposes such as to conduct sales
                                    of merchandise or services of any kind. You must obtain COMPANY’s prior written consent
                                    to make commercial offers of any kind on the Site, whether by advertising, solicitations,
                                    links, or any other form of communication. COMPANY will investigate and take appropriate
                                    legal action against anyone who violates this provision, including without limitation, removing
                                    the offending communication from the Site and barring such violators from use of the Site.
                                </div>
                                <div class="describe5 mb-4">
                                    <h4 class="text-dark"><b>LINKS AND SEARCH RESULTS.</b></h4>
                                    The Site may automatically produce search results that reference and/or link to third party sites
                                    throughout the World Wide Web. COMPANY has no control over these sites or the content within them.
                                    COMPANY does not guarantee, represent or warrant that the content contained in the sites is accurate,
                                    legal and/or inoffensive. COMPANY does not endorse the content of any third party site, nor does it make any
                                    representation or warranty about these sites, including that they will not contain viruses or otherwise impact
                                    your computer. By using the Site to search for or link to another site, you agree and understand that you may not
                                    make any claim against COMPANY for any damages or losses, whatsoever, resulting from your use of the Site to obtain
                                    search results or to link to another site. If you have a problem with a link from the Site, you may notify us at
                                    <a href="mailto:info@https://freelancearabia.ae">info@https://freelancearabia.ae</a>
                                </div>
                                <div class="describe6 mb-4">
                                    <h4 class="text-dark"><b>COPYRIGHT POLICY.</b></h4>
                                    COMPANY may terminate the privileges of any user who uses this Site to unlawfully transmit copyrighted
                                    material without a license, express consent, valid defense or fair use exemption to do so. If you submit
                                    information to this Site, you warrant that the information does not infringe the copyrights or other rights
                                    of third parties.
                                </div>
                                <div class="describe7 mb-4">
                                    <h4 class="text-dark"><b>INTELLECTUAL PROPERTY.</b></h4>
                                    Although COMPANY is not responsible for the content, quality or accuracy of data provided by users,
                                    compilations of such data, text, information and other materials made available to users through COMPANY’s system.
                                    The On-line Materials are COMPANY’s intellectual property, and are protected by U.S. and international intellectual
                                    property laws. The On-line Materials may not be copied or redistributed either in whole or in part without prior written
                                    consent of COMPANY, except as expressly and specifically permitted under these Terms of Use.
                                </div>

                                <div class="describe8 mb-4">
                                    The On-line Materials are and will remain the exclusive property of COMPANY. All rights, titles and interests
                                    in and to the On-line Materials will be and remain vested solely in COMPANY. Under no circumstances will you
                                    have any right, directly or indirectly, to own, use, copy, loan, sell, rent, lease, license, sublicense, redistribute,
                                    assign or otherwise convey the On- line Materials, or any rights thereto, except as expressly and specifically provided
                                    in the Terms of Use. Nothing in these Terms of Use will convey to you any right, title or interest, except that of a license
                                    with the express rights and subject to all limitations herein. Nothing in these Terms of Use grants you the right, directly or
                                    indirectly, to use the On-line Materials to create a product for resale or to use the On-line Materials in any way that competes
                                    with COMPANY.
                                </div>
                                <div class="describe9 mb-4">
                                    You acknowledge and agree that COMPANY will own all rights, titles and interests in and to any copy, translation,
                                    modification, adaptation, derivative work or improvement of the On- line Materials made by or for you. At COMPANY’s
                                    request, you must execute, or obtain the execution of, any instrument that may be necessary to assign these rights,
                                    titles or interests to COMPANY or perfect these rights, titles or interests in COMPANY’s name. DISCLAIMER OF WARRANTY,
                                    LIMITATION OF DAMAGES. COMPANY MAKES NO WARRANTY OR REPRESENTATION OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT
                                    NOT LIMITED TO WARRANTY OF TITLE OR NON-INFRINGEMENT OR IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE,
                                    NON-INFRINGEMENT OR OTHER VIOLATION OF RIGHTS IN RELATION TO THE AVAILABILITY, ACCURACY, VALIDITY, RELIABILITY OR CONTENT OF
                                    THESE PAGES AND/OR THE SITE. COMPANY ALSO DOES NOT MAKE ANY REPRESENTATION OR WARRANTY REGARDING THE ACCURACY OR RELIABILITY
                                    OF ANY ADVICE, OPINION, STATEMENT OR OTHER INFORMATION THAT IS SUBMITTED, DISPLAYED OR UPLOADED THROUGH THE SITE BY ANY USER.
                                    COMPANY SHALL NOT BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL OR CONSEQUENTIAL DAMAGES, LOST PROFITS OR FOR BUSINESS
                                    INTERRUPTION ARISING OUT OF THE USE OF OR INABILITY TO USE THIS SITE, EVEN IF COMPANY HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH
                                    DAMAGES. SOME JURISDICTIONS DO NOT ALLOW EXCLUSION OF CERTAIN WARRANTIES OR LIMITATIONS OF LIABILITY, SO THE ABOVE LIMITATIONS OR
                                    EXCLUSIONS MAY NOT APPLY TO YOU. THE LIABILITY OF COMPANY WOULD IN SUCH CASE BE LIMITED TO THE GREATEST EXTENT OF LIABILITY PERMITTED
                                    BY LAW.
                                </div>

                                <div class="describe10 mb-4">
                                    <h4 class="text-dark"><b>VIOLATION OF TERMS OF USE.</b></h4>
                                    You understand and agree that in COMPANY’s sole discretion, and without prior notice, COMPANY may terminate your
                                    access to the Site, or exercise any other remedy available and remove any unauthorized user information, if COMPANY
                                    believes that the information you provide has violated or is inconsistent with these Terms of Use, or violates the
                                    rights of COMPANY, or any third party, or violates the law. You agree that monetary damages may not provide a sufficient
                                    remedy to COMPANY for violations of these Terms of Use and you consent to injunctive or other equitable relief for such
                                    violations. COMPANY may release user information about you if required by law or subpoena.
                                </div>

                                <div class="describe11 mb-4">
                                    <h4 class="text-dark"><b>INDEMNITY.</b></h4>
                                    You agree to indemnify and hold COMPANY, its subsidiaries, affiliates, officers, agents and other partners and
                                    employees, harmless from any loss, liability, claim or demand, including reasonable attorneys’ fees, made by
                                    any third party due to or arising out of or relating to your use of the Site, including also your use of the
                                    Site to provide a link to another site or to upload content or other information to the Site, or your breach
                                    of the Terms of Use.
                                </div>

                                <div class="describe12 mb-4">
                                    <h4 class="text-dark"><b>LICENCE GRANTED TO YOU.</b></h4>
                                    By providing materials to COMPANY, including by submitting or uploading content or materials for use on the Site you
                                    represent and warrant that you or the owner of all rights to such content or materials has expressly granted COMPANY
                                    an irrevocable world-wide right in all languages and in perpetuity to use and exploit all or any part of the content
                                    and materials provided by you. COMPANY may publish and distribute any such submitted content or materials at its sole
                                    discretion by any method now existing or later developed. You agree that you shall waive all claims and have no recourse
                                    against COMPANY for any alleged or actual infringement or misappropriation of any proprietary rights in any communication,
                                    content or material submitted to COMPANY. Any communication or materials you send to COMPANY will be treated as non- confidential
                                    and non-proprietary and may be disseminated or used by COMPANY for any purpose, including, but not limited to, developing, creating,
                                    manufacturing or marketing products or services.
                                </div>

                                <div class="describe13 mb-4">
                                    <h4 class="text-dark"><b>ADVERTISING.</b></h4>
                                    The Site may contain advertisements and/or sponsorships. The advertisers and/or sponsors that provide these
                                    advertisements and sponsorships are solely responsible for insuring that the materials submitted for inclusion
                                    on the Site are accurate and comply with all applicable laws. COMPANY is not responsible for the acts or omissions
                                    of any advertiser or sponsor.
                                </div>

                                <div class="describe14 mb-4">
                                    <h4 class="text-dark"><b>ADVERTISING.</b></h4>
                                    The Site may contain advertisements and/or sponsorships. The advertisers and/or sponsors that provide these
                                    advertisements and sponsorships are solely responsible for insuring that the materials submitted for inclusion
                                    on the Site are accurate and comply with all applicable laws. COMPANY is not responsible for the acts or omissions
                                    of any advertiser or sponsor.
                                </div>
                                <div class="describe14 mb-4">
                                    <h4 class="text-dark"><b>Payment.</b></h4>

                                    “Free Lance Arabia Portal” maintains the https://freelancearabia.ae/. Website ("Site").
                                    <br>

                                    “United Arab of Emirates is our country of domicile” and stipulate that the governing law is the local law. All disputes arising in connection therewith shall be heard only by a court of competent jurisdiction in U.A.E.
                                    <br>

                                    ‘’Visa or MasterCard debit and credit cards in AED will be accepted for payment’’
                                    <br>
                                    ‘’We will not trade with or provide any services to OFAC (Office of Foreign Assets Control) and sanctioned countries in accordance with the law of UAE’’
                                    <br>
                                    ‘’Customer using the website who are Minor /under the age of 18 shall not register as a User of the website and shall not transact on or use the website’’
                                    <br>
                                    ‘’Cardholder must retain a copy of transaction records and Merchant policies and rules’’
                                    <br>
                                    ‘’User is responsible for maintaining the confidentiality of his account’’

                                </div>
                                <div class="describe15 mb-4">
                                    <h4 class="text-dark"><b>SEVERABILITY.</b></h4>
                                    If any provision of the Terms of Use is held to be invalid, void or unenforceable, the remaining provisions shall
                                    nevertheless continue in full force and effect.
                                </div>
                                <div class="describe16">
                                    <b>
                                        Headings & section titles in this Agreement are for convenience and do not define, limit, or extend any provision of this Agreement.
                                    </b>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
