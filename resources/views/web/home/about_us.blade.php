

@extends('web.layouts.master')
@section('content')
    <main dir="ltr">
        <div class="container mt-4">
            <div class="row">
                <div class="col-12">
                    <div class="Terms mt-5 mb-5 bg-white border border-1" style="border-radius: 5px;padding: 30px 40px;">
                        <div class="header mb-4">
                            <h2 class="text-dark mb-0"><b>
                                    {{t_label('FreelancerArabic About Us')}}

                                    </b> </h2>
                        </div>
                        <div class="descriptions">
                            <div class="describe">

                                <div class="describe1 mb-4">
                                    {{t_label('Freelance Arabia will revolutionize the way you work.
                                    We want to make sure that every individual gets the chance to work with leading organizations,
                                    SMEs, and start-ups. We are not only committed to providing the best talent,
                                    but also to providing the best working environment.
                                    You can be rest assured knowing you are in safe hands with us! .')}}

                                </div>

                                <div class="describe2 mb-4">

                                    {{t_label('Freelance Arabia is a platform in the Middle East that provides professional freelancers with a high-value platform to showcase their talents.
                                    Our vision is to create a freelance opportunity for all, not just the few.
                                    Freelance Arabia is the first Arab freelancing platform that seeks to be a global freelancing platform,
                                    connecting employers and freelancers from all over the world. .')}}

                                </div>

                                <div class="describe2 mb-4">
                                    {{t_label('Weve created an environment where employers and freelancers can exchange their expertise.
                                    When you work with Freelance Arabia, you can be sure of quality work delivered at competitive prices.')}}
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </main>
    <!-- end main -->
@endsection
    <!-- begin footer -->




