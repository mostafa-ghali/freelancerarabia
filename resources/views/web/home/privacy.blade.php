
@extends('web.layouts.master')
@section('content')
    <main dir="ltr">
        <div class="container my-5">
            <div class="row">
                <div class="col-12">
                    <div class="Terms mt-5 mb-5 bg-white border border-1" style="border-radius: 5px;padding: 30px 40px;">
                        <div class="header mb-4">
                            <h2 class="text-dark mb-0"><b>FreelancerArabic Privacy Policy</b> </h2>
                        </div>
                        <div class="descriptions">
                            <div class="describe">
                                <div class="describe1 mb-4">
                                    INFORMATION GATHERED BY <a href=" https://freelancearabia.ae" target="_blank"> https://freelance-arabia.com </a>. This is <a href=" https://freelancearabia.ae" target="_blank"> https://freelancearabia.ae </a>’s
                                    (“<a href=" https://freelancearabia.ae" target="_blank"> https://freelance-arabia.com </a>”) online privacy policy (“Policy”). This policy applies only
                                    to activities <a href=" https://freelancearabia.ae" target="_blank"> https://freelance-arabia.com </a> engages in on its website and does not apply to <a href=" https://freelancearabia.ae" target="_blank"> https://freelancearabia.ae </a>
                                    activities that are "offline" or unrelated to the website.
                                </div>

                                <div class="describe2 mb-4">
                                    <a href=" https://freelancearabia.ae" target="_blank"> https://freelancearabia.ae </a> collects certain anonymous data regarding the usage of the website. This information
                                    does not personally identify users, by itself or in combination with other information, and is gathered to
                                    improve the performance of the website. The anonymous data collected by the <a href=" https://freelancearabia.ae" target="_blank"> https://freelancearabia.ae </a> website can
                                    include information such as the type of browser you are using, and the length of the visit to the website.
                                    You may also be asked to provide personally identifiable information on the <a href=" https://freelancearabia.ae" target="_blank"> https://freelancearabia.ae </a> website, which
                                    may include your name, address, telephone number and e-mail address. This information can be gathered when
                                    feedback or e-mails are sent to <a href=" https://freelancearabia.ae" target="_blank"> https://freelancearabia.ae </a>, when you register for services, or make purchases via the website.
                                    In all such cases you have the option of providing us with personally identifiable information.
                                </div>

                                <div class="describe3 ps-4 mb-4">
                                    <div class="Item1 mb-4">
                                        <h5 class="text-dark"> <b>1- USE AND DISCLOSURE OF INFORMATION:</b></h5>
                                        <ul>
                                            <li>
                                                Except as otherwise stated below, we do not sell, trade or
                                                rent your personally identifiable information collected on the site to others. The information
                                                collected by our site is used to process orders, to keep you informed about your order status, to notify
                                                you of products or special offers that may be of interest to you, and for statistical purposes for improving
                                                our site. We will disclose your Delivery information to third parties for order tracking purposes or process
                                                your check or money order, as appropriate, fill your order, improve the functionality of our site, perform statistical
                                                and data analyses deliver your order and deliver promotional emails to you from us. For example, we must release your
                                                mailing address information to the delivery service to deliver products that you ordered.
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="Item2 mb-4">
                                        <h5 class="text-dark"> <b>2- credit/debit cards’ details and personally:</b></h5>
                                        <ul>
                                            <li>
                                                All credit/debit cards’ details and personally identifiable information will NOT be stored, sold,
                                                shared, rented or leased to any third parties
                                            </li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="describe4 mb-4">
                                    <h4 class="text-dark"><b>COOKIES.</b></h4>
                                    Cookies are small bits of data cached in a user’s browser. <a href=" https://freelancearabia.ae" target="_blank"> https://freelancearabia.ae </a> utilizes cookies to determine
                                    whether or not you have visited the home page in the past. However, no other user information is gathered.
                                </div>
                                <div class="describe5 mb-4">
                                    <a href=" https://freelancearabia.ae" target="_blank"> https://freelancearabia.ae </a> may use non-personal "aggregated data" to enhance the operation of our website, or analyze
                                    interest in the areas of our website. Additionally, if you provide <a href=" https://freelancearabia.ae" target="_blank"> https://freelancearabia.ae </a> with content for publishing
                                    or feedback, we may publish your user name or other identifying data with your permission.
                                </div>
                                <div class="describe6 mb-4">
                                    <a href=" https://freelancearabia.ae" target="_blank"> https://freelancearabia.ae </a> may also disclose personally identifiable information in order to respond to a subpoena, court order or
                                    other such request. <a href=" https://freelancearabia.ae" target="_blank"> https://freelancearabia.ae </a> may also provide such personally identifiable information in response to a law enforcement
                                    agencies request or as otherwise required by law. Your personally identifiable information may be provided to a party if
                                    <a href=" https://freelancearabia.ae" target="_blank"> https://freelancearabia.ae </a>  files for bankruptcy, or there is a transfer of the assets or ownership of <a href=" https://freelancearabia.ae" target="_blank"> https://freelancearabia.ae </a> in connection
                                    with proposed or consummated corporate reorganizations, such as mergers or acquisitions.
                                </div>

                                <div class="describe7 ps-4 mb-4">
                                    <div class="Item1 mb-4">
                                        <h5 class="text-dark"> <b>3- SECURITY:</b></h5>
                                        <ul>
                                            <li>
                                                <a href=" https://freelancearabia.ae" target="_blank"> https://freelancearabia.ae </a> takes appropriate steps to ensure data privacy and security including through various hardware and
                                                software methodologies. However, <a href=" https://freelancearabia.ae" target="_blank"> https://freelancearabia.ae </a> cannot guarantee the security of any information that is disclosed online.
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="Item2 mb-4">
                                        <h5 class="text-dark"> <b>4- OTHER WEBSITES:</b></h5>
                                        <ul>
                                            <li>
                                                <a href=" https://freelancearabia.ae" target="_blank"> https://freelancearabia.ae </a> is not responsible for the privacy policies of websites to which it links. If you provide any information to
                                                such third parties different rules regarding the collection and use of your personal information may apply. We strongly suggest
                                                you review such third party’s privacy policies before providing any data to them. We are not responsible for the policies or practices
                                                of third parties. Please be aware that our sites may contain links to other sites on the Internet that are owned and operated by third parties. The information practices of those <a href=" https://freelancearabia.ae" target="_blank"> https://freelancearabia.ae </a> linked to our site is not covered by this Policy. These other sites may send their own cookies or clear GIFs to users, collect data or solicit personally identifiable information. We cannot control this collection of information. You should contact these entities directly if you have any questions about their use of the information that they collect.
                                            </li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="describe8 mb-4">
                                    <h4 class="text-dark"><b>MINORS.</b></h4>
                                    <a href=" https://freelancearabia.ae" target="_blank"> https://freelancearabia.ae </a> does not knowingly collect personal information from minors under the age of 18. Minors are not permitted to use the
                                    <a href=" https://freelancearabia.ae" target="_blank"> https://freelancearabia.ae </a> website or services, and <a href=" https://freelancearabia.ae" target="_blank"> https://freelancearabia.ae </a> requests that minors under the age of 18 not submit any personal information
                                    to the website. Since information regarding minors under the age of 18 is not collected, <a href=" https://freelancearabia.ae" target="_blank"> https://freelancearabia.ae </a> does not knowingly distribute personal
                                    information regarding minors under the age of 18.
                                </div>
                                <div class="describe9 mb-4">
                                    <h4 class="text-dark"><b>CORRECTIONS AND UPDATES.</b></h4>
                                    If you wish to modify or update any information <a href=" https://freelancearabia.ae" target="_blank"> https://freelancearabia.ae </a> has received, please contact
                                    <a href="mailto:info@https://freelancearabia.ae"> info@https://freelancearabia.ae</a>.
                                </div>

                                <div class="describe10 ps-4 mb-4">
                                    <div class="Item1 mb-4">
                                        <h5 class="text-dark"> <b>5- MODIFICATIONS OF THE PRIVACY POLICY:</b></h5>
                                        <ul>
                                            <li>
                                                <a href=" https://freelancearabia.ae" target="_blank"> https://freelancearabia.ae </a> . The Website Policies and Terms & Conditions would be changed or updated occasionally to meet the
                                                requirements and standards. Therefore, the Customers are encouraged to frequently visit these sections in order to
                                                be updated about the changes on the website. Modifications will be effective on the day they are posted.
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
