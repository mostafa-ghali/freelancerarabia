@php


    $original_categories = \App\Models\Category\Category::get();

@endphp


@extends('web.layouts.master')
@push('style_css')
@endpush
@section('header_class' , ' section-banner')
@push('more_header')
    <div class="container pt-5">

        <div class="row">


            <div class="col-md-5  pt-5 offset-sm-12">
                <div class="header home_sidebar_header pb-2 pt-5 font-weight-bold">
                    {{t_label('communicate_with_service')}}


                </div>
                <div
                    class="sub-header d-none d-sm-block mt-2 ">{{t_label('here_you_will_find_freelance_services')}}</div>
                <div class="banner-search">

                    <form class="banner-search-form  search-form d-flex mt-5" action="{{route('store.general_search')}}"
                          method="post">
                        @csrf
                        {{--    <span class="search-icon">
            <i class="fa fa-search"></i>
          </span>--}}
                        <input
                            class="form-control "
                            type="search"
                            name="filter"
                            value="{{old('filter')}}"
                            placeholder="{{t_label('search')}}"

                            {{--                                                                         placeholder="تطوير مواقع الكترونية"--}}

                        />
                        <button class="btn btn-primary btn-lg" type="submit"><i class="fa fa-search"></i></button>
                    </form>
                    @if($rabdom_categories && count($rabdom_categories) )
                        <div class="most-requested">
                        <span class="most-requested-title"> {{t_label('most_wanted')}}
                        </span>
                            <div class="most-requested-list">
                                @foreach($rabdom_categories as $cat)
                                    <div class="most-requested-item"
                                         onclick="location.href='{{route('store.ServicesBaseSubCategory',$cat->id)}}'">
                                             {{$cat->name}}
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    @endif

                </div>

            </div>

            <div class="col-md-7  header_img_section  pt-2">
                <div class="swiper header_swiper_slider">
                    <div class="swiper-wrapper  ">
                        @foreach(range(1,7) as $num)
                            <div class="swiper-slide">
                                {{--                                <img src="{{asset('/media/bg/globe-mobile.14616ad.jpg')}}" class="img-fluid">--}}
                                <img src="{{asset("/media/svg/sliders/vectors-0$num.svg")}}" class="img-fluid  ">
                                {{--                                <img src="{{asset('/media/bg/hero-instance-2--desktop@2x.webp')}}" class="img-fluid">--}}
                            </div>
                        @endforeach
                    </div>
                    {{--   <div class="swiper-button-next"></div>
                       <div class="swiper-button-prev"></div>
                       <div class="swiper-pagination"></div>--}}
                    <div class="header_swiper_slider-pagination"></div>

                </div>

            </div>

        </div>

    </div>

@endpush
@section('content')

    <main>
        {{--        @include('web.home.header')--}}


        <div class="subcatigory section container-fluid my-4 py-5">
            <h3 class="text-start my-2 pb-3 px-4 " >{{t_label('Popular professional services')}}</h3>
            <div class="swiper mySwiper">
                <div class="swiper-wrapper landing-swiper">
                    @foreach($sub_first_categories as $category)
                        <div class="swiper-slide">
                            <a href="{{route('store.ServicesBaseSubCategory',$category->id)}}">
                                <img data-src="{{asset($category->image)}} " class="lazy" alt=""/>
                                <div>
                                         <h4>{{$category->name}} </h4>


                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>
                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>
                <div class="swiper-pagination"></div>
            </div>
        </div>
        <!-- end subcatigory -->


        <div class="freelancer_video container-fluid">
            <div class="row px-3 pt-2">
                <div class="col-md-5 col-sm-12 left">
                    <h2 class="mb-4" style="color: #0f2a4a">
                        {{t_label('Theres a whole world of talent waiting for you')}}

                        </h2>

                    @foreach(['Exceptional fee rates',"Middle East's largest market" , "First freelance portal with the option of a monthly subscription fee-free"] as $one)

                        <div class="py-2  px-2">
                            <h5 class="d-flex">

                                <div class="px-2">
                                    <img class=" lazy" data-src="{{asset('web_en/img/apple-touch-icon.png')}}">

                                </div>
                                <div>
                                    {{t_label($one)}}

                                </div>


                            </h5>
                            {{--                           <p class="px-2">Find high-quality services at every price point. No hourly rates, just project-based pricing.</p>--}}
                        </div>
                    @endforeach
                </div>
                <div class="col-md-7 col-sm-12 ">
                    <div class="video_play">
                        <div class="auto_play_section">
                            <div class="auto_play_icon">
                                <i class="fa fa-play"></i>


                            </div>
                        </div>

                        <img data-src="{{asset('media/video_img.jpeg')}}" class="img-fluid lazy">
                    </div>
                    {{--                        <video  src="{{asset('media/Freelance_Arabia_Platform.mp4')}}" controls class="mt-3" width="100%" height="400"  title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></video>--}}
                    {{--                        <iframe class="mt-3" width="100%" height="400" src="https://www.youtube.com/embed/gVwgOxJOLWA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>--}}
                </div>

            </div>
        </div>

        <!-- end freelancer_video -->
        <!-- begin how-it-works -->
        <div class="section section-how-it-works">
            <h2 class="text-center section-title">
                {{t_label('whole_world_of_premium_services')}}
            </h2>
            <!-- begin content-boxes -->
            <div class="content-boxes">
                <!-- begin box -->
                <div class="box">
                    <div class="box-image">
                        <img class="lazy" data-src="{{asset('web_ar/img/box-3.png')}}" alt=""/>
                    </div>
                    <div class="box-title">{{t_label('implement_your_projects')}}</div>
                    <div class="box-content">{{t_label('choose_your_service_according')}}</div>
                </div>
                <!-- end box -->
                <!-- begin box -->
                <div class="box">
                    <div class="box-image">
                        <img data-src="{{asset('web_ar/img/box-2.png')}}" class="lazy" alt=""/>
                    </div>
                    <div class="box-title">{{t_label('get_your_work_done_quickly')}}</div>
                    <div class="box-content">
                        {{t_label('choose_the_right_freelancer_to_get')}}
                    </div>
                </div>
                <!-- end box -->
                <!-- begin box -->
                <div class="box">
                    <div class="box-image">
                        <img data-src="{{asset('web_ar/img/box-1.png')}}"  class="lazy" alt=""/>
                    </div>
                    <div class="box-title">{{t_label('guaranteed_payment_method')}}</div>
                    <div class="box-content">{{t_label('dont_pay_until_you_agree_to_work')}}</div>
                </div>
                <!-- end box -->
            </div>
            <!-- end content-boxes -->
        </div>
        <!-- end how-it-works -->

        <!-- begin services -->
        <div class="section section-services">
            <h2 class="text-center text-white section-title">{{t_label('service_sections')}}</h2>
            <div class="service-list">
                <div class="content-boxes">
                @foreach($original_categories as $category)
                    <!-- begin box -->


                        <div class="box"
                             onclick="document.location='{{route('store.ServicesBaseSubCategory',$category)}}'">
                            <div class="box-image">
                                <img width="80px" class="lazy" height="80px" style="border-radius: 25px"
                                     data-src="{{asset($category->image)}}" alt=""/>
                            </div>

                                <div class="box-title">{{$category->name}}</div>

                        </div>
                        <!-- end box -->
                    @endforeach

                </div>
            </div>
        </div>
        <!-- end services -->

        <div class="section section-start-now text-center">
            <div class="start-now-wrapper row">
                <div class="col-12 col-md-12 col-lg-12">
                    <h2 class="d-inline">{{t_label('find_the_talent_needed_to_grow')}}</h2>
                </div>
                <div class="col-12 col-md-12 col-lg-12 text-center mt-3">
                    <button data-bs-target="#SignupModal" data-bs-toggle="modal" href="#"
                            class="btn btn-primary bold">{{t_label('start_now')}}</button>
                </div>
            </div>
        </div>
        <!-- end start-now -->
    </main>
@endsection


