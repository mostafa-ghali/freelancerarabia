 @extends('web.layouts.master')
@section('content')
    <main class="container-fluid my-5 py-4">
        <div class="section section-faqs">
            <h2 class="text-start section-title">{{t_label('common_questions')}}</h2>
            <div class="accordion" id="accordionExample">
                @foreach($general_questions as $general_question)
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="headingOne">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#collapse_{{$general_question->id}}" aria-expanded="true"
                                    aria-controls="collapseOne">

                                    {{$general_question->question}}

                             </button>
                        </h2>
                        <div id="collapse_{{$general_question->id}}" class="accordion-collapse collapse show"
                             aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                            <div class="accordion-body">

                                    {{$general_question->answer}}

                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </main>
    <!-- end main -->
@endsection
