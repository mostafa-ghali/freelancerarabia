@extends('web.layouts.master')
@section('content')
    <br>
    <br>
    <!-- begin top-services -->
    {{--    <div class="top-services">
            <div class="owl-carousel top-services-carousel">
                @foreach($categories as $category)
                    @if(getLang() =='ar')
                        <div class="item text-center">{{$category->name_ar}}</div>
                    @else
                        <div class="item text-center">{{$category->name_en}}</div>
                    @endif
                @endforeach

            </div>
        </div>--}}
    <!-- end top-services -->


    <!-- begin main -->
    <main class="container-fluid">
        @if(session()->has('alert-success'))
            <div class="alert alert-success" style="text-align: center">{{session()->get('alert-success')}}</div>
    @endif
    <!-- begin welcome-banner -->
        <div class="section section-welcome-banner">
            <div class="row m-0">
                <div class="col-12 col-md-6 col-lg-4 section-welcome-banner-start ">
                    <h2>{{t_label('welcome') .' '.auth('web')->user()->name}}
                    </h2>
                    <div class="mt-4">
                        {{t_label('get_the_service_you_want')}}
                    </div>
                    <div class="mt-4">
                        <form class="banner-search-form store-search search-form d-flex"
                              action="{{route('store.home_search')}}" method="get">
                            @csrf
                            <span class="search-icon">
                    <i class="fa fa-search"></i>
                  </span>
                            <input
                                class="form-control"
                                type="search"
                                placeholder="{{t_label('search')}}"
                                name="filter"
                            />
                            <button class="btn btn-primary btn-lg" type="submit"> {{t_label('search')}} </button>
                        </form>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-8 section-welcome-banner-end pe-0 mt-3 mt-md-0 ps-0 ps-md-3">
                    <div id="carouselExampleCaptions" class="carousel slide " data-bs-ride="carousel">
                        <div class="carousel-indicators">
                            <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0"
                                    class="active" aria-current="true" aria-label="Slide 1"></button>
                            <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1"
                                    aria-label="Slide 2"></button>
                            <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2"
                                    aria-label="Slide 3"></button>
                        </div>
                        <div class="carousel-inner">
                            @foreach($home_sliders as $key => $home_slider)

                                <div class="carousel-item {{$key == 0 ? 'active' : '' }}" style="height: 300px">
                                    <div class="welcome-image"
                                         style="background-image: url('{{asset($home_slider->image)}}');"></div>
                                    <div class="carousel-caption d-none d-md-block" style="color: black">
                                        <div class="welcome-title">
                                            <h5>
                                                    {{$home_slider->title_1}}

                                            </h5>
                                        </div>
                                        <div class="welcome-subtitle">
                                            <p>
                                                     {{$home_slider->title_2}}
                                                .</p>
                                        </div>
                                    </div>
                                </div>
                            @endforeach

                        </div>
                        <button hidden class="carousel-control-prev" type="button"
                                data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="visually-hidden">{{t_label('Previous')}}</span>
                        </button>
                        <button hidden class="carousel-control-next" type="button"
                                data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="visually-hidden">{{t_label('Next')}}</span>
                        </button>
                    </div>
                </div>

            </div>
        </div>
        <!-- end welcome-banner -->

        <!-- begin new-services -->
        <div class="section section-new-services">
            <h4 class="text-start my-2 section-title">{{t_label('latest_services')}}</h4>

            <div class="subcat py-3">
                <div class="">
                    <div class="row">
                        @if($latest_services->count()>0)
                            @foreach( $latest_services as $service)
                                <div class="my-2 col-md-3 col-sm-12">
                                    @component('web.services.service_card',['service'=>$service])@endcomponent
                                </div>
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>

        </div>
        <!-- end new-services -->


        <!-- begin recommended-services -->
        <div class="section section-recommended-services">
            <h4 class="text-start my-2 section-title">{{t_label('services_you_may_like')}}</h4>
            <div class="subcat py-3">
                <div class="">
                    <div class="row">
                        @if($random_services->count()>0)
                            @foreach( $random_services as $service)
                                <div class="my-2 col-md-3 col-sm-12">
                                    @component('web.services.service_card',['service'=>$service ])@endcomponent

                                </div>
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>

        </div>
        <!-- end recommended-services -->

        <!-- begin faqs -->
        <div class="section section-faqs">
            <h4 class="text-start my-2 section-title">{{t_label('common_questions')}}</h4>
            <div class="accordion" id="accordionExample">
                @foreach($general_questions as $general_question)
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="headingOne">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse"

                                    {{$general_question->question}}
                            </button>
                        </h2>
                        <div id="collapse_{{$general_question->id}}" class="accordion-collapse collapse show"
                             aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                    {{$general_question->answer}}

                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <!-- end faqs -->
    </main>
    <!-- end main -->

@endsection
@section('scripts')
    <script>
        var swiper = new Swiper(".mySwiper", {
            slidesPerView: 1,
            slidesPerGroup: 1,
            loop: true,

            loopFillGroupWithBlank: true,
            navigation: {
                nextEl: ".swiper-button-next",
                prevEl: ".swiper-button-prev",
            },

        });
    </script>
@endsection

