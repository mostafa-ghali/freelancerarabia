@extends('web.layouts.master')
@section('not_strong_style')

    <link href="{{url('web_en/css//doorloopcrm.webflow.472313bb0.min.css')}}"
          rel="stylesheet" type="text/css"/>

@endsection
@section('content')


         <main class="main-box">
             <div class="container">
                 <div class="custom-card-header mt-5">
                     @if(session()->has('alert-success'))
                         <div class="alert alert-success" style="text-align: center">{{session()->get('alert-success')}}</div>
                     @endif
                 </div>

                 <form>
                     <div class="price-table-main">
                         <div class="pricing-table">
                             {{--                <div class="pricing-table-header">--}}



                             @foreach($packages as  $package)
                                 <div class="plan-price-block">
                                     <div class="price-plan-top plan--second">
                                         <div>{{$package->package_name}}</div>
                                     </div>
                                     <div class="price-plan-block">
                                         <div class="price-top-main">
                                             <div id="toDisablePrice" class="price-top-numbers">

                                                 @if($package->old_price >0)
                                                  <p
                                                      class="prices monthly-price price-strike">$<span
                                                          id="StandardPriceMonthly" class="price-strike-text">{{$package->old_price}}</span><span
                                                          id="StandardPriceMonthlyDecimals" class="decimals">00</span
                                                      ></p>
                                                 @endif
                                                 <p class="prices monthly-price">$<span id="StandardPriceMonthlyCoupon"
                                                                                        class="span-price">{{$package->new_price}}</span><span
                                                         id="StandardPriceMonthlyDecimalsCoupon" class="decimals">00</span>
                                                 </p>
                                                 {{--                                        <div class="billed-monthly-label">for the first 2 months</div>--}}
                                             </div>
                                             <div class="period-text">{{$package->no_monthes}} {{t_label('Month with system commission')}} </div>
                                             <div class="plan-popular--label"></div>
                                         </div>
                                         <div class="plan-btn"><a href="#" class="btn btn-cta btn-pricing w-inline-block mb-2 ">
                                                 @if(auth()->user()->package_id !== $package->id)
                                                     <div onclick="location.href='{{route('packages.subscribe', $package)}}'"   class="cta-btn-text cta-on-pricing">{{t_label('Subscribe package')}}</div>
                                                 @else
                                                 <div onclick="location.href='{{route('packages.subscribe', $package)}}'" class="cta-btn-text cta-on-pricing">{{t_label('Renew package')}}</div>

                                                 @endif
                                                 <img
                                                     src="https://global-uploads.webflow.com/5f073e32d304276cc8b4ff30/602f3eff85f3c117e5e38819_arrow-white.svg"
                                                     loading="lazy" alt="" class="btn-arrow image pricing"/></a>
                                         </div>
                                     </div>
                                 </div>
                             @endforeach

                         </div>


                     </div>
                 </form>

             </div>

        </main>

@endsection
