@php


    $original_categories = \App\Models\Category\Category::get();

@endphp

@extends('web.layouts.master')
@push('style_css')
@endpush
@section('header_class' , '   ')
@push('more_header')
    <div class="banne pt-3 px-4" style="">
        <div class="jumbotron jumbotron-fluid mt-5 pt-2" style="
          height: 300px;
width: 100%;
background: linear-gradient(55deg,#006ece,#194275);
display: flex;
justify-content: center;
align-items: center;
flex-direction: column;
margin-top: 20px;
color: white;
border-radius:5px;
background-image:url('{{$main_category->banner}}');
background-size: 100% 100%;
            ">


            <div class="container">


                <p class="lead breadcrumb-wrapper mt-5">
                    @include('web.layouts.breadcrumbs')
                </p>


            </div>
        </div>
    </div>

@endpush
@section('content')

    <main>
        <div class="subcat ">
            <div class="container">
                <form
                    {{--                    action="{{route('store.filter.deliver_time_filter')}}"--}}
                    method="get">

                    {{--           <input type="hidden" name="deliver_time_filter"
                                      value="deliver_time_filter">--}}
                    <input type="hidden" name="sub_category_id"
                           value="{{$sub_category_id}}">
                    <div class="fillter row">
                        <div class="col-md-12 col-sm-12  d-flex">


                            @if($sub_categories->filtersOptions->count() > 0)
                                <div class="dropdown my-3 mx-2">
                                    <a class="aa btn  dropdown-toggle" href="#" role="button"
                                       id="dropdownMenuLink" data-display="static" data-bs-display="static"
                                       data-bs-toggle="dropdown" aria-expanded="false">
                                        {{t_label('Service Options') }}<i class="mt-1 fas fa-angle-down"></i>
                                    </a>


                                    <ul class="dropdown-menu  px-3 py-3"
                                        aria-labelledby="dropdownMenuLink">
                                        <div class="border-bottom">
                                            @foreach($sub_categories->filtersOptions as $filter_options)
                                                @if(\App\Models\Category\SubCategoryFiltersOptionsItems::where('sub_category_filters_options_id',$filter_options->id)->count() > 0)
                                                    <P>{{ $filter_options->sub_category_option_name}}</P>
                                                    <div
                                                        class="d-flex justify-content-between my-2 px-3">
                                                        @foreach(\App\Models\Category\SubCategoryFiltersOptionsItems::where('sub_category_filters_options_id',$filter_options->id)->get() as  $filter_options_item)
                                                            <div class="form-check px-2">
                                                                <input class="form-check-input"
                                                                       name="tag[]"

                                                                       {{in_array($filter_options_item->value, request('tag',[]))?"checked":""}}
                                                                       type="checkbox"
                                                                       value="{{$filter_options_item->value}}"
                                                                       id="flexCheckDefault_{{$filter_options_item->id}}">
                                                                <label class="form-check-label"
                                                                       for="flexCheckDefault_{{$filter_options_item->id}}">
                                                                    {{ $filter_options_item->option_filter_name }}
                                                                </label>
                                                            </div>
                                                        @endforeach
                                                    </div>
                                                @endif
                                            @endforeach


                                        </div>

                                        <div class="bottom d-flex justify-content-between">
                                            <a href="">{{t_label('clear all')}}</a>
                                            <button type="submit" class="btn btn-primary">
                                                {{t_label('apply')}}
                                            </button>
                                        </div>
                                    </ul>
                                </div>

                            @endif


                            <div class="dropdown my-3 mx-2">
                                <a class="aa btn  dropdown-toggle" href="#" role="button"
                                   data-display="static" data-bs-display="static"
                                   id="dropdownMenuLink"
                                   data-bs-toggle="dropdown" aria-expanded="false">
                                    {{t_label('Seller Details')}}
                                    <i class="mt-1 fas fa-angle-down"></i>
                                </a>

                                <ul class="dropdown-menu  px-3 py-3"
                                    aria-labelledby="dropdownMenuLink">

                                    <div class="border-bottom">
                                        <P>{{t_label('Seller Details')}}</P>
                                        <div
                                            class="d-flex justify-content-between my-2 px-3">
                                            <div class="form-check px-2">
                                                <input class="form-check-input"
                                                       type="radio"
                                                       value="fresh_talent"

                                                       {{"fresh_talent" ==  request('seller_type')?"checked":""}}
                                                       id="flexCheckDefault"
                                                       name="seller_type">
                                                <label class="form-check-label"
                                                       for="flexCheckDefault">
                                                    {{t_label('Fresh Talent')}}
                                                </label>
                                            </div>
                                            <div class="form-check px-2">
                                                <input class="form-check-input"
                                                       type="radio"
                                                       value="super_talent"
                                                       {{"super_talent" ==  request('seller_type')?"checked":""}}

                                                       id="flexCheckChecked"
                                                       name="seller_type">
                                                <label class="form-check-label"
                                                       for="flexCheckChecked">
                                                    {{t_label('Super Talent')}}

                                                </label>
                                            </div>
                                        </div>
                                        <div
                                            class="d-flex justify-content-between my-2 px-3 ">
                                            <div class="form-check px-2">
                                                <input class="form-check-input"
                                                       type="radio"
                                                       value="top_talent"
                                                       {{"top_talent" ==  request('seller_type')?"checked":""}}

                                                       id="flexCheckDefault"
                                                       name="seller_type">
                                                <label class="form-check-label"
                                                       for="flexCheckDefault">
                                                    {{t_label('Top Talent')}}

                                                </label>
                                            </div>

                                        </div>
                                    </div>

                                    <div class="bottom d-flex justify-content-between">
                                        <a href="">{{t_label('clear all')}}</a>
                                        <button type="submit" class="btn btn-primary">
                                            {{t_label('apply')}}
                                        </button>
                                    </div>

                                </ul>
                            </div>


                            <div class="dropdown budget my-3 mx-2">
                                <a class="aa btn  dropdown-toggle" href="#" role="button"
                                   data-display="static" data-bs-display="static"
                                   id="dropdownMenuLink"
                                   data-bs-toggle="dropdown" aria-expanded="false">
                                    {{t_label('Budget')}} <i class="mt-1 fas fa-angle-down"></i>
                                </a>

                                <ul class="dropdown-menu px-2 py-3"
                                    aria-labelledby="dropdownMenuLink">


                                    <div class="border-bottom">
                                        <div
                                            class="d-flex justify-content-between my-2 px-3">
                                            <div class="d-flex  row mx-2">
                                                <label class="col-12">{{t_label('From')}}</label>
                                                <input class="col-12" type="number"
                                                       name="min_price"
                                                       value="{{request('min_price')}}"
                                                       placeholder="0">
                                            </div>
                                            <div class="d-flex  row mx-2">
                                                <label class="col-12">{{t_label('To')}}</label>
                                                <input class="col-12" type="number"
                                                       name="max_price"

                                                       value="{{request('max_price')}}"
                                                       placeholder="1000000">
                                            </div>
                                        </div>
                                    </div>


                                    <div
                                        class="bottom py-2 d-flex justify-content-between">

                                        <a href="">{{t_label('clear all')}}</a>
                                        <button type="submit" class="btn btn-primary">
                                            {{t_label('apply')}}
                                        </button>
                                    </div>
                                </ul>
                            </div>

                            <div class="dropdown delivery my-3 mx-2">
                                <a class="aa btn  dropdown-toggle" role="button"
                                   data-display="static" data-bs-display="static"
                                   id="dropdownMenuLink"
                                   data-bs-toggle="dropdown" aria-expanded="false">
                                    {{t_label('Delivery Time')}} <i class="mt-1 fas fa-angle-down"></i>
                                </a>

                                <ul class="dropdown-menu  px-3 py-3"
                                    aria-labelledby="dropdownMenuLink">
                                    <div class="border-bottom">

                                        <div class="form-check">
                                            <input class="form-check-input" type="radio"
                                                   name="delivery_time"
                                                   value="one_day"
                                                   {{request('delivery_time') == "one_day"?"checked":""}}

                                                   id="flexRadioDefault1">
                                            <label class="form-check-label"
                                                   for="flexRadioDefault1">
                                                {{t_label('One Day')}}
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio"
                                                   name="delivery_time"

                                                   {{request('delivery_time') == "up_3_days"?"checked":""}}
                                                   value="up_3_days"
                                                   id="flexRadioDefault2">
                                            <label class="form-check-label"
                                                   for="flexRadioDefault2">

                                                {{t_label('Up 3 Day')}}

                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio"
                                                   name="delivery_time"
                                                   value="up_7_days"
                                                   {{request('delivery_time') == "up_7_days"?"checked":""}}

                                                   id="flexRadioDefault1">
                                            <label class="form-check-label"
                                                   for="flexRadioDefault1">
                                                {{t_label('Up 7 Day')}}

                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio"
                                                   name="delivery_time"
                                                   {{request('delivery_time') == "any_time"?"checked":""}}

                                                   value="any_time"
                                                   id="flexRadioDefault2">
                                            <label class="form-check-label"
                                                   for="flexRadioDefault2">
                                                {{t_label('Anytime')}}
                                            </label>
                                        </div>
                                    </div>

                                    <div
                                        class="bottom py-2 d-flex justify-content-between">

                                        <a href="">{{t_label('clear all')}}</a>
                                        <button type="submit" class="btn btn-primary">
                                            {{t_label('apply')}}
                                        </button>
                                    </div>

                                </ul>
                            </div>

                            <div class="form-check form-switch mx-2 mt-4">
                                <input class="form-check-input" type="checkbox"
                                       role="switch" name="local_seller"
                                       {{request('local_seller') ?"checked":""}}

                                       data-display="static" data-bs-display="static"
                                       id="flexSwitchCheckDisabled" value="1">
                                <label class="form-check-label"
                                       for="flexSwitchCheckDisabled">{{t_label('Local sellers')}}</label>
                            </div>
                            <div class="form-check form-switch mx-2 mt-4">
                                <input class="form-check-input" name="online_seller"
                                       type="checkbox" role="switch"
                                       {{request('online_seller') ?"checked":""}}

                                       data-display="static" data-bs-display="static"
                                       id="flexSwitchCheckCheckedDisabled" value="1">
                                <label class="form-check-label"

                                       for="flexSwitchCheckCheckedDisabled">
                                    {{t_label('Online sellers')}}
                                </label>
                            </div>


                        </div>
                    </div>
                </form>

                <div class="row">
                    @if($services->count()>0)
                        @foreach( $services as $service)
                            <div class="my-2 col-md-3 col-sm-12">
                                <div class="card">
                                    <div id="carouselExampleControls{{$service->id}}"
                                         class="carousel slide w-100 drop-shadow rounded"
                                         data-bs-ride="carousel">
                                        <div class="carousel-inner">
                                            @foreach($service->images as  $key => $slider)
                                                <div
                                                    class="carousel-item {{$key == 0 ? 'active' : '' }}">
                                                    <img src=" {{$slider->image}}"
                                                         class="d-block w-100"
                                                         alt="...">
                                                </div>
                                            @endforeach
                                        </div>
                                        <button class="carousel-control-prev"
                                                type="button"
                                                data-bs-target="#carouselExampleControls{{$service->id}}"
                                                data-bs-slide="prev">
                                                                            <span class="carousel-control-prev-icon"
                                                                                  aria-hidden="true"></span>
                                            <span
                                                class="visually-hidden">{{t_label('Previous')}}</span>
                                        </button>
                                        <button class="carousel-control-next"
                                                type="button"
                                                data-bs-target="#carouselExampleControls{{$service->id}}"
                                                data-bs-slide="next">
                                                                            <span class="carousel-control-next-icon"
                                                                                  aria-hidden="true"></span>
                                            <span class="visually-hidden">{{t_label('Next')}}</span>
                                        </button>
                                    </div>
                                    <!-- end swiper -->
                                    <div class="down ">
                                        <div class="avatarr px-2 py-2">
                                            <img src="{{$service->ServiceOwner->image}}"
                                                 class="avatar-image"/>
                                            <a>{{$service->ServiceOwner->name}}</a>
                                        </div>
                                        <div class="service-description px-2 py-2">
                                            <a href="#">{{$service->title}} </a>
                                        </div>
                                        <div
                                            class="d-flex justify-content-between px-2 pt-2">
                                            <p class="str pt-2 px-2">
                                                <span class="fa fa-star checked"></span>
                                                <span class=""> {{$service->rate}}</span>
                                            </p>
                                            <ul class="p-0 d-flex">
                                                <li class="pt-1">
                                                    <a href="{{route('store.user.add_fav',$service->id)}}">


                                                        @if ($service->is_favourite)
                                                            <i style="color:  red"
                                                               class="fas fa-heart"></i>
                                                        @else
                                                            <i style="color: #a7a5a5"
                                                               class="fas fa-heart"> </i>
                                                        @endif
                                                    </a>

                                                </li>
                                                <li class="pt-1">
                                                    <a href="#"><i class="fas fa-share"
                                                                   style="color: #a7a5a5"></i></a>

                                                </li>
                                            </ul>
                                        </div>
                                        <div class="bar">
                                            <a href="{{route('store.service.details',$service->id)}}"
                                               class="btn w-100">{{t_label('Service details')}}</a>
                                        </div>
                                    </div>
                                </div><!-- end card -->
                            </div>
                        @endforeach
                    @endif
                </div>


            </div>
        </div>
        <!-- end login-modal -->
    </main>

@endsection


