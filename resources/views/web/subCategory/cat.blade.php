
@extends('web.layouts.master')
@push('style_css')
@endpush
@section('header_class' , '  ')
@push('more_header')

@endpush
@section('content')

    <main>
        {{--        @include('web.home.header')--}}
        <div class="container-fluid py-5 mt-5 cate">
        <div class="bagr" style="background-image:url('{{$main_category->banner}}');
            background-size: 100% 100%;
             ">
        <p class="hed text-dark">{{$main_category->name}}</p>
{{--        <p class="titl">Get your words across—in any language.</p>--}}

                </div>
            <div class="mx-5">
                    <h2 class="text-center">{{ $main_category->name }}</h2>
                <h6 class="text-center">

                </h6>
            </div>
            <div class="row pt-3">

            <div class="col-md-12 col-sm-12 rightbar">
                    <div class="container">
                        <div class="row m-0 p-0 justify-content-center">
                            @if($subCategory->count() > 0)
                                @foreach($subCategory as $category)
                                    <div class="col-md-4 py-2 category-box">
                                        <a href="{{route('store.ServicesBaseSubCategory',$category->id)}}">
                                            <div class="">
                                                <img src="{{$category->image}} " alt="{{$category->image}}" class="category-image">
                                                <h4 class="px-1 pt-2">{{ $category->name }}</h4>

                                            </div>
                                        </a>
                                    </div>
                                @endforeach
                            @endif

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end start-now -->
    </main>
@endsection


