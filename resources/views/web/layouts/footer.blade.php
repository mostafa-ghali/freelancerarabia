<footer class="footer">
    <div class="footer-wrapper row">
        <div class="col-12 col-md-4 footer-start">
            <img data-src="{{asset('web_ar/img/logo-white.png')}}" class="lazy footer-logo" alt="" />
            <br>
            <br>
            <div class="social-links">
                <a href="https://twitter.com/FreelanceArabi1" rel="noopener noreferrer" target="_blank" class="social-link">
                    <i class="fab fa-twitter"></i>
                </a>
                <a href="https://www.facebook.com/FreelanceArabia.ae" target="_blank" rel="noopener noreferrer" class="social-link">
                    <i class="fab fa-facebook"></i>
                </a>
                <a href="https://www.instagram.com/freelancearabia.ae/" target="_blank" rel="noopener noreferrer" class="social-link">
                    <i class="fab fa-instagram"></i>
                </a>

                <a href="https://www.linkedin.com/in/freelance-arabia-ba1353220/" target="_blank" rel="noopener noreferrer" class="social-link">
                    <i class="fab fa-linkedin"></i>
                </a>
            </div>
            <div class="footer-payment">
                <img
                     src="{{asset('web_ar/img/visa.png')}}"
                    alt="visa"
                    width="50px"
                 />
                <img
                    class="" src=" {{asset('web_ar/img/mastercard.png')}}" alt="paypal" width="50px" />
                <img class=" " src=" {{asset('web_ar/img/paypal.png')}}" alt="paypal" width="50px" />
            </div>
        </div>
        <div class="col-12 col-md-8 footer-end">
            <div class="row">
                <!-- begin footer-list -->
                <div class="col-12 col-md-6 col-lg-4">
                    <ul class="footer-list">
                        <li class="footer-list-item footer-list-title">{{t_label('entrepreneur')}}</li>
                        <li class="footer-list-item">
                            <a href="{{route('store.common_questions')}}" class="footer-link"> {{t_label('common_questions_footer')}} </a>
                        </li>

                        <li class="footer-list-item">
                            <a  class="footer-link"
                                data-bs-target="#SignupModal"
                                data-bs-toggle="modal"
                            >
                                <i class="footer-link"></i>{{t_label('subscribe')}}
                            </a>
                        </li>

                        <li class="footer-list-item">
                            <a href="mailto:info@freelancearabiauae.com" class="footer-link"> {{t_label('proposals')}} </a>
                        </li>
                    </ul>
                </div>
                <!-- end footer-list -->
                <!-- begin footer-list -->
                <div class="col-12 col-md-6 col-lg-4">
                    <ul class="footer-list">
                        <li class="footer-list-item footer-list-title">{{t_label('service_owner')}}</li>
                        <li class="footer-list-item">
                            <a href="{{route('store.common_questions')}}" class="footer-link"> {{t_label('common_questions_footer')}} </a>
                        </li>
                        <li class="footer-list-item">
                            <a
                                class="footer-link"
                                data-bs-target="#SignupModal"
                                data-bs-toggle="modal"
                            >
                                <i class="footer-link"></i>{{t_label('subscribe')}}
                            </a>
                        </li>
                        <li class="footer-list-item">
                            <a href="mailto:info@freelancearabiauae.com" class="footer-link"> {{t_label('proposals')}} </a>
                        </li>
                    </ul>
                </div>
                <!-- end footer-list -->
                <!-- begin footer-list -->
                <div class="col-12 col-md-6 col-lg-4">
                    <ul class="footer-list">
                        <li class="footer-list-item footer-list-title">
                            <a href="{{route('store.about_us')}}" class="footer-link" style="font-size:1.15rem !important"> {{t_label('about_us')}}</a>
                        </li>
                        <li class="footer-list-item">
                            <a href="{{route('store.privacy')}}" class="footer-link">{{t_label('privacy_policy')}} </a>
                        </li>
                        <li class="footer-list-item">
                            <a href="{{route('store.terms_conditions')}}" class="footer-link"> {{t_label('terms_conditions')}} </a>
                        </li>
{{--                        <li class="footer-list-item">--}}
{{--                            <a--}}
{{--                                class="footer-link"--}}
{{--                                data-bs-target="#ContactUsModal"--}}
{{--                                data-bs-toggle="modal"--}}
{{--                            >--}}
{{--                                <i class="footer-link"></i>{{t_label('contact_us')}}--}}
{{--                            </a>--}}
{{--                        </li>--}}
                    </ul>
                </div>
                <!-- end footer-list -->
            </div>
        </div>
        <hr />
        <div class="col-12 text-end text-white" style="direction: ltr">

            {{t_label('All right reserved | Freelance-Arabia.com')}}
        </div>
    </div>
</footer>
<!-- end footer -->
