<!-- begin signup-modal -->
<div
    class="modal modal-blur fade"
    id="SignupModal"
    tabindex="-1"
    aria-labelledby="SignupModalLabel"
    aria-hidden="true"
>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h3 class="modal-title w-100" id="SignupModalLabel">
                    {{t_label('create_accont')}}
                </h3>
            </div>
            <div class="modal-body">


                <form action="{{route('store.register')}}" method="POST" class="requires-validation" novalidate>
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    @if(session()->has('alert-fail-send-otp'))
                        <div class="alert alert-danger" role="alert">
                            {{session()->get('alert-fail-send-otp')}}
                        </div>
                    @endif
                    @csrf
                    <div class="col-md-12">
                        <input
                            class="form-control"
                            type="email"
                            name="user_email"
                            placeholder="{{t_label('email')}}"
                            required
                        />
                    </div>
                    <div class="col-md-12 mt-3">
                        <input
                            class="form-control"
                            type="text"
                            name="user_name"
                            placeholder=" {{t_label('username')}} "
                            required
                        />
                    </div>

                    <div class="col-md-12 mt-3">
                        <input
                            class="form-control"
                            type="password"
                            name="user_password"
                            placeholder="{{t_label('pass')}}"
                            required
                        />
                    </div>

                    <div class="col-12 font-xl text-center mt-4 sideline">
                        <span>{{t_label('or')}}</span>
                    </div>

                    <div
                        class="col-12 d-flex  gap-4 justify-content-center mt-4 mx-auto w-50"
                    >
                        <a href="{{route('SocialAuth.redirect','google')}}"><img
                                src="{{asset('web_ar/img/google.png')}}" alt=""/></a>
                        {{--                        <a href="#"><img src="{{asset('web_ar/img/apple.png')}} " alt="" /></a>--}}
{{--                        <a href="#"><img src="{{asset('web_ar/img/facebook.png')}} " alt=""/></a>--}}
                        <a href="{{route('SocialAuth.redirect','facebook')}}" ><img
                                src="{{asset('web_ar/img/facebook.png')}} " alt=""/></a>

                    </div>

                    <!--
                <div class="form-check">
                  <input class="form-check-input" type="checkbox" value="" id="invalidCheck" required>
                  <label class="form-check-label">I confirm that all data are correct</label>
                 <div class="invalid-feedback">Please confirm that the entered data are all correct!</div>
                </div> -->

                    <div class="form-button mt-4">
                        <button
                            type="submit"
                            class="btn btn-primary w-100 btn-hover"
{{--                            data-bs-dismiss="modal" data-bs-toggle="modal" href="#VerifyEmailModal"--}}
                        >
                            {{t_label('create_accont')}}
                        </button>
                    </div>
                    <div class="col-12 mt-2 text-center " style="font-size: .875rem">
                        <span>{{t_label('joining_means_agreeing_to')}}</span>
                        <a href="{{route('store.terms_conditions')}}"> <b>{{t_label('terms_conditions')}}</b></a>
                    </div>
                    <div class="col-md-12 mt-4 text-center">
                        <span>{{t_label('have_account')}}</span>
                        <!-- <span
                          class="cursor-pointer"
                          data-bs-toggle="modal"
                          data-target="#LoginModal"
                          ><b>تسجيل الدخول</b></span
                        > -->
                        <a data-bs-toggle="modal" data-bs-dismiss="modal" data-bs-target="#LoginModal"
                           href="#"><b>{{t_label('login')}}</b></a>

                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- end signup-modal -->
<!-- begin login-modal -->
<div
    class="modal modal-blur fade"
    id="LoginModal"
    tabindex="-1"
    aria-labelledby="LoginModalLabel"
    aria-hidden="true"
>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h5 class="modal-title w-100" id="LoginModalLabel">{{t_label('login')}}</h5>
            </div>

            <div class="modal-body">
                @if(session()->has('alert-fails'))
                    <div class="alert alert-danger" role="alert">
                        {{session()->get('alert-fails')}}
                    </div>
                @endif
                <form class="requires-validation" novalidate method="POST" action="{{ route('store.login') }}">
                    @csrf
                    <div class="col-md-12">
                        <input
                            class="form-control"
                            type="email"
                            name="email"
                            placeholder="{{t_label('email')}}"
                            required
                        />
                    </div>

                    <div class="col-md-12 mt-3">
                        <input
                            class="form-control"
                            type="password"
                            name="password"
                            placeholder="{{t_label('pass')}}"
                            required
                        />
                    </div>

                    <div class="col-12  font-xl text-center mt-4 sideline">
                        <span>
                            <span class="orText">{{t_label('or')}}</span>
                        </span>
                    </div>

                    <div
                        class="col-12 d-flex gap-4 justify-content-center mt-4 mx-auto w-50"
                    >
                        <a href="{{route('SocialAuth.redirect','google')}}" ><img
                                src="{{asset('web_ar/img/google.png')}}" alt=""/></a>
                        {{--                        <a href="{{route('SocialAuth.redirect','apple')}}"><img src="{{asset('web_ar/img/apple.png')}} " alt="" /></a>--}}
                        <a href="{{route('SocialAuth.redirect','facebook')}}" ><img
                                src="{{asset('web_ar/img/facebook.png')}} " alt=""/></a>
                    </div>

                    <div class="form-button mt-4">
                        <button

                            type="submit"
                            class="btn btn-primary w-100 btn-hover font-sm"
                        >
                            {{t_label('login')}}
                        </button>
                    </div>
                    <div class="col-12 d-flex justify-content-between mt-1">
                        <label class="form-check-label "
                        ><input
                                class="form-check-input me-2"
                                type="checkbox"
                                value=""
                                id="rememberMe"
                            />{{t_label('remember')}}</label
                        >
                        <a class="cursor-pointer " data-bs-dismiss="modal" data-bs-toggle="modal"
                           href="#ForgotPasswordModal">
                            {{t_label('forgot_pass')}}
                        </a>
                    </div>
                    <div class="col-md-12 mt-4  text-center">
                        <span>{{t_label('dont_have_account')}}</span>
                        <!-- <span class="cursor-pointer"><b>أنشئ حساب جديد</b></span> -->
                        <a data-bs-dismiss="modal" role="button" data-bs-toggle="modal"
                           href="#SignupModal"><b>{{t_label('create_accont')}}</b></a>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>
<!-- end login-modal -->
<!-- begin login-modal -->
<div
    class="modal modal-blur fade"
    id="ContactUsModal"
    tabindex="-1"
    aria-labelledby="ContactUsLabel"
    aria-hidden="true"
>
    <div class="modal-dialog modal-xl">
        <div class="modal-content">


            <div class="modal-body p-0">
                <div class="row">
                    <div class="col-lg-5 col-12 contact-bg">
                        <div
                            class="d-flex py-lg-0 py-4 flex-column gap-3 container w-75 justify-content-center align-items-center h-100">
                            <div class="d-flex w-100">
                                <div style="width: 50px">
                                    <i class="fas fa-map-marker-alt"></i>
                                </div>
                                <div class="">
                                    <p class="mb-0 text-dark font-weight-bold">Address</p>
                                    <span class="font-size-xs">Lorem ipsum dolor sit amet.</span>
                                </div>
                            </div>
                            <div class="d-flex w-100">
                                <div style="width: 50px">
                                    <i class="fas fa-phone-alt"></i>
                                </div>
                                <div class="">
                                    <p class="mb-0 text-dark font-weight-bold">Let's talk</p>
                                    <span class="font-size-xs">+971 000 0000 0</span>
                                </div>
                            </div>

                            <div class="d-flex w-100">
                                <div style="width: 50px">
                                    <i class="fas fa-envelope"></i>
                                </div>
                                <div class="">
                                    <p class="mb-0 text-dark font-weight-bold">General Support</p>
                                    <span class="font-size-xs">contact@gmail.com</span>
                                </div>
                            </div>
                        </div>


                    </div>
                    <div class="col-lg-7 col-12 bg-light">
                        <div class="container">
                            <div class="requires-validation contactForm">
                                <div class="modal-header text-center">
                                    <h2 class="modal-title text-primary w-100"
                                        id="ContactUsLabel">{{t_label('contact_us')}}</h2>
                                </div>
                                <div class="col-md-12">
                                    <label for="company_name" class="form-check-label form-label ">
                                        {{t_label('company_name')}}
                                    </label>

                                    <input class="form-control" value="" id="company_name"
                                           placeholder="Free Lance Arabia Portal"/>
                                </div>

                                <div class="col-md-12">
                                    <label for="complete_address" class="form-check-label form-label ">
                                        {{t_label('complete_address')}}
                                    </label>

                                    <textarea id="complete_address" class="form-control" rows="3"
                                              placeholder=" Level 1, Al Bateen Tower C6 Bainunah , ADIB Building, Street 34 ,Abu Dhabi, United Arab Emirates"></textarea>
                                </div>
                                <div class="col-md-12">
                                    <label for="country" class="form-check-label ">
                                        {{t_label('country')}}
                                    </label>

                                    <input class="form-control" id="country" placeholder="United Arab Emirates"
                                           value=""/>
                                </div>

                                <div class="col-md-12">
                                    <label for="contact_nu" class="form-check-label ">
                                        {{t_label('contact_us')}}
                                    </label>

                                    <input class="form-control" placeholder=" +972521343040" id="contact_nu" value=""/>
                                </div>

                                <div class="col-md-12">
                                    <label for="email" class="form-check-label ">
                                        {{t_label('email')}}
                                    </label>

                                    <input id="email" class="form-control" placeholder=" info@freelancearabiauae.com"
                                           value=""/>
                                </div>

                                <br>

                                <div class="col-12 text-center mb-4">
                                    <button type="submit" class="btn btn-primary" style="min-width: 120px">Send</button>
                                </div>


                            </div>
                        </div>
                    </div>

                </div>


            </div>
        </div>
    </div>
</div>
<!-- end login-modal -->
<!-- begin login-modal -->

<!-- end login-modal -->
<!-- begin verify-email-modal -->
<div
    class="modal modal-blur fade"
    id="VerifyEmailModal"
    tabindex="-1"
    aria-labelledby="VerifyEmailModalLabel"
    aria-hidden="true"
>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h5 class="modal-title w-100" id="VerifyEmailModalLabel">{{t_label('Email verification')}}</h5>
            </div>
            <div class="modal-body">
                <div class="text-center">
                    {{t_label('Enter the verification code')}}
                </div>
                <form class="requires-validation" novalidate method="post" action="{{route('store.email_verification')}}">
                    @if ($errors->any())
                        <div class="alert alert-danger" >
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    @if(session()->has('alert-fails'))
                        <div class="alert alert-danger" role="alert">
                            {{session()->get('alert-verify-email')}}
                        </div>
                    @endif
                    @csrf
                    <div class="col-md-12 mt-4">
                        <div class="text-center partitioned-input-outer">
                            <div class="text-cente partitioned-input-inner form form-group">
                                {{--                                <input type="email" name="verification_email" class="form-control" id="verification_email" value="{{old('verification_email')}}" >--}}
                                <br>
                                <input type="text" maxlength="4"  name="verification_otp" class="partitioned-input"/>
                            </div>
                        </div>
                    </div>

                    <div class="form-button mt-4">
                        <button
                            {{--                            href="landing-buyer.html"--}}
                            type="submit"
                            class="btn btn-primary w-100 btn-hover font-sm"
                        >
{{--                            تسجيل الدخول--}}

                            {{t_label('Log in')}}
                        </button>
                    </div>

                    {{--                    <div class="col-md-12 mt-4 text-white text-center">--}}
                    {{--                        <span>لم يصلني كود التفعيل؟</span>--}}
                    {{--                        <!-- <span class="cursor-pointer"><b>أنشئ حساب جديد</b></span> -->--}}
                    {{--                        <a href="#"><b>اعد ارسال الكود</b></a>--}}
                    {{--                    </div>--}}
                </form>
            </div>
        </div>
    </div>
</div>
{{--<div
    class="modal modal-blur fade"
    id="VerifyEmailModal"
    tabindex="-1"
    aria-labelledby="VerifyEmailModalLabel"
    aria-hidden="true"
>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h5 class="modal-title w-100" id="VerifyEmailModalLabel">تفعيل الايميل</h5>
            </div>
            <div class="modal-body">

                <div class="text-center">

                    <div class="text-center text-white">

                        {{t_label(' Enter  the verification code')}}

                    </div>
                    <form class="requires-validation" novalidate method="post"
                          action="{{route('store.email_verification')}}">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        @if(session()->has('alert-fails'))
                            <div class="alert alert-danger" role="alert">
                                {{session()->get('alert-verify-email')}}
                            </div>
                        @endif
                        @csrf
                        <div class="col-md-12 mt-4">
                            <div class="text-center partitioned-input-outer">
                                <div class="text-cente partitioned-input-inner form form-group">
                                    --}}{{--                                <input type="email" name="verification_email" class="form-control" id="verification_email" value="{{old('verification_email')}}" >--}}{{--
                                    <br>
                                    <input type="text" maxlength="4" name="verification_otp" class="partitioned-input"/>
                                </div>
                            </div>
                        </div>

                        <div class="form-button mt-4">
                            <button
                                --}}{{--                            href="landing-buyer.html"--}}{{--
                                type="submit"
                                class="btn btn-primary w-100 btn-hover font-sm"
                            >
                                {{t_label('Log in')}}

                            </button>
                        </div>

                        --}}{{--                    <div class="col-md-12 mt-4 text-white text-center">--}}{{--
                        --}}{{--                        <span>لم يصلني كود التفعيل؟</span>--}}{{--
                        --}}{{--                        <!-- <span class="cursor-pointer"><b>أنشئ حساب جديد</b></span> -->--}}{{--
                        --}}{{--                        <a href="#"><b>اعد ارسال الكود</b></a>--}}{{--
                        --}}{{--                    </div>--}}{{--
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>--}}
<!-- end verify-email-modal -->
<!-- begin forgot-password-modal -->
<div
    class="modal modal-blur fade"
    id="ForgotPasswordModal"
    tabindex="-1"
    aria-labelledby="ForgotPasswordModalLabel"
    aria-hidden="true"
>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h5 class="modal-title w-100" id="ForgotPasswordModalLabel"> {{t_label('Forget Password ? ')}}</h5>
            </div>
            <div class="modal-body">

                <div class="text-center">

                    <div class="text-center text-white">
                        {{t_label('To reset your password, enter your email address')}}
                     </div>
                    <form class="requires-validation" novalidate>
                        <div class="col-md-12 mt-4">
                            <input
                                class="form-control"
                                type="text"
                                name="email"
                                 placeholder="{{t_label('email')}}"
                                required
                            />
                        </div>

                        <div class="form-button mt-4">
                            <button
                                type="button"
                                class="btn btn-primary w-100 btn-hover"
                                data-bs-dismiss="modal" data-bs-toggle="modal" href="#ForgotPasswordCodeModal"
                            >
                                {{t_label('Send')}}
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end forgot-password-modal -->
<!-- begin forgot-password-code-modal -->
<div
    class="modal modal-blur fade"
    id="ForgotPasswordCodeModal"
    tabindex="-1"
    aria-labelledby="ForgotPasswordCodeModalLabel"
    aria-hidden="true"
>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h5 class="modal-title w-100" id="ForgotPasswordCodeModalLabel">{{t_label('Forget Password ? ')}}</h5>
            </div>
            <div class="modal-body">
                 <div class="text-center">
                      <div class="text-center  ">
                        {{t_label('Please check your email and enter the verification code')}}
                     </div>
                    <form class="requires-validation " novalidate>
                        <div class="col-md-12 mt-4 text-center">
                            <div class="text-center partitioned-input-outer">
                                <div class="text-cente partitioned-input-inner">
                                    <input type="text" maxlength="4" class="partitioned-input"/>
                                </div>
                            </div>
                        </div>

                        <div class="form-button mt-4">
                            <button
                                type="button"
                                class="btn btn-primary w-100 btn-hover"
                                data-bs-dismiss="modal" data-bs-toggle="modal" href="#ForgotPasswordRecoverModal"

                            >
                                {{t_label('Reset your password ')}}                        </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end forgot-password-code-modal -->
<!-- begin forgot-password-recover-modal -->
<div
    class="modal modal-blur fade"
    id="ForgotPasswordRecoverModal"
    tabindex="-1"
    aria-labelledby="ForgotPasswordRecoverModalLabel"
    aria-hidden="true"
>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h5 class="modal-title w-100"
                    id="ForgotPasswordRecoverModalLabel">{{t_label('Forget Password ? ')}}</h5>
            </div>
            <div class="modal-body">
                 <div class="text-center">

                    <div class="text-center text-white">
                        {{t_label('To reset your password, enter your email address')}}
                     </div>
                    <form class="requires-validation" novalidate>
                        <div class="col-md-12 mt-3">
                            <input
                                class="form-control"
                                type="password"
                                name="password"
                                placeholder={{t_label('new password')}}
                                    required
                            />
                        </div>
                        <div class="col-md-12 mt-3">
                            <input
                                class="form-control"
                                type="password"
                                name="password"
                                placeholder="Reenter password"
                                required
                            />
                        </div>

                        <div class="form-button mt-4">
                            <button

                                data-bs-dismiss="modal" data-bs-toggle="modal" data-bs-target="#LoginModal"
                                class="btn btn-primary w-100 btn-hover"
                            >
                                {{t_label('Reset your password ')}}                        </button>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


<div
    class="modal modal-blur fade"
    id="video_model"
    tabindex="-1"
    aria-labelledby="video_modelLabel"
    aria-hidden="true"
>
    <div class="modal-dialog ">
        <div class="modal-content p-0 m-0">
            {{--                <div class="modal-header text-center">--}}
            {{--                    <h5 class="modal-title w-100" id="ForgotPasswordRecoverModalLabel">نسيت كلمة المرور</h5>--}}
            {{--                </div>--}}
            <div class="modal-body p-0 m-0">
                <video src="{{asset('media/Freelance_Arabia_Platform.mp4')}}" controls class="mt-3" width="100%"
                       height="100%"></video>

            </div>
        </div>
    </div>
</div>
@yield('modals')
@stack('modals_s')
