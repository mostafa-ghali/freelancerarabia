@php
    $dir = getLang() === 'ar' ? 'rtl' : 'ltr';
    $auth_user = auth('web')->user();
    $sub_categories_ids = [];

    $languages = \App\Models\Languages::get();
    foreach (\App\Models\Category\Category::get()->pluck('id')->toArray() as $category){
        $sub_cat = \App\Models\Category\SubCategory::where('category_id',$category)->first();
        if (is_null($sub_cat)){
            continue;
        }
        array_push($sub_categories_ids , $sub_cat->id);
    }    $original_categories = \App\Models\Category\Category::get();
    $categories = \App\Models\Category\SubCategory::whereIN('id',$sub_categories_ids)->get();
    $original_categories = \App\Models\Category\Category::get();
    $labels = labels(['web.general']);
  if(!is_null($auth_user)){
        $messages = \App\Models\Messages\Messages::where('receiver_id',$auth_user->id)->with('Sender')->get()->take(5);
        $notifications =$auth_user->unreadNotifications;
        $user_cart = \App\Models\Cart\Cart::where('user_id',$auth_user->id)->where('cart_id', getCartId())->get();
        $cart_items  = \App\Models\Cart\Cart::where('user_id',$auth_user->id)->where('cart_id', getCartId())->with('service')->take(5)->get();

  }



@endphp

    <!DOCTYPE html>
<html dir="{{$dir}}">
<head>
    @include('web.layouts.head')

</head>

<body dir="{{$dir}}">
<div class="site-container">


@include('web.layouts.header')



        <!-- end nav -->

        <!-- begin main -->
    {{--    <img src="{{asset('web_ar/img/comming-soon.jpg')}}">--}}
    @yield('content')
<!-- end main -->
    <!-- begin footer -->
</div>


@include('web.layouts.footer')

    <!-- end footer -->

@include('web.layouts.modal')





@include('web.layouts.script')



    </body>

</html>

