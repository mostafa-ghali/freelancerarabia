@extends('web.layouts.master')
@section('content')
    <!-- begin main -->
    <main class="container-fluid">
        <br>
        <br>
        <div class="site-padding">
            <form action="{{route('checkout.cart')}}" method="POST">
                @csrf
                <div class="row mt-5  mb-5">
                    <div class="col-12 mt-5 ">
                        <h1 class="font-xl">
                            {{t_label('shopping_basket')}}
                        </h1>
                    </div>
                    <hr>
                    <div class="col-lg-8">

                        @if(count($carts))
                            @foreach($carts as $cart)
                                <div class="custom-card card-border mb-3">
                                    <div class="custom-card-body">
                                        <div
                                            class="order-header border-bottom d-flex justify-content-between align-items-baseline">
                                            <h2 class="font-md">
                                                {{$cart->service->title}}
                                            </h2>
                                            <span>
{{--                                    <a href="{{route('store.cart.delete_item',$cart->service->id)}}">--}}
                                                {{--                                        <i class="fa fa-trash text-danger" aria-hidden="true"></i>--}}
                                                {{--                                    </a>--}}
                                </span>
                                        </div>
                                        <div class="mt-3 summary-row">
                                            <div class="title ">{{t_label('amount')}}</div>
                                            <div class="">
                                                <span
                                                    class=" num-border num-border-primary p-1 font-md "> {{$cart->service->currency_price}} </span>
                                                {{current_currencies()->code}}
                                            </div>
                                        </div>

                                        <div class="mt-3 summary-row">
                                            <div class="title">{{t_label('number_of_item')}}</div>
                                            <div class=""><span
                                                    class=" num-border num-border-primary p-1 font-md ">{{$cart->quantity}}</span>
                                            </div>
                                        </div>

                                        <div class="mt-3 summary-row">
                                            <label class="title">{{t_label('Additional_Services')}}</label>
                                            <div class="">

                                                @if(\App\Models\Services\AdditionalServices::where('service_id',$cart->service->id)->count() >0)
                                                    @foreach(\App\Models\Services\AdditionalServices::where('service_id',$cart->service->id)->get() as $add_service)

                                                        <ul class="check-list d-flex">
                                                            <input type="checkbox" style="margin: 10px"
                                                                   data-price="{{$add_service->currency_price}}"
                                                                   name="additional_service[{{$cart->service->id}}][]"
                                                                   value="{{$add_service->id}}">
                                                            <li>{{t_label('Title')}} : {{$add_service->title}}</li>
                                                            <li>{{t_label('Price')}}
                                                                : {{$add_service->currency_price_symbol}} </li>
                                                        </ul>
                                                    @endforeach

                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @php
                                    $add_servoices  =\App\Models\Services\AdditionalServices::where('service_id',$cart->service->id)->get();
                                      $add_service = 0 ;
                                      foreach ($add_servoices as $add_servoice){
                                          $add_service +=$add_servoice->price;
                                      }
                                @endphp
                            @endforeach
                        @else
                            <div class="mb-3">{{t_label('No Item')}}</div>
                        @endif
                    </div>

                    <div class="col-lg-4">
                        <div class="custom-card card-border h-100">
                            <div class="custom-card-body h-100">
                            <span style="color:#5b84a3">
                                {{t_label('Payment Details')}}
                               </span>
                                <hr>
                                <div class="mt-3 summary-row">
                                    <div class="title ">{{t_label('price')}}</div>
                                    <div class=" ">
                                        <span
                                            class=" num-border num-border-primary p-1 font-md "> {{$sub_total}}</span> {{current_currencies()->code}}
                                    </div>
                                </div>


                                <div class="mt-3 summary-row">
                                    <div class="title ">
                                        {{t_label('Tax_amount')}}
                                    </div>
                                    <div class="">
                                      <span class=" num-border num-border-primary p-1 font-md ">
{{--                                          {{$strip_tax .'%'}}--}}
                                          {{$total_tax}}

                                      </span>
                                        {{current_currencies()->code}}
                                    </div>
                                </div>
                                {{--
                                                            <div class="mt-3 summary-row">
                                                                <div class="title ">{{t_label('Tax')}}</div>
                                                                <div class="">
                                                                      <span class=" num-border num-border-primary p-1 font-md ">
                                                                          {{$total_tax}}
                                                                      </span>
                                                                </div>
                                                            </div>--}}


                                <div class="mt-3 summary-row">
                                    {{--                                <div class="title ">{{t_label('Additional_Services')}}</div>--}}
                                    {{--    <div class="">
                                                <p style="font-size: 18px"><img class="mx-2"
                                                                                src="{{asset('web_en/img/Icon feather-check-circle.png')}}"
                                                                                width="20px"> service one</p>
                                                <p style="font-size: 18px"><img class="mx-2"
                                                                                src="{{asset('web_en/img/Icon feather-check-circle.png')}}"
                                                                                width="20px"> service one</p>
                                                <p style="font-size: 18px"><img class="mx-2"
                                                                                src="{{asset('web_en/img/Icon feather-check-circle.png')}}"
                                                                                width="20px"> service one</p>
                                        </div>--}}
                                </div>

                                <div class="mt-3 summary-row">
                                    <div class="title ">
                                        {{t_label('Total')}}
                                    </div>
                                    <div class="">
                                      <span class=" num-border num-border-primary p-1 font-md ">
                                      {{$total }}
                                      </span> {{current_currencies()->code}}
                                    </div>
                                </div>
                                <div class="col-12 text-end mt-3">
                                    <button class="btn btn-primary text-capitalize px-4 w-100 form-control" type="submit">
                                        {{t_label('pay')}}
                                    </button>
                                </div>


                            </div>

                        </div>


                    </div>

                </div>
            </form>
            <hr>


            <div class="subcat py-5">
                <div class="container-fluid">
                    <h3 style="color:#5b84a3">
                        {{t_label('Services_you_may_like')}}
                    </h3>
                    <div class="row">
                        <div class="my-2 col-md-3 col-sm-12">
                            <div class="card">
                                <div class="swiper mySwiper">
                                    <div class="swiper-wrapper">
                                        <div class="swiper-slide">
                                            <a href="#">
                                                <img data-src="{{asset('web_en/img/banner1.png')}} " class="lazy" alt=""/>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="swiper-button-next"></div>
                                    <div class="swiper-button-prev"></div>
                                    <div class="swiper-pagination"></div>
                                </div><!-- end swiper -->
                                <div class="down ">
                                    <div class="avatarr px-2 py-2">
                                        <img data-src="{{asset('web_ar/img/default-avatar.jpg')}}" class="avatar-image lazy"/>
                                        <a href="#">{{t_label('username')}}</a>
                                    </div>
                                    <div class="desc px-2">
                                        <a href="#">
                                            {{t_label('I_will_create_UI_UX_design_for_mobile_app_,_website_or_landing')}}
                                        </a>
                                    </div>
                                    <div class="d-flex justify-content-between px-2 pt-2">
                                        <p class="str pt-2 px-2">
                                            <span class="fa fa-star checked"></span>
                                            <span class="">5.0</span>(35)
                                        </p>
                                        <ul class="p-0 d-flex">
                                            <li class="pt-1"><a href="#"><i class="fas fa-heart"></i></a></li>
                                        </ul>
                                    </div>
                                    <div class="bar">
                                        <a href="#" class="btn w-100">{{t_label('service details')}}</a>
                                    </div>
                                </div>
                            </div><!-- end card -->
                        </div>


                    </div>
                </div>
            </div>
            <!-- end recommended-services -->

    </main>

@endsection
