@extends('web.layouts.master')
@section('content')
    <!-- begin main -->
    <main class="container-fluid">
        <div class="site-padding">
            <div class="row  mb-5">
                <div class="col-12 mt-5 ">
                    <h1 class="font-xl">
                        {{t_label('Confirm_and_checkout')}}
                    </h1>
                </div>
                <div class="col-lg-8 mt-3 mb-3 ">
                    <div class="custom-card">
                        <div class='form-row row'>
                            <!--div class='col-md-12 error form-group hide' >
                                <div class='alert-danger alert'>

                                </div>
                            </div-->
                        </div>
                        <div class="custom-card-body">
                            <form action="{{route('confirm.checkout')}}" method="POST"
                                  class="require-validation"
                                  data-cc-on-file="false"
                                  data-stripe-publishable-key="{{ env('STRIPE_KEY') }}"
                                  id="payment-form">
                                @csrf
                                <input type="hidden" name="paymentIntentId" value="{{$paymentIntentId}}">
                                <div class="col-lg-12">
                                    <div>
                                        <div class='form-row row'>
                                            <div class='col-xs-12 form-group required'>
                                                <label class='control-label'>{{t_label('Name on Card')}}</label> <input
                                                    class='form-control' size='4' type='text'>
                                            </div>
                                        </div>
                                        <div class='form-row row'>
                                            <div class='col-xs-12 form-group  required'>
                                                <label class='control-label'>
                                                    {{t_label('Card_Number')}}
                                                </label>
                                                <input
                                                    autocomplete='off' class='form-control card-number'
                                                    size='20'
                                                    type='text'>
                                            </div>
                                        </div>
                                        <div class='form-row row'>
                                            <div class='col-xs-12 col-md-4 form-group cvc required'>
                                                <label class='control-label'>
                                                    {{t_label('CVC')}}
                                                </label> <input
                                                    autocomplete='off'
                                                    class='form-control card-cvc' placeholder='ex. 311'
                                                    size='4'
                                                    type='text'>
                                            </div>
                                            <div class='col-xs-12 col-md-4 form-group expiration required'>
                                                <label class='control-label'>
                                                    {{t_label('Expiration_Month')}}
                                                </label> <input
                                                    class='form-control card-expiry-month' placeholder='MM'
                                                    size='2'
                                                    type='text'>
                                            </div>
                                            <div class='col-xs-12 col-md-4 form-group expiration required'>
                                                <label class='control-label'>
                                                    {{t_label('Expiration_Year')}}
                                                </label> <input
                                                    class='form-control card-expiry-year' placeholder='YYYY'
                                                    size='4'
                                                    type='text'>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-12 pt-4">

                                        <div id="paymentMethods">
                                            <div class="font-md text-primary">
                                            </div>
                                            <div class="mt-3">

                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <div class="text-end">

                                    <button class="btn btn-primary px-4" type="submit">
                                        {{t_label('Confirm_and_checkout')}}
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4  mt-3 mb-3 ">
                    <div class="custom-card card-border h-100">
                        <div class="custom-card-body h-100">
                            <span style="color:#5b84a3">
                                {{t_label('Payment Details')}}
                               </span>
                            <hr>
                            <div class="mt-3 summary-row">
                                <div class="title ">{{t_label('price')}}</div>
                                <div class=" ">
                                        <span
                                            class="  num-border-primary p-1 font-md "> {{session('sub_total_currency')}}</span> {{current_currencies()->code}}
                                </div>
                            </div>


                            <div class="mt-3 summary-row">
                                <div class="title ">
                                    {{t_label('Tax_amount')}}
                                </div>
                                <div class="">
                                      <span class=" num-border-primary p-1 font-md ">
{{--                                          {{$strip_tax .'%'}}--}}
                                          {{session('total_tax_c')}}

                                      </span>
                                    {{current_currencies()->code}}
                                </div>
                            </div>


                            <div class="mt-3 summary-row">
                                {{--                                <div class="title ">{{t_label('Additional_Services')}}</div>--}}
                                {{--    <div class="">
                                            <p style="font-size: 18px"><img class="mx-2"
                                                                            src="{{asset('web_en/img/Icon feather-check-circle.png')}}"
                                                                            width="20px"> service one</p>
                                            <p style="font-size: 18px"><img class="mx-2"
                                                                            src="{{asset('web_en/img/Icon feather-check-circle.png')}}"
                                                                            width="20px"> service one</p>
                                            <p style="font-size: 18px"><img class="mx-2"
                                                                            src="{{asset('web_en/img/Icon feather-check-circle.png')}}"
                                                                            width="20px"> service one</p>
                                    </div>--}}
                            </div>

                            <div class="mt-3 summary-row">
                                <div class="title ">
                                    {{t_label('Total')}}
                                </div>
                                <div class="">
                                      <span class="   num-border-primary p-1 font-md ">
                                        {{session('total_c')}}

                                      </span> {{current_currencies()->code}}
                                </div>
                            </div>


                        </div>

                    </div>


                </div>

            </div>
        </div>
    </main>
    <!-- end main -->

@endsection
@section('scripts')
    <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
    <script type="text/javascript">
        $(function () {
            var $form = $(".require-validation");
            $('form.require-validation').bind('submit', function (e) {
                var $form = $(".require-validation"),
                    inputSelector = ['input[type=email]', 'input[type=password]',
                        'input[type=text]', 'input[type=file]',
                        'textarea'
                    ].join(', '),
                    $inputs = $form.find('.required').find(inputSelector),
                    $errorMessage = $form.find('div.error'),
                    valid = true;
                $errorMessage.addClass('hide');
                $('.has-error').removeClass('has-error');
                $inputs.each(function (i, el) {
                    var $input = $(el);
                    if ($input.val() === '') {
                        $input.parent().addClass('has-error');
                        $errorMessage.removeClass('hide');
                        e.preventDefault();
                    }
                });
                if (!$form.data('cc-on-file')) {
                    e.preventDefault();
                    Stripe.setPublishableKey($form.data('stripe-publishable-key'));
                    Stripe.createToken({
                        number: $('.card-number').val(),
                        cvc: $('.card-cvc').val(),
                        exp_month: $('.card-expiry-month').val(),
                        exp_year: $('.card-expiry-year').val()
                    }, stripeResponseHandler);
                }
            });

            function stripeResponseHandler(status, response) {
                if (response.error) {
                    $('.error')
                        .removeClass('hide')
                        .find('.alert')
                        .text(response.error.message);
                } else {
                    /* token contains id, last4, and card type */
                    var token = response['id'];
                    $form.find('input[type=text]').empty();
                    $form.append("<input type='hidden' name='stripeToken' value='" + token + "'/>");
                    $form.get(0).submit();
                }
            }
        });
    </script>
@endsection

