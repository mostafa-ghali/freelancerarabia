@extends('web.layouts.master')
@section('content')
    <!-- begin main -->
    <main class="container mb-5"  style="margin-top:110px">
        <div class="custom-card " style=" border-radius:20px;    box-shadow: 0 2px 5px 0 rgba(193,200,207,0.3)!important;
">
            <div class="custom-card-body chat">
                <div class="chat-side sidenav" style="border-radius:20px;    box-shadow: 0 2px 5px 0 rgba(193,200,207,0.3)!important;" id="chatSideNav">
                    <div class="chat-header">
                        <div class="dropdown custom-dropdown font-lg text-primary">
                            {{--                        <div class="dropdown-toggle" data-bs-toggle="dropdown">--}}
                            <div class="dropdown-toggle">

                                <a href="{{route('store.contact.show')}}"
                                   class="">{{t_label('all_chats')}}  </span>
                                </a>


                                {{--                      <i class="fas fa-chevron-down"></i>--}}
{{--                                </a>--}}
                            </div>

                            <div
                                class="dropdown-menu"
                                aria-labelledby="dropdownMenuButton"
                            >
                                <label>
                                    <input
                                        class="form-check-input me-3"
                                        type="radio"
                                        name="chatType"
                                        value=""
                                        checked
                                    />
                                    {{t_label('all_chats')}}
                                </label>
                                <label>
                                    <input
                                        class="form-check-input me-3"
                                        type="radio"
                                        name="chatType"
                                        value=""
                                    />
                                    الأحدث
                                </label>
                                <label>
                                    <input
                                        class="form-check-input me-3"
                                        type="radio"
                                        name="chatType"
                                        value=""
                                    />
                                    المميزة </label
                                ><label>
                                    <input
                                        class="form-check-input me-3"
                                        type="radio"
                                        name="chatType"
                                        value=""
                                    />
                                    الغير مقروءة
                                </label>
                            </div>
                        </div>

                        <div class="text-primary font-sm">
                            <i class="fas fa-search cursor-pointer"></i>
                            <i class="fas fa-arrow-right cursor-pointer btn-close-chat"></i>
                        </div>
                    </div>
                    <div class="chat-list h-100">
                        @foreach($senders as $sender)
                            <a href="{{route('store.contact.showMessages',$sender->id,auth('web')->user()->id)}}">
                                <div class="chat-list-item border-bottom">
                                    <div class="chat-list-item-avatar">
                                        <div class="avatar">
                                            <img data-src="{{asset($sender->image)}}" class="lazy avatar-image"/>
                                        </div>
                                    </div>
                                    <div class="chat-list-item-name">
                                        <div class="font-sm text-primary">{{$sender->name}}</div>

                                        <span
                                            class="font-xs">{{substr($sender->messeges->where('sender_id',$sender->id)->latest()->first()->message,-30) }}</span>
                                    </div>
                                    <div
                                        class="chat-list-item-time">{{time_elapsed_string($sender->messeges->where('sender_id',$sender->id)->latest()->first()->created_at)}}</div>
                                </div>
                            </a>
                        @endforeach
                    </div>
                </div>
                <div class="chat-main">
                    <div class="chat-header">
                        <div>
                            <button class="btn btn-light btn-open-chat">
                                <i class="fas fa-bars"></i>
                            </button>
                        </div>
                        <div>

                            <div class="chat-title font-md">
                                <a href="{{route('store.profile_seller',$user->id)}}">{{$user->name}}</a>
                            </div>
                            {{--                        <div class="chat-info font-xs">--}}
                            {{--                            <span> اخر ظهور منذ 3 س </span>--}}
                            {{--                            |--}}
                            {{--                            <span>--}}
                            {{--                      <i class="far fa-clock text-sm"></i>--}}
                            {{--                      6 ابريل , 1:12 م--}}
                            {{--                    </span>--}}
                            {{--                        </div>--}}
                        </div>
                    </div>
                    <div class="chat-log">
                        @foreach($messages_sender as $message)
                            <div class="message message-sent">
                                <div class="message-avatar">
                                    <div class="avatar">
                                        <img data-src="{{asset('web_ar/img/default-avatar.jpg')}}" class="avatar-image lazy"/>
                                    </div>
                                </div>
                                <div class="message-name">
                                    <div>
                                        <a
                                            class="font-sm text-primary"
                                            href="#"
                                        >
                                            @foreach($message->Sender as $name)
                                                {{$name->name}}
                                            @endforeach
                                        </a>
                                    </div>
                                    @if($message->attachment)
                                        <span class="font-xs">
                                                   <div class="col-md-12 mb-2">
                  <img id="preview-image-before-upload" class="lazy" data-src="{{asset($message->attachment)}}"
                       style="max-height: 150px;">
              </div>
                                    </span>
                                        @endif

                                    <span class="font-xs">{{ substr($message->message , -50) }}</span>

                                </div>
                                <div class="message-time">{{time_elapsed_string($message->created_at)}}</div>
                            </div>
                        @endforeach
                            <div class="message message-sent">
                                <div class="message-avatar">
                                    <div class="avatar">
                                        <img data-src="" class="avatar-image lazy"/>
                                    </div>
                                </div>
                                <div class="message-name">
                                    <div>

                                    </div>

                                    <span class="font-xs">
                                                   <div class="col-md-12 mb-2">
                  <img id="preview-image-before-upload"
                       style="max-height: 150px;">
              </div>
                                    </span>

                                </div>
                                <div class="message-time">{{time_elapsed_string($message->created_at)}}</div>
                            </div>

                    </div>


                    <form action="{{route('store.contact.replayMessages')}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="chat-control">
                            <div class="chat-file cursor-pointer">
                                <input type="file" name="attachment" id="attachment" style="display: none">
                                <label for="attachment">
                                    <i class="fas fa-paperclip fa-lg"></i>
                                </label>

                            </div>

                            <div class="chat-input">
                                <input type="text" name="message" class="form-control"/>
                                <input type="hidden" name="sender_id" value="{{auth('web')->user()->id}}"
                                       class="form-control"/>
                                <input type="hidden" name="receiver_id"
                                       value="{{$messages_sender->first()->Sender->first()->id}}" class="form-control"/>
                            </div>

                            <button type="submit" class="btn btn-primary px-5 chat-btn">
                                <i class="fas fa-paper-plane"></i>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </main>
    <!-- end main -->
@endsection
@section('scripts')
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>

    <script type="text/javascript">

        $(document).ready(function (e) {


            $('#attachment').change(function(){

                let reader = new FileReader();

                reader.onload = (e) => {

                    $('#preview-image-before-upload').attr('src', e.target.result);
                }

                reader.readAsDataURL(this.files[0]);

            });

        });

    </script>

@endsection
