@extends('web.layouts.master')
@section('content')
    <!-- begin main -->
    <main class="container-fluid p-0 " >
        <div class="custom-card p-0 mt-5">
            <div class="custom-card-body chat">
                <div class="chat-side sidenav" id="chatSideNav">
                    <div class="chat-header">
                        <div class="dropdown custom-dropdown font-lg text-primary">
                            {{--                        <div class="dropdown-toggle" data-bs-toggle="dropdown">--}}
                            <div class="dropdown-toggle">
                                <a href="{{route('store.contact.show')}}"
                                   class="font-lg"> {{t_label('all_chats')}} </span>
                                </a>
                                </span>
                            </div>

                            <div
                                class="dropdown-menu"
                                aria-labelledby="dropdownMenuButton"
                            >
                                <label>
                                    <input
                                        class="form-check-input me-3"
                                        type="radio"
                                        name="chatType"
                                        value=""
                                        checked
                                    />
                                    {{t_label('all_chats')}}
                                </label>
                                <label>
                                    <input
                                        class="form-check-input me-3"
                                        type="radio"
                                        name="chatType"
                                        value=""
                                    />
                                    الأحدث
                                </label>
                                <label>
                                    <input
                                        class="form-check-input me-3"
                                        type="radio"
                                        name="chatType"
                                        value=""
                                    />
                                    المميزة </label
                                ><label>
                                    <input
                                        class="form-check-input me-3"
                                        type="radio"
                                        name="chatType"
                                        value=""
                                    />
                                    الغير مقروءة
                                </label>
                            </div>
                        </div>

                        <div class="text-primary font-sm">
                            <i class="fas fa-search cursor-pointer"></i>
                            <i class="fas fa-arrow-right cursor-pointer btn-close-chat"></i>
                        </div>
                    </div>
                    <div class="chat-list">

                        @foreach($senders as $sender)
                            <a href="{{route('store.contact.showMessages',$sender->id,auth('web')->user()->id)}}">
                                <div class="chat-list-item border-bottom">
                                    <div class="chat-list-item-avatar">
                                        <div class="avatar">
                                            <img data-src="{{asset($sender->image)}}" class="avatar-image lazy"/>
                                        </div>
                                    </div>
                                    <div class="chat-list-item-name">
                                        <div class="font-sm text-primary">{{$sender->name}}</div>

                                        <span
                                            class="font-xs">{{substr($sender->messeges->where('sender_id',$sender->id)->latest()->first()->message,-30)}}</span>
                                    </div>
                                    <div
                                        class="chat-list-item-time">{{time_elapsed_string($sender->messeges->where('sender_id',$sender->id)->latest()->first()->created_at)}}</div>
                                </div>
                            </a>
                        @endforeach
                    </div>
                </div>
                <div class="chat-main">
                    <div class="chat-header">
                        <div>
                            <button class="btn btn-light btn-open-chat">
                                <i class="fas fa-bars"></i>
                            </button>
                        </div>
                        <div>
                            @if(!is_null($first_uid_message))
                                <div class="chat-title font-md">
                                    <a href="{{route('store.profile_seller',$first_uid_message->Sender->first()->id)}}">{{$first_uid_message->Sender->first()->name}}</a>
                                </div>
                        </div>
                    </div>

                    <div class="chat-log">
                        {{--                    @foreach($messages as $message)--}}
                        <div class="message message-sent">
                            <div class="message-avatar">
                                <div class="avatar">
                                    <img data-src="{{asset($first_uid_message->Sender->first()->image)}}"
                                         class="avatar-image lazy"/>
                                </div>
                            </div>
                            <div class="message-name">
                                <div>
                                    <a
                                        class="font-sm text-primary"
                                        href="profile-seller.html"
                                    >

                                        {{$first_uid_message->Sender->first()->name}}
                                    </a>
                                </div>
                                @if($first_uid_message->attachment)

                                    <span class="font-xs">
                                                   <div class="col-md-12 mb-2">
                  <img id="preview-image-before-upload-chat lazy" data-src="{{asset($first_uid_message->attachment)}}"
                       style="max-height: 150px;">
              </div>
                                    </span>
                                @endif

                                <span class="font-xs">{{ $first_uid_message->message}}</span>
                            </div>
                            <div class="message-time">{{time_elapsed_string( $first_uid_message->created_at)}}</div>
                        </div>
                        <div class="message message-sent">
                            <div class="message-avatar">
                                <div class="avatar">
                                    <img data-src=""  class="avatar-image lazy"/>
                                </div>
                            </div>
                            <div class="message-name">
                                <div>
                                </div>

                                <span class="font-xs">
                                                   <div class="col-md-12 mb-2">
                  <img id="preview-image-before-upload-chat"
                       style="max-height: 150px;">
              </div>
                                    </span>

                            </div>
                            <div class="message-time"></div>
                        </div>
                        {{--                    @endforeach--}}
                    </div>
                    @endif
                    <form action="{{route('store.contact.replayMessages')}}" method="post"
                          enctype="multipart/form-data">

                        @csrf
                        <div class="chat-control">
                            <div class="chat-file cursor-pointer">
                                <input type="file" name="attachment" id="attachment_chat" style="display: none">
                                <label for="attachment_chat">
                                    <i class="fas fa-paperclip fa-lg"></i>
                                </label>

                            </div>
                            <div class="chat-input">
                                <input type="text" name="message" class="form-control"/>
                                <input type="hidden" name="sender_id" value="{{auth('web')->user()->id}}"
                                       class="form-control"/>
                                @if(!is_null($first_uid_message))
                                    <input type="hidden" name="receiver_id"
                                           value="{{$first_uid_message->Sender->first()->id}}" class="form-control"/>
                                @endif
                            </div>

                            <button type="submit" class="btn btn-primary px-5 chat-btn">
                                <i class="fas fa-paper-plane"></i>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </main>
    <!-- end main -->
@endsection
@section('scripts')
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>

    <script type="text/javascript">

        $(document).ready(function (e) {


            $('#attachment_chat').change(function () {

                let reader = new FileReader();

                reader.onload = (e) => {

                    $('#preview-image-before-upload-chat').attr('src', e.target.result);
                }

                reader.readAsDataURL(this.files[0]);

            });

        });

    </script>

@endsection

